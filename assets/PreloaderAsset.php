<?php

namespace app\assets;

use yii\web\AssetBundle;

class PreloaderAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/shop/assets';

    public $css = [
        'css/preloaders.css',
    ];

    public $js = [
        'js/jquery.preloaders.min.js',
        'js/preloaders.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG?true:false,
    ];

    public function init()
    {
        parent::init();
        $this->sourcePath = __DIR__ . DIRECTORY_SEPARATOR . 'src';
    }
}
