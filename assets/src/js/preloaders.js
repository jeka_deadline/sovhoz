function preloaderStart() {
    $.preloader.start({
        src : '/img/preloaders/sprites4.png',
        modal: false,
    });
}

function preloaderModalStart(el) {
    el = typeof el !== 'undefined' ?  el : null;
    if (el) {
        $('<div/>').addClass('jquery-preloader preloader-display').appendTo(el).preloader({
            src : '/img/preloaders/sprites4.png',
            modal: true
        });

        $('<div/>').addClass('jquery-preloader-overlay preloader-display-background').appendTo(el).fadeIn('slow');
    } else {
        $.preloader.start({
            src: '/img/preloaders/sprites4.png',
            modal: true,
        });
    }
}

function preloaderStop(el) {
    el = typeof el !== 'undefined' ?  el : null;
    if (el) {
        $(el+' .jquery-preloader').each(function(){
            clearTimeout($(this).data('interval'));
            $(this).remove();
        });
        $(el+' .jquery-preloader-overlay').remove();
    } else {
        $.preloader.stop();
    }
}