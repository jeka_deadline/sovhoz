<?php

use yii\db\Migration;

class m170720_131446_add_user_in_2shop extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_users', 'now_in_shop', $this->integer()->defaultValue(null));
        $this->addColumn('shop_seller_reports', 'in_shop', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170720_131446_add_user_in_2shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170720_131446_add_user_in_2shop cannot be reverted.\n";

        return false;
    }
    */
}
