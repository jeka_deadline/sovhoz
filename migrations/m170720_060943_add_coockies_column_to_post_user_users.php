<?php

use yii\db\Migration;

class m170720_060943_add_coockies_column_to_post_user_users extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_users', 'cookies', $this->text()->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170720_060943_add_coockies_column_to_post_user_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170720_060943_add_coockies_column_to_post_user_users cannot be reverted.\n";

        return false;
    }
    */
}
