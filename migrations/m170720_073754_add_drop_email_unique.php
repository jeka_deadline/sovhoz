<?php

use yii\db\Migration;

class m170720_073754_add_drop_email_unique extends Migration
{
    public function safeUp()
    {
       $this->dropIndex('uix_user_users_login', 'user_users');
       $this->dropIndex('uix_user_users_email', 'user_users');
    }

    public function safeDown()
    {
        echo "m170720_073754_add_drop_email_unique cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170720_073754_add_drop_email_unique cannot be reverted.\n";

        return false;
    }
    */
}
