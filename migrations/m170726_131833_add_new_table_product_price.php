<?php

use yii\db\Migration;

class m170726_131833_add_new_table_product_price extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_price', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'price_bag' => $this->float(),
            'price_kg' => $this->float(),
            'forShop' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx_id',
            'product_price',
            'product_id'
        );
        $this->addForeignKey(
            'fk-price-product_id',
            'product_price',
            'product_id',
            'product_products',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        echo "m170726_131833_add_new_table_product_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_131833_add_new_table_product_price cannot be reverted.\n";

        return false;
    }
    */
}
