<?php

use yii\db\Migration;

class m170720_061608_add_coockies_column_to_post extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product_products', 'check_one', $this->boolean()->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170720_061608_add_coockies_column_to_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170720_061608_add_coockies_column_to_post cannot be reverted.\n";

        return false;
    }
    */
}
