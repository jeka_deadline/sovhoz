<?php
namespace app\modules\core\components;

use Yii;
use yii\web\Controller;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class BackendController extends Controller
{

    public $model;
    public $searchModel;

    public function init()
    {
        parent::init();
        $this->model = new $this->searchModel();
    }

    protected function getGridSerialColumn()
    {
        return ['class' => 'yii\grid\SerialColumn'];
    }

    protected function getGridActions($options = [])
    {
        $buttons = [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('core', 'Actions'),
        ];

        return ArrayHelper::merge($buttons, $options);
    }

    protected function getGridActive($attribute = 'active')
    {
        return [
            'attribute' => $attribute,
            'filter'    => Html::activeDropDownList($this->model, $attribute, ['0' => Yii::t('core', 'No'), '1' => Yii::t('core', 'Yes')], ['class' => 'form-control', 'prompt' => '']),
            'value'     => function($model) use ($attribute) {
                              return ($model->$attribute) ? '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>';
            },
            'format'    => 'html',
        ];
    }

}