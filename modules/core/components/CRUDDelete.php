<?php
namespace app\modules\core\components;

use Yii;
use app\modules\core\components\BackendBaseAction;

class CRUDDelete extends BackendBaseAction
{

    public $modelPrimaryKey     = 'id';
    public $redirectAfterAction = ['index'];

    public function run()
    {
        $model = $this->findModel(Yii::$app->request->get($this->modelPrimaryKey));

        if (!empty($this->scenarios)) {
            if (is_string($this->scenarios)) {
                $model->setScenario($this->scenarios);
            }
        }
        
        $model->delete();

        return $this->controller->redirect($this->redirectAfterAction);
    }

}