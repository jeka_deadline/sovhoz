<?php
namespace app\modules\core\components;
use Yii;
use app\modules\core\components\BackendBaseAction;
class CRUDCreate extends BackendBaseAction
{
    public $view                = 'crud-share-template-create-update';
    public $activeFormConfig    = ['options' => ['enctype' => 'multipart/form-data']];
    public $redirectAfterAction = ['index'];
    public $scenarios           = '';
    public function run()
    {
        $model  = new $this->controller->modelName();
        if (!empty($this->scenarios)) {
            if (is_string($this->scenarios)) {
                $model->setScenario($this->scenarios);
            }
        }
        $this->controller->viewPath     = $this->viewPath;
        $this->controller->view->title  = $this->title;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->redirect($this->redirectAfterAction);
        } else {
            return $this->controller->render($this->view, [
                'model'             => $model,
                'formElements'      => $model->getFormElements(),
                'activeFormConfig'  => $this->activeFormConfig,
                'headerContent'     => $this->headerContent,
                'footerContent'     => $this->footerContent,
            ]);
        }
    }
}



