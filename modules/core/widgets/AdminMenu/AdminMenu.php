<?php
namespace app\modules\core\widgets\AdminMenu;

use Yii;
use yii\base\Widget;
use app\modules\user\models\User;

class AdminMenu extends Widget
{

    public function run()
    {
        $userRoleId = Yii::$app->user->identity->role_id;

        switch ($userRoleId) {
            case User::ROLE_ADMIN:
                $template = 'admin';
                break;
            case User::ROLE_SELLER:
                $template = 'seller';
                break;
            case User::ROLE_OPERATOR:
                $template = 'operator';
                break;
            case User::ROLE_MANAGER:
                $template = 'manager';
                break;
            default:
                $template = NULL;
        }

        if ($template) {
            return $this->render($template);
        }
    }

}