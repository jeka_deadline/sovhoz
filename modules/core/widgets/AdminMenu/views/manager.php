<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?= $this->render('card-management'); ?>
<?= $this->render('user-management'); ?>
<?= $this->render('shop-management'); ?>
<?= $this->render('product-price'); ?>
<?= $this->render('all-checks'); ?>
<?= $this->render('admin-report'); ?>
<?= $this->render('seller-report'); ?>
<?= $this->render('product-management'); ?>
<?= $this->render('list-sellers'); ?>
<?= $this->render('clients-in-shops'); ?>