<?php
use yii\helpers\Url;
?>

<?= $this->render('user-management'); ?>
<?= $this->render('shop-management'); ?>
<?= $this->render('product-management'); ?>
<?= $this->render('card-management'); ?>
<?= $this->render('product-price'); ?>
<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/percentage/index']); ?>"><?= Yii::t('percentage', 'Control percentages'); ?></a></li>
<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/link-percentage-sum-sale/index-link-bonus']); ?>"><?= Yii::t('percentage', 'Management bonus percentage'); ?></a></li>
<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/link-percentage-sum-sale/index-link-discount']); ?>"><?= Yii::t('percentage', 'Management discount percentage'); ?></a></li>
<?= $this->render('all-checks'); ?>
<?= $this->render('check-card'); ?>
<?= $this->render('admin-report'); ?>
<?= $this->render('seller-report'); ?>
<?= $this->render('list-sellers'); ?>
<?= $this->render('clients-in-shops'); ?>
<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/report/export']); ?>"><?= Yii::t('check', 'Export'); ?></a></li>