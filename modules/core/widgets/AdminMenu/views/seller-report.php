<?php
use yii\helpers\Url;
?>

<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/report/find-reports']); ?>"><?= Yii::t('shop', 'Seller reports'); ?></a></li>