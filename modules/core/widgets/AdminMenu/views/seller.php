<?php
use yii\helpers\Url;
?>

<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/sale/sale']); ?>"><?= Yii::t('shop', 'Sale product'); ?></a></li>
<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/report/create']); ?>"><?= Yii::t('shop', 'Create report'); ?></a></li>
<?= $this->render('check-card'); ?>
<?= $this->render('check'); ?>
