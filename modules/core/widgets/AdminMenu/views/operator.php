<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?= $this->render('user-management'); ?>
<?= $this->render('card-management'); ?>
<?= $this->render('shop-management'); ?>
<?= $this->render('product-management'); ?>
<?= $this->render('product-price'); ?>
<?= $this->render('seller-report'); ?>
<?= $this->render('all-checks'); ?>
<?= $this->render('list-sellers'); ?>