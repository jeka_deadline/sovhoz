<?php
use yii\helpers\Url;
?>

<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/category/index']); ?>"><?= Yii::t('category', 'Categories product management'); ?></a></li>
<li class="list-group-item"><a href="<?= Url::toRoute(['/shop/product/index']); ?>"><?= Yii::t('product', 'Products management'); ?></a></li>