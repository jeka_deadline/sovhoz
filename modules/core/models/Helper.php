<?php
namespace app\modules\core\models;

use Yii;
use yii\web\ForbiddenHttpException;

class Helper
{

    public static function checkRule($dataRules, $userRoleId, $action)
    {
        if (isset($dataRules[ $userRoleId ][ 'danied-actions' ])) {
            if ($dataRules[ $userRoleId ][ 'danied-actions' ] === '*') {
                throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
            }
            foreach ($dataRules[ $userRoleId ][ 'danied-actions'] as $actionId) {
                if ($action->id === $actionId) {
                    throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
                }
            }
        }

        if (!isset($dataRules[ $userRoleId ])) {
            throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
        }

        if (!isset($dataRules[ $userRoleId ][ 'access-actions' ])) {
            return TRUE;
        }

        $allowActions = $dataRules[ $userRoleId ][ 'access-actions' ];

        if (is_string($allowActions)) {
            if ($allowActions === '*') {
                return TRUE;
            }
            $allowActions = explode(',', $allowActions);
        }

        foreach ($allowActions as $actionId) {
            if ($action->id === $actionId) {
                return TRUE;
            }
        }

        throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
    }

}