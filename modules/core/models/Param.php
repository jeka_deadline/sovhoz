<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\Param as BaseParam;

class Param extends BaseParam
{

    public static function savePostParams($values)
    {
        if (!$values || !is_array($values)) {
            return FALSE;
        }

        $params = self::find()->all();

        foreach ($params as $param) {
            if (isset($values[ $param->code ])) {
                $param->value = $values[ $param->code ];

                $param->save(FALSE);
            }
        }

        return TRUE;
    }

    public function getType()
    {
        return $this->hasOne(TypeFieldForm::className(), ['id' => 'type_id']);
    }

}