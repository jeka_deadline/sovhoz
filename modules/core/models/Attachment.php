<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\Attachment as BaseAttachment;
use yii\helpers\ArrayHelper;

class Attachment extends BaseAttachment
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'image' => [
                    'class'       => '\backend\modules\core\behaviors\UploadFileBehavior',
                    'attribute'   => 'file',
                    'uploadPath'  => Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . call_user_func([$this, 'getFilePath']),
                ],
            ] 
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order'], 'default', 'value' => 0],
          ]);
    }

    public function getFormElements()
    {
        return [
            'title'         => ['type' => 'text'],
            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            'file'          => ['type' => 'file'],
            'display_order' => ['type' => 'text'],
        ];
    }

}