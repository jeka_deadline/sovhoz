<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\Page as BasePage;
use common\models\core\PageMeta;
use yii\helpers\ArrayHelper;
use common\models\core\Helper as CommonHelper;

class Page extends BasePage
{

    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
                [['meta_description', 'meta_keywords'], 'string'],
                [['meta_title'], 'string', 'max' => 255],
            ]
        );
    }
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'meta_title' => Yii::t('core', 'Meta Title'),
                'meta_description' => Yii::t('core', 'Meta Description'),
                'meta_keywords' => Yii::t('core', 'Meta Keywords'),
            ]
        );
    }

    public function getFormElements()
    {
        $meta = $this->getMeta();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }

        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => Yii::t('core', 'Page information'),
                        'active'    => TRUE,
                        'elements'  => [
                            'header'        => ['type' => 'text', 'fieldAttributes' => ['template' => '{label}{input}']],
                            'menu_class'    => ['type' => 'text'],
                            'display_order' => ['type' => 'text'],
                            'active'        => ['type' => 'checkbox'],
                        ],
                    ],
                    [
                        'title'     => Yii::t('core', 'Meta information'),
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'header', 'display_order', 'active'];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();
    }

    private function saveMeta()
    {
        $meta = $this->getMeta();

        if (!$meta) {
            $meta = new PageMeta();
        }
        
        $meta->owner_id         = $this->id;
        $meta->model_class      = CommonHelper::generateShortModelClass(get_class($this));
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;

        $meta->save(FALSE);
    }

    private function getMeta()
    {
        return CommonHelper::getPageMetaForModel($this->id, get_class($this));
    }

}