<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= Tabs::widget([
        'items' => $tabs,
    ]); ?>

    <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>