<?php

use app\modules\core\widgets\AdminMenu\AdminMenu;

$this->title = 'My Yii Application';
?>
<div class="row">
    <div class="col-lg-4 col-md-4">

        <ul class="list-group">

            <?= AdminMenu::widget(); ?>

        </ul>

    </div>
    <div class="col-lg-8 col-md-8">

    </div>

</div>
