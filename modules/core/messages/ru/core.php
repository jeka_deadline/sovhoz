<?php
    return [
        'Create' => 'Создать',
        'Update' => 'Обновить',
        'Cancel' => 'Отмена',
        'Actions' => 'Действия',
        'Auto' => 'Калькулятор',
        'Date' => 'Дата',
        'Date from' => 'С даты',
        'Date to' => 'По дату',
        'Search' => 'Поиск',
        'Date/Time' => 'Дата/время',
        'Signature' => 'Подпись',
        'Access danied' => 'Доступ запрещен',
        'Yes' => 'Да',
        'No' => 'Нет',
        'Forbidden' => 'Запрещено',
        'Login' => 'Авторизироваться',
        'Delete' => 'Удалить',
        'Count' => 'Количество',
    ];
?>