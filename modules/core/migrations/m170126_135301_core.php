<?php

use yii\db\Migration;

class m170126_135301_core extends Migration
{
    public function safeUp()
    {
        // таблица соц.ссылок
        $this->createTable('{{%core_social_links}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(50)->notNull(),
            'image'         => $this->string(255)->defaultValue(NULL),
            'class'         => $this->string(255)->defaultValue(NULL),
            'url'           => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),

         ]);

        // таблица текстовых блоков
        $this->createTable('{{%core_text_block}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(100)->notNull(),
            'description'   => $this->string(255)->defaultValue(NULL),
            'uri'           => $this->string(255)->notNull(),
            'content'       => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица прикреплений
        $this->createTable('{{%core_attachments}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(255)->notNull(),
            'description'   => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'file'          => $this->string(255)->notNull(),
        ]);

        // таблица шаблонов
        $this->createTable('{{%core_templates}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(100)->notNull(),
            'path'          => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица страниц
        $this->createTable('{{%core_pages}}', [
            'id'            => $this->primaryKey(),
            'route'         => $this->string(255)->notNull(),
            'header'        => $this->string(255)->notNull(),
            'menu_class'    => $this->string(255)->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица мета-данных страниц
        $this->createTable('{{%core_pages_meta}}', [
            'id'                => $this->primaryKey(),
            'owner_id'          => $this->integer()->notNull(),
            'modelClass'        => $this->string(255)->notNull(),
            'meta_title'        => $this->string(255)->defaultValue(NULL),
            'meta_description'  => $this->text()->defaultValue(NULL),
            'meta_keywords'     => $this->text()->defaultValue(NULL),
        ]);

         // таблица для типов меню
        $this->createTable('{{%core_type_menu}}', [
            'id'            => $this->primaryKey(),
            'code'          => $this->string(255)->notNull(),
            'title'         => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица для связивания типов меню с таблицами страниц
        $this->createTable('{{%core_links_menu_page}}', [
            'id'          => $this->primaryKey(),
            'page_id'     => $this->integer(11)->notNull(),
            'type_id'     => $this->integer(11)->notNull(),
            'type_model'  => $this->string(255)->notNull(),
        ]);

        // таблица для типов элементов на форме
        $this->createTable('{{%core_types_field_form}}', [
            'id'          => $this->primaryKey(),
            'name_field'  => $this->string(255)->notNull(),
        ]);

        // таблица группы параметров
        $this->createTable('{{%core_groups_params}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(NULL),
        ]);

        // Таблица для параметров системы
        $this->createTable('{{%core_params}}', [
            'id'          => $this->primaryKey(),
            'type_id'     => $this->integer(11)->notNull(),
            'group_id'    => $this->integer(11)->notNull(),
            'name'        => $this->string(255)->notNull(),
            'code'        => $this->string(255),
            'description' => $this->text()->defaultValue(NULL),
            'value'       => $this->text()->defaultValue(NULL),
        ]);

        $this->createTable('{{%core_mail_templates}}', [
            'id'          => $this->primaryKey(),
            'code'        => $this->string(255)->notNull(),
            'title'       => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(NULL),
            'text'        => $this->text()->notNull(),
        ]);

        $this->createRelations();
        $this->insertCorePages();
        $this->insertTypesMenu();
        $this->insertTypesFieldForm();
        $this->insertGroupsParams();
        $this->insertDefaultParams();

    }

    private function createRelations()
    {
        // создание связи таблицы меню-страницы с таблицей типов меню
        $this->createIndex('ix_core_menu_pages_type_id', '{{%core_links_menu_page}}', 'type_id');
        $this->addForeignKey('fk_core_menu_pages_type_id', '{{%core_links_menu_page}}', 'type_id', '{{%core_type_menu}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('uix_core_text_block_uri', '{{%core_text_block}}', 'uri', TRUE);
        $this->createIndex('uix_core_pages_route', '{{%core_pages}}', 'route', TRUE);
        $this->createIndex('uix_core_type_menu_code', '{{%core_type_menu}}', 'code', TRUE);
        $this->createIndex('uix_core_params_code', '{{%core_params}}', 'code', TRUE);

        $this->createIndex('ix_core_params_type_id', '{{%core_params}}', 'type_id');
        $this->addForeignKey('fk_core_params_type_id', '{{%core_params}}', 'type_id', '{{%core_types_field_form}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('ix_core_params_group_id', '{{%core_params}}', 'group_id');
        $this->addForeignKey('fk_core_params_group_id', '{{%core_params}}', 'group_id', '{{%core_groups_params}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('uix_core_mail_templates_code', '{{%core_mail_templates}}', 'code', TRUE);
    }

    private function removeRelations()
    {
        $this->dropForeignKey('fk_core_menu_pages_type_id', '{{%core_links_menu_page}}');
        $this->dropIndex('ix_core_menu_pages_type_id', '{{%core_links_menu_page}}');
        $this->dropIndex('uix_core_text_block_uri', '{{%core_text_block}}');
        $this->dropIndex('uix_core_pages_route', '{{%core_pages}}');
        $this->dropIndex('uix_core_type_menu_code', '{{%core_type_menu}}');
        $this->dropIndex('uix_core_params_code', '{{%core_params}}');

        $this->dropForeignKey('fk_core_params_group_id', '{{%core_params}}');
        $this->dropIndex('ix_core_params_group_id', '{{%core_params}}');
        $this->dropForeignKey('fk_core_params_type_id', '{{%core_params}}');
        $this->dropIndex('ix_core_params_type_id', '{{%core_params}}');

        $this->dropIndex('uix_core_mail_templates_code', '{{%core_mail_templates}}');
    }

    private function insertTypesMenu()
    {
        $this->batchInsert('{{%core_type_menu}}', ['code', 'title', 'active'], [
            [
                'code'    => 'top',
                'tite'    => 'Верхнее меню',
                'active'  => 1,
            ],
            [
                'code'    => 'bottom',
                'tite'    => 'Нижнее меню',
                'active'  => 1,
            ]
        ]);
    }

    private function insertTypesFieldForm()
    {
        $this->batchInsert('{{%core_types_field_form}}', ['name_field'], [
            [
                'name_field' => 'checkbox',
            ],
            [
                'name_field' => 'text',
            ],
            [
                'name_field' => 'textarea',
            ],
            [
                'name_field' => 'radio',
            ],
        ]);
    }

    private function insertCorePages()
    {
        $this->insert('{{%core_pages}}', [
            'route'   => 'core/index/index',
            'header'  => 'Главная страница',
            'active'  => 1,
        ]);
    }

    private function insertGroupsParams()
    {
        $this->batchInsert('{{%core_groups_params}}', ['name', 'description'], [
            [
                'name'        => 'SMTP параметры',
                'description' => 'Данные для smtp сервера',
            ],
            [
                'name'        => 'Контакты для обратной связи',
                'description' => 'Данные для контактов сайта',
            ],
            [
                'name'        => 'Системные',
                'description' => 'Системные параметры',
            ],
            [
                'name'        => 'Другое',
                'description' => 'Другие параметры',
            ]
        ]);
    }

    private function insertDefaultParams()
    {
        $this->batchInsert('{{%core_params}}', ['type_id', 'group_id', 'name', 'code', 'description', 'value'], [
            [
                'type_id'     => 2,
                'group_id'    => 1,
                'name'        => 'Host',
                'code'        => 'smtp_host',
                'description' => 'Хост для smtp сервера',
                'value'       => 'smtp.yandex.ru',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 1,
                'name'        => 'Username',
                'code'        => 'smtp_username',
                'description' => 'Логин для smtp сервера',
                'value'       => 'jeka@yandex.ru',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 1,
                'name'        => 'SMTP password',
                'code'        => 'smtp_password',
                'description' => 'Пароль для smtp сервера',
                'value'       => '159753',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 1,
                'name'        => 'Port',
                'code'        => 'smtp_port',
                'description' => 'Порт для smtp сервера',
                'value'       => '465',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 1,
                'name'        => 'Encryption',
                'code'        => 'smtp_encryption',
                'description' => 'Шифрование для smtp сервера',
                'value'       => 'ssl',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 2,
                'name'        => 'Телефон для обратной связи',
                'code'        => 'core_phone',
                'description' => '',
                'value'       => '+380935994767',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 2,
                'name'        => 'Email для обратной связи',
                'code'        => 'core_email',
                'description' => '',
                'value'       => 'jeka.deadline@gmail.com',
            ],
            [
                'type_id'     => 3,
                'group_id'    => 2,
                'name'        => 'Адрес обратной связи',
                'code'        => 'core_address',
                'description' => '',
                'value'       => '',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 4,
                'name'        => 'Email тех. поддержки',
                'code'        => 'support_email',
                'description' => '',
                'value'       => 'jeka@yandex.ru',
            ],
            [
                'type_id'     => 2,
                'group_id'    => 4,
                'name'        => 'From',
                'code'        => 'support_from',
                'description' => 'От кого будут приходить письма',
                'value'       => 'Test',
            ],
        ]);
    }

    public function safeDown()
    {
        $this->removeRelations();

        $this->dropTable('{{%core_params}}');
        $this->dropTable('{{%core_links_menu_page}}');
        $this->dropTable('{{%core_type_menu}}');
        $this->dropTable('{{%core_pages_meta}}');
        $this->dropTable('{{%core_templates}}');
        $this->dropTable('{{%core_pages}}');
        $this->dropTable('{{%core_attachments}}');
        $this->dropTable('{{%core_text_block}}');
        $this->dropTable('{{%core_social_links}}');
        $this->dropTable('{{%core_types_field_form}}');
        $this->dropTable('{{%core_groups_params}}');
        $this->dropTable('{{%core_mail_templates}}');
    }
}