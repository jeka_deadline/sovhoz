<?php
namespace app\modules\user\controllers;

use Yii;
use app\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use app\modules\user\models\forms\CreateUserForm;
use app\modules\user\models\forms\CreateSellerForm;
use app\modules\user\models\forms\CreateClientForm;
use app\modules\user\models\User;
use app\modules\shop\models\Card;
use app\modules\shop\models\Client;
use app\modules\user\models\searchModels\UserSearch;
use app\modules\core\models\Helper;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

class UserController extends BackendController
{

    public $modelName   = 'app\modules\user\models\User';
    public $searchModel = 'app\modules\user\models\searchModels\UserSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'update-user-role', 'update-user-block', 'list-sellers'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->searchByRole(Yii::$app->request->queryParams, Yii::$app->user->identity->role_id);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'currentUserId' => Yii::$app->user->identity->getId(),
        ]);
    }

    public function actionUpdateUserRole($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($user->id === Yii::$app->user->identity->getId()) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'You can not change role for self'));

            return $this->redirect(['index']);
        }

        $user->updateAttributes(['is_admin' => !$user->is_admin]);

        return $this->redirect(['index']);
    }

    public function actionUpdateUserBlock($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($user->id === Yii::$app->user->identity->getId()) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'You can not change block for self'));

            return $this->redirect(['index']);
        }

        $valueBlock = ($user->blocked_at) ? NULL : new Expression('NOW()');

        $user->updateAttributes(['blocked_at' => $valueBlock]);

        return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($user->id === Yii::$app->user->identity->getId()) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'You can not delete self'));

            return $this->redirect(['index']);
        }

        $user->delete();

        Yii::$app->session->setFlash('success', Yii::t('user', 'User success delete'));

        return $this->redirect(['index']);
    }

    public function actionCreate($role)
    {
        $roleId = Yii::$app->user->identity->role_id;

        if ($roleId == User::ROLE_MANAGER) {
            switch ($role) {
                case User::ROLE_MANAGER:
                case User::ROLE_ADMIN:
                    throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
            }
        }

        if ($roleId == User::ROLE_OPERATOR) {
            switch ($role) {
                case User::ROLE_MANAGER:
                case User::ROLE_OPERATOR:
                case User::ROLE_ADMIN:
                    throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
            }
        }

        switch ($role) {
            case User::ROLE_ADMIN:
            case User::ROLE_MANAGER:
            case User::ROLE_OPERATOR:
                $model = new CreateUserForm(NULL, ['scenario' => CreateUserForm::SCENARIO_CREATE]);
                break;
            case User::ROLE_CLIENT:
                $model = new CreateClientForm(NULL, ['scenario' => 'not_client']);
                break;
            case User::ROLE_SELLER:
                $model = new CreateSellerForm(NULL, ['scenario' => CreateUserForm::SCENARIO_CREATE]);
                break;
            default:
                throw new NotFoundHttpException(Yii::t('user', "Role not found"));
        }

        if ($model->load(Yii::$app->request->post()) && $model->createUser($role)) {
            return $this->redirect(['index']);
        }

        return $this->render('create-update-user-form', [
            'model' => $model,
            'role'  => $role,
        ]);
    }

    public function actionUpdate($id)
    {
        $user = User::find()
                          ->where(['id' => $id])
                          ->with('profile')
                          ->one();

        $currentUserRoleId = Yii::$app->user->identity->role_id;

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($currentUserRoleId === User::ROLE_MANAGER && $user->role_id !== User::ROLE_SELLER) {
            throw new ForbiddenHttpException(Yii::t('core', 'Access danied'), 403);
        }

        switch ($user->role_id) {
            case User::ROLE_ADMIN:
            case User::ROLE_MANAGER:
            case User::ROLE_OPERATOR:
                $model = new CreateUserForm($user);
                break;
            case User::ROLE_CLIENT:
                $model = new CreateClientForm($user);
                break;
            case User::ROLE_SELLER:
                $model = new CreateSellerForm($user);
                break;
            default:
                throw new NotFoundHttpException(Yii::t('user', "Role not found"));
        }

        if ($model->load(Yii::$app->request->post()) && $model->updateUser()) {
            return $this->redirect(['index']);
        }

        return $this->render('create-update-user-form', [
            'model' => $model,
            'role' => $user->role_id

        ]);
    }

    public function actionView($id)
    {
        $user = User::find()
                          ->where(['id' => $id])
                          ->with('profile')
                          ->one();
        $card_user = null;
        if ($user->role_id == 3)
        {
        $client = Client::find()
                          ->where(['user_id' => $id])
                          ->one();
        $card_user = Card::find()
                          ->where(['id' => $client->card_id])
                          ->one();
          if (!$client) {
                    $card_user = null;
                }
                if (!$card_user) {
                    $card_user = null;
                }
        }
                if (!$user) {
                    throw new NotFoundHttpException(Yii::t('user', "User not found"));
                }

                return $this->render('view', [
                'model' => $user,
                'model_card' => $card_user,
                ]);
    }

    public function actionListSellers()
    {
        $query = User::find()->where(['role_id' => User::ROLE_SELLER]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('list-sellers', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
            User::ROLE_MANAGER => [
                'access-actions' => ['view', 'update', 'index', 'list-sellers', 'create'],
            ],
            User::ROLE_OPERATOR => [
                'access-actions' => ['list-sellers', 'create', 'index'],
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

}