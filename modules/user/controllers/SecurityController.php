<?php
namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\forms\LoginSellerForm;
use app\modules\user\models\User;


class SecurityController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin($type = NULL)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model            = new LoginForm();
        $sellerLoginModel = new LoginSellerForm();
        $currentModel     = ($type) ? $sellerLoginModel : $model;

        if ($currentModel->load(Yii::$app->request->post()) && $currentModel->login()) {
            return $this->goBack();
        } else {

            return $this->render('login', [
                'model'             => $model,
                'sellerLoginModel'  => $sellerLoginModel,
                'type'              => $type,
            ]);
        }
    }

    public function actionLogout()
    {
        $id = Yii::$app->user->identity->id;
        $user = User::find()
                          ->where(['id' => $id])
                          ->one();
        $user->cookies = null;
        $user->now_in_shop = null;
        $user->save();
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('name');
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
