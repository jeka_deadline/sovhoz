<?php
namespace app\modules\user\models\forms;

use Yii;
use app\modules\user\models\forms\CreateUserForm;
use app\modules\shop\models\Client;
use app\modules\shop\models\Card;
use yii\helpers\ArrayHelper;

class CreateClientForm extends CreateUserForm
{

    public $cardId;
    public $typeCard;
    public $initCardValue;

    private $_card;
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['login', 'email', 'surname', 'name', 'patronymic', 'gender', 'password'], 'string', 'on' =>'not_client'],
                [['cardId'], 'exist', 'targetClass' => Card::className(), 'targetAttribute' => 'id', 'filter' => function($query)
                {
                    return $query->andWhere(['is', 'type', NULL]);
                }, 'on' => self::SCENARIO_CREATE],
                [['typeCard'], 'in', 'range' => ['bonus', 'discount'], 'on' => self::SCENARIO_CREATE],
                [['cardId'], 'required', 'when' => function($model) {
                    return !empty($this->typeCard);
                }, 'whenClient' => "function (attribute, value) { return $('#typeCard').val() != '';}", 'on' => self::SCENARIO_CREATE],
                [['typeCard'], 'required', 'when' => function($model) {
                    return !empty($this->cardId);
                }, 'whenClient' => "function (attribute, value) { return $('#cardId').val() != '';}", 'on' => self::SCENARIO_CREATE],
                [['cardId'], 'exist', 'targetClass' => Card::className(), 'targetAttribute' => 'id', 'filter' => function($query)
                {
                    return $query->andWhere(['is', 'type', NULL]);
                }, 'on' => 'not_client'],
                [['typeCard'], 'in', 'range' => ['bonus', 'discount'], 'on' => 'not_client'],
                [['cardId'], 'required', 'when' => function($model) {
                    return !empty($this->typeCard);
                }, 'whenClient' => "function (attribute, value) { return $('#typeCard').val() != '';}", 'on' => 'not_client'],
                [['typeCard'], 'required', 'when' => function($model) {
                    return !empty($this->cardId);
                }, 'whenClient' => "function (attribute, value) { return $('#cardId').val() != '';}", 'on' => 'not_client'],
                [['initCardValue'], 'integer'],
                [['initCardValue'], 'default', 'value' => 0],
                
            ]
        );
    }
    
    public function createUser($role)
    {
        if (!$this->validate() || !$userId = parent::createUser($role)) {
            
            return FALSE;
        }
    
        $client           = new Client();
        $client->user_id  = $userId;
        $typeCard         = Card::checkTypeCard($this->typeCard);
       
        if ($this->cardId && $typeCard && $card = $this->getCard()) {
            $client->card_id = $card->id;

            $updateCardAttributes = [
                'type' => $typeCard,
            ];

            if ($this->initCardValue) {
                $updateCardAttributes[ 'value' ] = $this->initCardValue;
            }

            $card->updateAttributes($updateCardAttributes);
        }

        $client->save();

        return TRUE;
    }

    public function getListCards()
    {
        return ArrayHelper::map(Card::find()->andWhere(['is', 'type', NULL])->all(), 'id', 'number');
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'typeCard' => Yii::t('card', 'Type card'),
                'cardId' => Yii::t('card', 'Card'),
                'initCardValue' => Yii::t('card', 'Init card value'),
            ]
        );
    }

    private function getCard()
    {
        if (!$this->_card) {
            $this->_card = Card::findOne($this->cardId);
        }

        return $this->_card;
    }

}