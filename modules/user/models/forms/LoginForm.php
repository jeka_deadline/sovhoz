<?php
namespace app\modules\user\models\forms;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
use yii\web\Request;
use yii\helpers;
use yii\web\Cookie;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = false;

    protected $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();

            if ($user && $user->role_id === User::ROLE_ADMIN) {
                return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            }

            $dateInSystem =  new \DateTime('now');
            $nextDataDay = new \DateTime('now');
            $nextDataDay->modify('+1 DAY');
            $nextDataDay->setTime(0,0,0);
            $timeLiveCookie = strtotime($nextDataDay->format('Y-m-d H:i:s')) -
                              strtotime($dateInSystem->format('Y-m-d H:i:s'));
            $cookies = Yii::$app->request->cookies;
            if ($cookies->getValue('name')) {
                if ($cookies->getValue('name') == $user->cookies ) { return true; }
            }
            if (!$cookies->getValue('name') && $user->cookies == null) {

                 $cookies = Yii::$app->response->cookies;
                 $cookies->add(new \yii\web\Cookie(
                           ['name' => 'name',
                            'value' => $dateInSystem->format('Y-m-d'),
                            'expire' => time() + $timeLiveCookie
                           ]));

                 $user->cookies = $dateInSystem->format('Y-m-d');
                 $user->save();

            }
            if (!$cookies->getValue('name') && $user->cookies != null && $user->cookies != $dateInSystem->format('Y-m-d'))
            {
                 $cookies = Yii::$app->response->cookies;
                 $cookies->add(new \yii\web\Cookie(
                           ['name' => 'name',
                            'value' => $dateInSystem->format('Y-m-d'),
                            'expire' => time() + $timeLiveCookie
                           ]));

                 $user->cookies = $dateInSystem->format('Y-m-d');
                 $user->save();

            }
            if (!$cookies->getValue('name') && $user->cookies != null) {
                return false;
            }
            if ($user && $user->role_id === User::ROLE_SELLER) {
                return FALSE;
            }

            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('user', 'Email'),
            'password' => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Remember me'),
        ];
    }
}
