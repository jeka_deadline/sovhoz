<?php
namespace app\modules\user\models\forms;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
use app\modules\user\models\forms\LoginForm;
use yii\helpers\ArrayHelper;
use app\modules\shop\models\LinksSellerShop;
use app\modules\shop\models\Shop;
use app\modules\shop\models\SellerStat;
use yii\db\Expression;

/**
 * Login form
 */
class LoginSellerForm extends LoginForm
{
    public $shopId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['shopId'], 'required'],
                [['shopId'], 'number'],
                [['shopId'], 'validateShop'],
            ]
        );
    }

    public function validateShop()
    {
        $user = $this->getUser();
        if (!$user) {
            $this->addError('email', Yii::t('user', 'User not found'));
            return FALSE;
        }

        $listShops = ArrayHelper::map(LinksSellerShop::find()->where(['user_id' => $user->id])->all(), 'shop_id', 'shop_id');

        if (!isset($listShops[ $this->shopId ])) {
            $this->addError('shopId', Yii::t('user', 'You do not belong to this shop'));
            return FALSE;
        }

        return TRUE;

    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $dateInSystem =  new \DateTime('now');
            $nextDataDay = new \DateTime('now');
            $nextDataDay->modify('+1 DAY');
            $nextDataDay->setTime(0,0,0);
            $timeLiveCookie = strtotime($nextDataDay->format('Y-m-d H:i:s')) -
                              strtotime($dateInSystem->format('Y-m-d H:i:s'));
            $cookies = Yii::$app->request->cookies;
            if ($cookies->getValue('name')) {
                if ($cookies->getValue('name') != $user->cookies ) { return false; }
                  

            }
            if (!$cookies->getValue('name') && $user->cookies == null) {

                 $cookies = Yii::$app->response->cookies;
                 $cookies->add(new \yii\web\Cookie(
                           ['name' => 'name',
                            'value' => $dateInSystem->format('Y-m-d'),
                            'expire' => time() + $timeLiveCookie 
                           ]));

                 $user->cookies = $dateInSystem->format('Y-m-d');
                 $user->save(); 
            } 
            if (!$cookies->getValue('name') && $user->cookies != null && $user->cookies != $dateInSystem->format('Y-m-d')) 
            {
                 $cookies = Yii::$app->response->cookies;
                 $cookies->add(new \yii\web\Cookie(
                           ['name' => 'name',
                            'value' => $dateInSystem->format('Y-m-d'),
                            'expire' => time() + $timeLiveCookie 
                           ]));

                 $user->cookies = $dateInSystem->format('Y-m-d');
                 $user->save(); 
            }
            if (!$cookies->getValue('name') && $user->cookies != null) {
                return false;
            }
            if ($user && $user->role_id !== User::ROLE_SELLER) {
                return FALSE;
            }
            $login = Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);

            if ($login) { 
                $model              = new SellerStat();
                $model->user_id     = $user->id;
                $model->start_date  = new Expression('NOW()');
                $model->shop_id     = $this->shopId;
                $user = $this->getUser(); 
                $user->now_in_shop = $model->shop_id;
                $user->save();
                if (!$model->validate() || !$model->save()) {
                    $login = FALSE;
                    Yii::$app->user->logout();
                }
            }

            return $login;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'shopId' => Yii::t('shop', 'Shop'),
            ]
        );
    }

    public function getListShops()
    {
        return ArrayHelper::map(Shop::find()->all(), 'id', 'name');
    }
}
