<?php
namespace app\modules\user\models\forms;

use Yii;
use app\modules\user\models\forms\CreateUserForm;
use app\modules\shop\models\LinksSellerShop;
use app\modules\shop\models\Shop;
use yii\helpers\ArrayHelper;
use app\modules\user\models\User;

class CreateSellerForm extends CreateUserForm
{

    public $shopIds;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['shopIds'], 'required'],
               
            ]
        );
    }

    public function createUser($role)
    {
        if (!$this->validate()) {
            return FALSE;
        }

        if ($userId = parent::createUser($role)) {
        
            if (!is_array($this->shopIds)) {
                $this->shopIds = [ $this->shopIds ];
            }

            foreach ($this->shopIds as $shopId) {
                $model          = new LinksSellerShop();
                $model->user_id = $userId;
                $model->shop_id = $shopId;

                $model->save();
            }

            return TRUE;
        }
    }

    public function updateUser()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        if (!parent::updateUser()) {
            return FALSE;
        }

        $linksShopIds = ArrayHelper::map($this->_user->shopsBySeller, 'shop_id', function($model){ return $model; });

        if (!is_array($this->shopIds)) {
            $this->shopIds = [ $this->shopIds ];
        }

        foreach ($this->shopIds as $shopId) {
            if (isset($linksShopIds[ $shopId ])) {
                unset($linksShopIds[ $shopId ]);
                continue;
            }

            $model          = new LinksSellerShop();
            $model->user_id = $this->_user->id;
            $model->shop_id = $shopId;

            $model->save();
        }

        foreach ($linksShopIds as $model) {
            $model->delete();
        }

        return TRUE;
    }

    public function getShopsList()
    {
        return ArrayHelper::map(Shop::find()->all(), 'id', 'name');
    }

    public function setFields(User $user)
    {
        parent::setFields($user);
        $this->shopIds = ArrayHelper::map($user->shopsBySeller, 'id', 'shop_id');
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'shopIds' => Yii::t('shop', 'Shops'),
            ]
        );
    }
}