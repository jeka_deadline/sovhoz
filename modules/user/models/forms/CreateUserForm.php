<?php
namespace app\modules\user\models\forms;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use yii\db\Expression;

class CreateUserForm extends Model
{

    // user fields
    public $login;
    public $email;
    public $password;
    public $repeatPassword;
    public $isConfirmEmail;

    // profile fields
    public $surname;
    public $name;
    public $patronymic;
    public $phone;
    public $dateBirth;
    public $gender;

    protected $_user;

    const SCENARIO_CREATE = 'create';

    public function __construct($user = NULL, $options = [])
    {
        if ($user instanceof User) {
            $this->_user = $user;

            $this->setFields($user);
        }

        parent::__construct($options);
    }
    public function rules()
    {
        return [
            // user rules
            [['login', 'email', 'surname', 'name', 'patronymic', 'gender'], 'required', 'on' => self::SCENARIO_CREATE],
            [['login'], 'string', 'min' => 3],
            [['login'], 'match', 'pattern' => '/^[a-z][a-z\d_-]+[a-z\d]$/i', 'message' => Yii::t('user', 'Login must consist only "a-z 0-9 _-"'), 'on' => self::SCENARIO_CREATE],
            [['password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_CREATE],
            [['repeatPassword'], 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_CREATE],
            [['login', 'email'], 'filter', 'filter' => 'trim'],
            [['email'], 'string', 'max' => 100, 'on' => 'not_client'],
            [['email'], 'email'],
            [['login'], 'string', 'max' => 50],
            [['email'], 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email', 'filter' => function($query) {
                if ($this->getScenario() === self::SCENARIO_CREATE) {
                    return $query;
                }

                return $query->andWhere(['<>', 'id', $this->_user->id]);
            }, 'on' => self::SCENARIO_CREATE],
            [['login'], 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'login', 'filter' => function($query) {
                if ($this->getScenario() === self::SCENARIO_CREATE) {
                    return $query;
                }

                return $query->andWhere(['<>', 'id', $this->_user->id]);
            }],
            [['password', 'repeatPassword'], 'string', 'max' => 100],
            [['isConfirmEmail'], 'boolean'],

            // profile rules
            [['surname', 'name', 'patronymic', 'phone'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'patronymic'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 15],
            [['gender'], 'in', 'range' => ['male', 'female']],
            [['dateBirth'], 'safe'],
        ];
    }

    public function createUser($role)
    {
        if (!$this->validate()) {
            return FALSE;
        }
        
           
        $user = new User();
        $user->login            = $this->login;
        $user->email            = $this->email;
        $user->confirm_email_at = ($this->isConfirmEmail) ? new Expression('NOW()') : NULL;
        $user->role_id          = $role;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->setRegisterIp();
        
        if ($user->validate() && $user->save()) {
            $profile              = new Profile();
            $profile->attributes  = $this->attributes;
            $profile->date_birth  = $this->dateBirth;

            $user->link('profile', $profile);

            return $user->id;
        }

        return FALSE;
    }

    public function updateUser()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $user         = $this->_user;
        $user->email  = $this->email;
        $user->login  = $this->login;

        if ($this->password) {
            $user->setPassword($this->password);
        }

        foreach ($user->profile->attributes as $attributeName => $attributeValue) {
            if (!property_exists($this, $attributeName)) {
                continue;
            }

            $user->profile->$attributeName = $this->$attributeName;
        }

        $user->profile->date_birth = $this->dateBirth;

        if ($user->save() && $user->profile->save()) {
            return $user->id;
        }

        return FALSE;

    }

    public function setFields(User $user)
    {
        $this->email = $user->email;
        $this->login = $user->login;

        foreach ($user->profile->attributes as $attributeName => $attributeValue) {
            if (!property_exists($this, $attributeName)) {
                continue;
            }

            $this->$attributeName = $attributeValue;
        }
        $this->dateBirth = $user->profile->date_birth;
    }

    public static function getListRoles()
    {
        return [
            User::ROLE_ADMIN  => Yii::t('user', 'Administrator'),
            User::ROLE_SELLER => Yii::t('user', 'Seller'),
            User::ROLE_CLIENT => Yii::t('user', 'Client'),
        ];
    }
    public function getGenderList()
    {
        return [
            'male'    => Yii::t('user', 'Male'),
            'female'  => Yii::t('user', 'Female'),
        ];
    }

    public function attributeLabels()
    {
        return [
            'login'           => Yii::t('user', 'Login'),
            'password'        => Yii::t('user', 'Password'),
            'email'           => Yii::t('user', 'Email'),
            'repeatPassword'  => Yii::t('user', 'Repeat password'),
            'isConfirmEmail'  => Yii::t('user', 'Is confirm email'),
            'surname'         => Yii::t('user', 'Surname'),
            'name'            => Yii::t('user', 'Name'),
            'patronymic'      => Yii::t('user', 'Patronymic'),
            'phone'           => Yii::t('user', 'Phone'),
            'dateBirth'       => Yii::t('user', 'Date birth'),
            'gender'          => Yii::t('user', 'Gender'),
        ];
    }
}