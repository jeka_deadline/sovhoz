<?php

namespace app\modules\user\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * UserSearch represents the model behind the search form about `app\modules\user\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public $surname;
    public $phone;
    public function rules()
    {
        return [
            [['id', 'role_id', 'reset_password_token'], 'integer'],
            [['email', 'login', 'password_hash', 'auth_key', 'blocked_at', 'confirm_email_at', 'register_ip', 'created_at', 'updated_at'], 'safe'],
            ['surname', 'string'],
            ['phone','string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()->joinWith('profile');
        if ($this->phone)
        {
            $query->andWhere([Profile::tableName() . '.`phone`' => $this->phone]);

        }
        if ($this->surname)
        {
            $query->andWhere([Profile::tableName() . '.`surname`' => $this->surname]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'reset_password_token' => $this->reset_password_token,
            'blocked_at' => $this->blocked_at,
            'confirm_email_at' => $this->confirm_email_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'surname' => $this->surname,
            'phone' => $this->phone,
        ]);

        $query
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'register_ip', $this->register_ip]);

        return $dataProvider;
    }

    public function searchByRole($params, $role)
    {
        $query = static::find()->joinWith('profile');
        if ($this->phone)
        {
            $query->andWhere([Profile::tableName() . '.`phone`' => $this->phone]);

        }
        if ($this->surname)
        {
            $query->andWhere([Profile::tableName() . '.`surname`' => $this->surname]);
        }

        switch ($role) {
            case User::ROLE_OPERATOR:
                $query->andWhere(['not', ['role_id' => User::ROLE_CLIENT]]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'reset_password_token' => $this->reset_password_token,
            'blocked_at' => $this->blocked_at,
            'confirm_email_at' => $this->confirm_email_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'surname' => $this->surname,
            'phone' => $this->phone,
        ]);

        $query
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'register_ip', $this->register_ip]);

        return $dataProvider;
    }
}
