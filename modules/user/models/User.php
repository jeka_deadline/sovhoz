<?php
namespace app\modules\user\models;

use Yii;
use app\models\user\User as BaseUser;
use app\models\shop\LinksSellerShop;
use yii\helpers\ArrayHelper;
use app\modules\shop\models\Shop;

class User extends BaseUser
{

    const ROLE_ADMIN    = 1;
    const ROLE_SELLER   = 2;
    const ROLE_CLIENT   = 3;
    const ROLE_OPERATOR = 4;
    const ROLE_MANAGER  = 5;

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function isAdmin()
    {
        return ($this->role_id === self::ROLE_ADMIN);
    }

    public function isSeller()
    {
        return ($this->role_id === self::ROLE_SELLER);
    }

    public function isManeger()
    {
        return ($this->role_id === self::ROLE_MANAGER);
    }

    public function isOperator()
    {
        return ($this->role_id === self::ROLE_OPERATOR);
    }

    public function getRoleName()
    {
        switch ($this->role_id) {
            case self::ROLE_ADMIN:
                return Yii::t('user', 'Administrator');
            case self::ROLE_SELLER:
                return Yii::t('user', 'Seller');
            case self::ROLE_CLIENT:
                return Yii::t('user', 'Client');
            case self::ROLE_OPERATOR:
                return Yii::t('user', 'Operator');
            case self::ROLE_MANAGER:
                return Yii::t('user', 'Manager');
        }

        return NULL;
    }

    public function attributeLabels()
    {
        return [
            'role_id' => Yii::t('user', 'Role'),
            'login' => Yii::t('user', 'Login'),
            'email' => Yii::t('user', 'Email'),
            'surname' =>Yii::t('user', 'Surname'),
            'phone' =>Yii::t('user', 'Phone'),
        ];
    }

    public function getListNameShopsForSeller()
    {
        if ($this->role_id != self::ROLE_SELLER) {
            return NULL;
        }

        return implode(', ', ArrayHelper::map($this->listShops, 'id', 'name'));
    }

    // ================================================================== Relations =========================================================================

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    public function getSocials()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

    public function getShopsBySeller()
    {
        return $this->hasMany(LinksSellerShop::className(), ['user_id' => 'id']);
    }

    public function getListShops()
    {
        return $this->hasMany(Shop::className(), ['id' => 'shop_id'])
                    ->viaTable(LinksSellerShop::tableName(), ['user_id' => 'id']);
    }


}