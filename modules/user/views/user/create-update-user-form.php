<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use app\modules\user\models\forms\CreateUserForm;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label'   => Yii::t('user', 'User information'),
                'content' => $this->render('_block_user_information', [
                    'form' => $form,
                    'model' => $model,
                    'role' => $role,
                ]),
            ],
            [
                'label'   => Yii::t('user', 'Profile information'),
                'content' => $this->render('_block_profile_information', [
                    'form' => $form,
                    'model' => $model,
                    'role' => $role,
                ]),
            ]
        ],
    ]); ?>
<?php if ($role == 3){ ?>
    <?= Html::submitButton(($model->getScenario() === 'not_client') ? Yii::t('user', 'Create user') : Yii::t('user', 'Update user'), [
        'class' => 'btn btn-primary'
    ]); ?>
    <?= Html::a(Yii::t('core', 'Cancel'), ['index'], ['class' => 'btn btn-default']); ?>    


<?php } else { ?>
<?= Html::submitButton(($model->getScenario() === CreateUserForm::SCENARIO_CREATE) ? Yii::t('user', 'Create user') : Yii::t('user', 'Update user'), [
        'class' => 'btn btn-primary'
    ]); ?>
    <?= Html::a(Yii::t('core', 'Cancel'), ['index'], ['class' => 'btn btn-default']); ?>    
<?php } ?>
<?php ActiveForm::end();?>