<?php
use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'label' => 'ФИО',
            'attribute' => 'profile',
            'value' => function($model) {return $model->profile->surname . ' ' . $model->profile->name . ' '  . $model->profile->patronymic ; },
        ],
        [
            'label' => 'Дата рождения',
            'attribute' => 'profile.date_birth',
        ],
        [
            'label' => 'Телефон',
            'attribute' => 'profile.phone',
        ],
        [
            'label' => 'Магазины',
            'attribute' => 'profile',
            'value' => function($model){ return $model->getListNameShopsForSeller(); }
        ],
    ],
]); ?>