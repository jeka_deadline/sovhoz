<?= $form->field($model, 'surname')->textInput(); ?>

<?= $form->field($model, 'name')->textInput(); ?>

<?= $form->field($model, 'patronymic')->textInput(); ?>

<?= $form->field($model, 'gender')->dropDownList($model->getGenderList(), ['prompt' => Yii::t('user', 'Choose gender')]); ?>

<?= $form->field($model, 'phone')->textInput(); ?>

<?= $form->field($model, 'dateBirth')->widget('trntv\yii\datetime\DateTimeWidget', [
    'phpDatetimeFormat' => 'yyyy-MM-dd',
]); ?>