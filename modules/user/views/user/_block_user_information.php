<?php
use app\modules\user\models\forms\CreateUserForm;
use app\modules\user\models\User;
?>

<?php if ((int)$role === User::ROLE_SELLER) : ?>

    <?= $this->render('_block_seller_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>
    

<?php endif; ?>

<?php if (((int)$role === User::ROLE_CLIENT) && ($model->getScenario() === 'not_client')) : ?>

    <?= $this->render('_block_client_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>

<?php endif; ?>

<?php if (((int)$role !== User::ROLE_CLIENT) && ($model->getScenario() === 'not_client')) : ?>

    <?= $form->field($model, 'login')->textInput(); ?>
    <?= $form->field($model, 'email')->textInput(); ?>
    <?= $form->field($model, 'password')->passwordInput(); ?>

    <?php if ($model->getScenario() === CreateUserForm::SCENARIO_CREATE) : ?>
        <?= $form->field($model, 'repeatPassword')->passwordInput(); ?>
        <?= $form->field($model, 'isConfirmEmail')->checkbox(); ?>



    <?php endif; ?>

<?php endif; ?>
<?php if ((int)$role !== User::ROLE_CLIENT) : ?>

    <?= $form->field($model, 'login')->textInput(); ?>
    <?= $form->field($model, 'email')->textInput(); ?>
    <?= $form->field($model, 'password')->passwordInput(); ?>
    <?= $form->field($model, 'repeatPassword')->passwordInput(); ?>
    <?= $form->field($model, 'isConfirmEmail')->checkbox(); ?>
<?php endif; ?>