<?php
use kartik\select2\Select2;
?>

<?= $form->field($model, 'shopIds')->widget(Select2::classname(), [
    'data' => $model->getShopsList(),
    'language' => 'ru',
    'options' => ['placeholder' =>  Yii::t('shop' , 'Choose shop'), 'multiple' => TRUE],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]); ?>