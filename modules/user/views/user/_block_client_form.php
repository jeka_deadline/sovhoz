<?php
use app\modules\shop\models\Card;
use kartik\select2\Select2;
?>

<?= $form->field($model, 'typeCard')->widget(Select2::classname(), [
    'data' => Card::getListTypesCard(),
    'language' => 'ru',
    'options' => ['placeholder' =>  Yii::t('card' , 'Choose type card'), 'id' => 'typeCard'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

<?= $form->field($model, 'cardId')->widget(Select2::classname(), [
    'data' => $model->getListCards(),
    'language' => 'ru',
    'options' => ['placeholder' =>  Yii::t('card' , 'Choose card'), 'id' => 'cardId'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

<?= $form->field($model, 'initCardValue'); ?>
