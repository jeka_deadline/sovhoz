<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
?>

<?= Html::a(Yii::t('core', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>

<br>
<br>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'login',
        'email',
        [
            'label' => Yii::t('user', 'Is user block'),
            'value' => ($model->blocked_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        [
            'label' => Yii::t('user', 'Is user confirm email'),
            'value' => ($model->confirm_email_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        [
            'label' => Yii::t('user', 'Surname'),
            'value' => $model->profile->surname,
        ],
        [
            'label' => Yii::t('user', 'Name'),
            'value' => $model->profile->name,
        ],
        [
            'label' => Yii::t('user', 'Patronymic'),
            'value' => $model->profile->patronymic,
        ],
        [
            'label' => Yii::t('user', 'Date birth'),
            'value' => $model->profile->date_birth,
        ],
    ],
]); ?>
<br>
<br>
<?php if ($model_card != null) { ?>

   <table class="table">
        <thead>
            <th><?= Yii::t('card', 'Card number'); ?></th>
            <th><?= Yii::t('card', 'Type card'); ?></th>
            <th><?= Yii::t('card', 'Value'); ?></th>
            <th><?= Yii::t('card', 'Bonus sum'); ?></th>
            <th><?= Yii::t('card', 'Total sum sale'); ?></th>
        </thead>
        <tbody>
            <td>
                <?= $model_card->number; ?>
            </td>
            <td>
                <?= $model_card->getTypeCard(); ?>
            </td>
            <td>
                <?= $model_card->value; ?>
            </td>
            <td>
                <?= $model_card->bonus_sum; ?>
            </td>
            <td>
                <?= $model_card->total; ?>
            </td>
        </tbody>
    </table> 
<?}?>

