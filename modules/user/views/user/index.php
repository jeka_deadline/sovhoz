<?php
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\user\models\forms\CreateUserForm;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
?>

<div class="btn-group">

    <?= Html::a(Yii::t('user', 'Create administrator'), ['create', 'role' => User::ROLE_ADMIN], ['class' => 'btn btn-primary']); ?>
    <?= Html::a(Yii::t('user', 'Create manager'), ['create', 'role' => User::ROLE_MANAGER], ['class' => 'btn btn-primary']); ?>
    <?= Html::a(Yii::t('user', 'Create operator'), ['create', 'role' => User::ROLE_OPERATOR], ['class' => 'btn btn-primary']); ?>
    <?= Html::a(Yii::t('user', 'Create seller'), ['create', 'role' => User::ROLE_SELLER], ['class' => 'btn btn-primary']); ?>
    <?= Html::a(Yii::t('user', 'Create client'), ['create', 'role' => User::ROLE_CLIENT], ['class' => 'btn btn-primary']); ?>

</div>

<br>
<br>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'surname',
            'value' => function($model) {return $model->profile->surname;},
        ],
        [
            'attribute' => 'phone',
            'value' => function($model) {return $model->profile->phone;},
            
        ],
        [
            'attribute' => 'role_id',
            'value'     => function($model){ return $model->getRoleName(); },
            'filter'    => CreateUserForm::getListRoles(),
        ],
        [
            'format'    => 'raw',
            'header'    => Yii::t('user', 'Security actions'),
            'value'     => function($model) use ($currentUserId)
            {
                if ($model->id === $currentUserId) {
                    return FALSE;
                }

                $optionsRole = $optionsBlock = [
                    'class' => 'btn btn-xs',
                ];

                if ($model->blocked_at) {
                    $textBtnBlock  = Yii::t('user', 'Unblock user');
                    $classBtnBlock = 'btn-danger';
                } else {
                    $textBtnBlock  = Yii::t('user', 'Block user');
                    $classBtnBlock = 'btn-success';
                }

                Html::addCssClass($optionsBlock, $classBtnBlock);

                return
                    '<div class="btn-group">' .
                        Html::a($textBtnBlock, Url::toRoute(['/user/user/update-user-block', 'id' => $model->id]), $optionsBlock) .
                    '</div>';
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('core', 'Actions')
        ],
    ],
]);