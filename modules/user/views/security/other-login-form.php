<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="col-lg-5">

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'autocomplete' => 'off']); ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('core', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>