<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="col-lg-5">

    <?php $form = ActiveForm::begin(['action' => ['login', 'type' => 'seller']]); ?>

        <?= $form->field($sellerLoginModel, 'email')->textInput(['autocomplete' => 'off']); ?>

        <?= $form->field($sellerLoginModel, 'shopId')->dropDownList($sellerLoginModel->getListShops(), ['prompt' => Yii::t('shop', 'Choose shop')]); ?>

        <?= $form->field($sellerLoginModel, 'password')->passwordInput() ?>

        <?= $form->field($sellerLoginModel, 'rememberMe')->checkbox() ?>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('core', 'Login'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>