<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;

$this->title = Yii::t('core', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">

        <?= Tabs::widget([
            'items' => [
                [
                    'label' => Yii::t('user', 'Other login'),
                    'content' => $this->render('other-login-form', ['model' => $model]),
                    'active' => (empty(Yii::$app->request->get('type'))),
                ],
                [
                    'label' => Yii::t('user', 'Seller login'),
                    'content' => $this->render('seller-login-form', ['sellerLoginModel' => $sellerLoginModel]),
                    'active' => (!empty(Yii::$app->request->get('type'))),
                ],
            ],
        ]); ?>

    </div>
</div>
