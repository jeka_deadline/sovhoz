<?php

use yii\db\Migration;

class m170531_091833_fix_date_birth_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%user_profiles}}', 'date_birth', $this->datetime()->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170531_091833_fix_date_birth_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
