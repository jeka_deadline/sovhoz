<?php

use yii\db\Migration;
use yii\db\Expression;

class m160331_140545_users extends Migration
{
    public function safeUp()
    {

      // таблица пользователей
      $this->createTable('{{%user_users}}', [
          'id'                    => $this->primaryKey(),
          'role_id'               => $this->smallInteger(1)->defaultValue(0),
          'email'                 => $this->string(100)->notNull(),
          'login'                 => $this->string(50)->notNull(),
          'password_hash'         => $this->string(100)->notNull(),
          'reset_password_token'  => $this->integer()->defaultValue(NULL),
          'auth_key'              => $this->string(32)->notNull(),
          'blocked_at'            => $this->timestamp()->defaultValue(NULL),
          'confirm_email_at'      => $this->timestamp()->defaultValue(NULL),
          'register_ip'           => $this->string(15)->notNull(),
          'created_at'            => $this->timestamp()->defaultValue(new Expression('NOW()')),
          'updated_at'            => $this->timestamp()->defaultValue(new Expression('NOW()')),
      ]);

      // таблица профиля пользователя
      $this->createTable('{{%user_profiles}}', [
          'id'          => $this->primaryKey(),
          'user_id'     => $this->integer(11)->notNull(),
          'surname'     => $this->string(100)->defaultValue(NULL),
          'name'        => $this->string(100)->defaultValue(NULL),
          'patronymic'  => $this->string(100)->defaultValue(NULL),
          'date_birth'  => $this->timestamp()->defaultValue(NULL),
          'gender'      => $this->string(6)->notNull(),
          'phone'       => $this->string(15)->defaultValue(NULL),

      ]);

      $this->createRelations();
      $this->createAdmins();

    }

    public function safeDown()
    {
        $this->removeRelations();
        $this->dropTable('{{%user_profiles}}');
        $this->dropTable('{{%user_users}}');
    }

    private function createRelations()
    {
        $this->createIndex('uix_user_users_login', '{{%user_users}}', 'login', TRUE);
        $this->createIndex('uix_user_users_email', '{{%user_users}}', 'email', TRUE);

        $this->createIndex('ix_user_profiles_user_id', '{{%user_profiles}}', 'user_id');
        $this->addForeignKey('fk_user_profiles_user_id', '{{%user_profiles}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');
    }

    private function removeRelations()
    {
        $this->dropIndex('uix_user_users_login', '{{%user_users}}');
        $this->dropIndex('uix_user_users_email', '{{%user_users}}');

        $this->dropForeignKey('fk_user_profiles_user_id', '{{%user_profiles}}');
        $this->dropIndex('ix_user_profiles_user_id', '{{%user_profiles}}');
    }

    private function createAdmins()
    {
        $this->insert('{{user_users}}', [
            'email'                 => 'jeka.deadline@gmail.com',
            'role_id'               => 1,
            'login'                 => 'admin',
            'password_hash'         => \Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'reset_password_token'  => NULL,
            'auth_key'              => md5(time() . \Yii::$app->getSecurity()->generateRandomString(50)),
            'confirm_email_at'      => new Expression('NOW()'),
            'register_ip'           => '127.0.0.1',
        ]);

        $this->insert('{{%user_profiles}}', [
            'user_id'     => 1,
            'surname'     => 'Бублик',
            'name'        => 'Евгений',
            'patronymic'  => 'Владимирович',
            'gender'      => 'male',
            'phone'       => '+380935994767',
            'date_birth'  => '1992-05-14',
        ]);
    }

}
