<?php
namespace app\modules\user;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'app\modules\user\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'core' ])) {
            Yii::$app->i18n->translations[ 'core' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/core/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'user' ])) {
            Yii::$app->i18n->translations[ 'user' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/user/messages',
            ];
        }

        if (!isset(Yii::$app->i18n->translations[ 'profile' ])) {
            Yii::$app->i18n->translations[ 'profile' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/profile/messages',
            ];
        }

        if (!isset(Yii::$app->i18n->translations[ 'shop' ])) {
            Yii::$app->i18n->translations[ 'shop' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }

        if (!isset(Yii::$app->i18n->translations[ 'card' ])) {
            Yii::$app->i18n->translations[ 'card' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => Yii::t('user', 'Users manager'),
                'items' => [
                    [
                        'label' => Yii::t('user', 'Users'),
                        'url'   => ['/user/user/index'],
                    ],
                ],
            ]
        ];
    }

}