<?php
    return [
        'List categories' => 'Список категорий товаров',
        'Update category' => 'Обновить категорию',
        'Create category' => 'Добавить категорию',
        'Choose category' => 'Выберите категорию товара',
        'View category' => 'Просмотр категории',
        'Parent category' => 'Родительская категория',
        'Name' => 'Название категории',
        'Description' => 'Описание категории',
        'Categories product management' => 'Управление категориями товаров',
    ];
?>