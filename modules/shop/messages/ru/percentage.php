<?php
    return [
        'List percentages' => 'Список процентов',
        'Create percentage' => 'Добавить процент',
        'Value' => 'Значение',
        'Percent' => 'Процент',
        'Control percentages' => 'Управление процентами',
        'Management bonus percentage' => 'Управление процентными бонусами',
        'Management discount percentage' => 'Управление процентными скидками',
    ];
?>