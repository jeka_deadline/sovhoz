<?php
    return [
        'List shops'                                => 'Список магазинов',
        'Create shop'                               => 'Добавить магазин',
        'Update shop'                               => 'Обновить магазин',
        'Name'                                      => 'Название магазина',
        'Phone'                                     => 'Телефон',
        'Address'                                   => 'Адрес магазина',
        'Description'                               => 'Описание магазина',
        'Finish sale'                               => 'Завершение продажи',
        'Cancel sale'                               => 'Отменить продажу',
        'Go sale'                                   => 'Совершить продажу',
        'Print check'                               => 'Печать чека',
        'Total'                                     => 'Итого',
        'Sum sale'                                  => 'Сумма покупок',
        'Create link percentage sum sale'           => 'Создать соотношение процента к сумме продажи',
        'Summary'                                   => 'Подвод итога',
        'Get summary'                               => 'Перейти к итогу',
        'Shops management'                          => 'Управление магазинами',
        'Create report'                             => 'Создание отчета',
        'Sale product'                              => 'Продажа товаров',
        'Admin report'                              => 'Отчет',
        'Bonus payment'                             => 'Количество списанных бонусов',
        'Shop'                                      => 'Магазин',
        'Seller'                                    => 'Продавец',
        'Client'                                    => 'Клиент',
        'Pay method'                                => 'Тип оплаты',
        'Cash'                                      => 'Наличные',
        'Non cash'                                  => 'Безналичные',
        'Pay bonuses'                               => 'Списанные бонусы',
        'Sum sale, rub'                             => 'Сумма покупки, руб',
        'Discount, rub'                             => 'Скидка, руб',
        'Bonus, rub'                                => 'Бонусы начисленные, руб',
        'Pay bonuses, rub'                          => 'Бонусы учтенные, руб',
        'Total sum'                                 => 'Итоговая сумма',
        'Passed:'                                   => 'Сдано:',
        'Cash on hand, rub'                         => 'Наличные денежные средства, руб.',
        'Non-cash funds, rub'                       => 'Безналичные денежные средства, руб.',
        'Total amount of discounts, rub'            => 'Общая сумма скидок, руб.',
        'The total amount of accrued bonuses, rub'  => 'Общая сумма начисленных бонусов, руб.',
        'Total amount of discounted bonuses, rub'   => 'Общая сумма учтенных бонусов, руб.',
        'Daily report'                              => 'Ежедневный отчет',
        'Find reports'                              => 'Поиск отчетов',
        'Reports to {date} not found'               => 'Отчеты за дату "{date}" не найдены',
        'Report seller {seller}'                    => 'Отчет продавца {seller}',
        'Seller reports'                            => 'Отчеты продавцов',
        'Count kilogramms'                          => 'Количество килограмм',
        'Count bags'                                => 'Количество мешков',
        'Shops'                                     => 'Магазины',
        'Choose shop'                               => 'Выберите магазины',
        'Requisites'                                => 'Реквизиты',
        'Shop phone'                                => 'Телефон магазин',
        'Sum'                                       => 'Сумма',
        'Total amoun sum'                           => 'Общая сумма',
        'Pay bonuses'                               => 'Бонусы учтенные',
        'Bonuses accrued'                           => 'Бонусы начисленные',
        'Back sale'                                 => 'Вернуться к выбору товаров',
        'Seller report'                             => 'Отчет продавца',
        'Print report'                              => 'Печать отчета',
        'Total sale kg'                             => 'Общее количество проданных кг',
        'For Shop'                                  => 'Магазин',
        'Category'                                  => 'Категория',
        'Choose Shop'                               => 'Выбирите магазин',
        'Export'                                    => 'Добавить',
        'View shop'                                 => 'Просмотр магазина',
        'Clients in shops'                          => 'Клиенты магазинов',
    ];
?>