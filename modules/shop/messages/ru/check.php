<?php
    return [
        'Checks' => 'Чеки',
        'Export' => 'Экспорт прайса по магазину',
        'Find check' => 'Найти чек',
        'Code' => 'Код чека',
        'Check with code "{code}" not fount' => 'Чек с кодом "{code}" не найден',
        'Check' => 'Чек',
        '№ check' => '№ чека',
        'Sales check' => 'Товарный чек',
        'Number/Date/Time' => 'Номер/Дата/Время',
    ];
?>