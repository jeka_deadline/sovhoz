<?php
    return [
        'List cards' => 'Список карт',
        'Create card' => 'Добавить карту',
        'Number' => 'Номер карты',
        'Value' => 'Процентное значение',
        'Card number' => 'Номер карты',
        'Type card' => 'Тип карты',
        'Total sum sale' => 'Итоговая сумма покупок за картой',
        'Link user' => 'Клиент',
        'Not has type' => 'Без типа',
        'Bonus type' => 'Бонусная',
        'Discount type' => 'Скидочная',
        'Check card' => 'Проверить карту',
        'Choose type card' => 'Выберите тип карты',
        'Card' => 'Карта',
        'Shop' => 'Выбрать магазин',
        'Choose card' => 'Выберите карту',
        'Init card value' => 'Начальное значение карты',
        'Pay bonus' => 'Оплата бонусами',
        'Card with this number not found' => 'Карта с данным номером не найдена',
        'Check other card' => 'Проверить другую карту',
        'Total' => 'Общая цена покупок',
        'Bonus sum' => 'Количество бонусов',
        'Cards management' => 'Управление картами',
        'Price management' => 'Управление прайсами',
        'Check card' => 'Проверить карту',
        'Update card' => 'Обновить карту',
         "User's card not found" => 'Пользователь с такой картой не найден'
    ]
?>