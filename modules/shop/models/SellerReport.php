<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\SellerReport as BaseSellerReport;
use app\modules\user\models\Profile;
use app\modules\shop\models\SellerStat;

class SellerReport extends BaseSellerReport
{

    public function setData($sellerHistories)
    {
        $this->clearData();
        $totalKg = 0;

        foreach ($sellerHistories as $itemSellerHistory) {
            if ($itemSellerHistory->pay_method === 'cash') {
                $this->total_cash_sum += $itemSellerHistory->total_sum;
            }
            if ($itemSellerHistory->pay_method === 'non-cash') {
                $this->total_non_cash_sum += $itemSellerHistory->total_sum;
            }

            $this->total_discount_sum     += $itemSellerHistory->total_discount_sum;
            $this->total_bonuses_sum      += $itemSellerHistory->total_bonuses_sum;
            $this->total_pay_bonuses_sum  += $itemSellerHistory->payment_bonus;
            $this->total_sale_kg          += $itemSellerHistory->total_sale_kg;
        }

        $lastEnter = SellerStat::find()->where(['user_id' => Yii::$app->user->identity->getId()])
                                       ->andWhere(['DATE(start_date)' => date('Y-m-d')])
                                       ->orderBy(['start_date' => SORT_ASC])->one();
        $lastEnter = ($lastEnter) ? $lastEnter->start_date : NULL;

        $this->last_enter = $lastEnter;
    }

    private function clearData()
    {
        $this->total_cash_sum         = 0;
        $this->total_non_cash_sum     = 0;
        $this->total_discount_sum     = 0;
        $this->total_bonuses_sum      = 0;
        $this->total_pay_bonuses_sum  = 0;
        $this->total_sale_kg          = 0;
    }

    public function getSellerUserProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'seller_user_id']);
    }

    public function getSeller()
    {
        if (!$this->sellerUserProfile) {
            return NULL;
        }

        return $this->sellerUserProfile->surname . ' ' . $this->sellerUserProfile->name . ' ' . $this->sellerUserProfile->patronymic;
    }

    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id'])
                    ->viaTable(LinksSellerShop::tableName(), ['user_id' => 'seller_user_id']);
    }

    public function getLastDeposit()
    {
        $report = SellerReport::find()
                                    ->where(['seller_user_id' => Yii::$app->user->identity->getId()])
                                    ->andWhere(['<', 'date', $this->date])
                                    ->orderBy(['date' => SORT_DESC])
                                    ->one();

        return ($report) ? $report->cashbox_balance : 0;
    }

}