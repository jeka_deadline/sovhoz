<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\Check as BaseCheck;

class Check extends BaseCheck
{

    public function getSeller()
    {
        $sellerHistory = $this->sellerHistory;

        if (!$sellerHistory) {
            return NULL;
        }

        $profile = $sellerHistory->userProfile;

        if (!$profile) {
            return NUll;
        }

        return $profile->surname . ' ' . $profile->name . ' ' . $profile->patronymic;
    }

    public function getDataSale()
    {
        $sellerHistory = $this->sellerHistory;

        if (!$sellerHistory) {
            return [];
        }

        return unserialize(base64_decode($sellerHistory->data));
    }

    public function getSellerHistory()
    {
        return $this->hasOne(SellerHistory::className(), ['id' => 'seller_history_id']);
    }

    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

}