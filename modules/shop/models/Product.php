<?php
namespace app\modules\shop\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\shop\Product as BaseProduct;
use app\modules\user\models\User;
use app\models\shop\ProductPrice;



class Product extends BaseProduct
{

    public function getFormElements()
    {
        $listPercentages = ArrayHelper::map(Percentage::find()->all(), 'id', function($model){ return $model->value . '%';});
        $categories = Category::getListCategories();
        $listUnit = ProductUnit::getAllListUnit();

        if (!$categories) {
            $categories = [];
        }

        $fields =  [
            'category_id'             => ['type' => 'dropdownlist', 'items' => $categories, 'attributes' => ['prompt' => Yii::t('category', 'Choose category')]],
            'name'                    => ['type' => 'text'],
            'description'             => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'check_one'               => ['type' => 'checkbox'] ,
            'price_bag'               => ['type' => 'text'],
            'price_bag_mass'          => ['type' => 'text'],
            'price_bag_unit'          => ['type' => 'dropdownlist', 'items' => $listUnit, 'attributes' => ['prompt' => Yii::t('product', 'Choose unit')]],
            'price_kg'                => ['type' => 'text'],
            'max_bonus_percentage'    => ['type' => 'dropdownlist', 'items' => $listPercentages, 'attributes' => ['prompt' => Yii::t('product', 'Choose max bonus')]],
            'max_discount_percentage' => ['type' => 'dropdownlist', 'items' => $listPercentages, 'attributes' => ['prompt' => Yii::t('product', 'Choose max discount')]],
            
            
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'category_id', 'name', 'description', 'price_kg', 'price_bag', 'price_bag_mass', 'price_bag_unit', 'max_bonus_percentage', 'max_discount_percentage', 'check_one'];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getDiscount()
    {
        return $this->hasOne(Percentage::className(), ['id' => 'max_discount_percentage']);
    }

    public function getBonus()
    {
        return $this->hasOne(Percentage::className(), ['id' => 'max_bonus_percentage']);
    }

    public static function getTypeProduct($type, $small = FALSE)
    {
        switch ($type) {
            case 'kg':
                return ($small) ? Yii::t('product', 'kg') : Yii::t('product', 'Kilogramms');
            case 'bag':
                return ($small) ? Yii::t('product', 'bg') : Yii::t('product', 'Bags');
            default:
                return NULL;
        }
    }

    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('category', 'Name'),
            'name' => Yii::t('product', 'Name'),
            'description' => Yii::t('product', 'Description'),
            'check_one' => Yii::t('product', 'Check One'),
            'price_bag' => Yii::t('product', 'Price bag'),
            'price_bag_mass' => Yii::t('product', 'Price Bag Mass'),
            'price_bag_unit' => Yii::t('product', 'Price Bag Unit'),
            'price_kg' => Yii::t('product', 'Price kilogramm'),
            'max_bonus_percentage' => Yii::t('product', 'Max bonus'),
            'max_discount_percentage' => Yii::t('product', 'Max discount'),
        ];
    }

    public static function getPriceProductWithDiscount($price, $discount)
    {
        return $price - $price * ($discount / 100);
    }

    public static function getProductBonus($sum, $bonus)
    {
        return $sum * ($bonus / 100);
    }

    public static function generateProductRowCheck($index, $type, $count, $product, $totalPrice, $bonus, $discount)
    {
        $name = $product->name;
        $priceOneCount = self::getPriceOneProduct($product, $type);
        switch ($type) {
            case 'kg':
                $type = Yii::t('product', '(kg)');
                break;
            case 'bag':
                $type = Yii::t('product', '(bag)');
                break;
        }

        $row = $index + 1 . '. ' . $name . ' ' . $count . ' ' . $type . ' x ' . $priceOneCount . ' ';

        if (!empty($discount)) {

            $totalPrice = self::getPriceProductWithDiscount($priceOneCount * $count, $discount);
            $row .= '* ' . $discount / 100 . '% ';
        }
        $row .= '= ' . $totalPrice . '<br>';

        return $row;
    }

    public static function getPriceOneProduct($product, $type)
    {
        switch ($type) {
            case 'kg':
                $currentUserId  = Yii::$app->user->identity->getId();
                $userCreate = User::find()->where(['id' => $currentUserId])->one();
                $listPrice = ProductPrice::find()
                            ->where(['product_id' => $product->id])
                            ->andWhere(['forShop'=> $userCreate->now_in_shop])
                            ->one();
                if ($listPrice) $product->price_kg = $listPrice->price_kg;
                return $product->price_kg;
            case 'bag':
                $currentUserId  = Yii::$app->user->identity->getId();
                $userCreate = User::find()->where(['id' => $currentUserId])->one();
                $listPrice = ProductPrice::find()
                            ->where(['product_id' => $product->id])
                            ->andWhere(['forShop'=> $userCreate->now_in_shop])
                            ->one();
                if ($listPrice) $product->price_bag = $listPrice->price_bag;
                return $product->price_bag;
            default:
                return NULL;
        }
    }

    public static function getBonusOrDiscount(ClientPurchasesHistory $clientPurchasesHistory)
    {
        if ($clientPurchasesHistory->bonus) {
            return Yii::t('shop', 'Б') . ': ' . number_format($clientPurchasesHistory->bonus, 2) . ' руб.';
        }
        if ($clientPurchasesHistory->discount) {
            return Yii::t('shop', 'С') . ': ' . number_format($clientPurchasesHistory->discount, 2) . ' руб.';
        }

    }

    public static function priceWithCurrency($price, $curr = 'руб')
    {
        return number_format($price, 2, '.', ' ') . ' ' . $curr . '.';
    }
}