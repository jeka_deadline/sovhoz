<?php
namespace app\modules\shop\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\shop\Category as BaseCategory;

class Category extends BaseCategory
{

    public static $tree;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['parent_id'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        $categories = self::getListCategories(0, 0, $this->id);

        if (!$categories) {
            $categories = [];
        }

        return [
            'parent_id'   =>  [
                                  'type'        => 'dropdownlist',
                                  'items'       => $categories,
                                  'attributes'  => ['prompt' => Yii::t('product', 'Choose parent category')]
                              ],
            'name'        =>  ['type' => 'text', 'fieldAttributes' => ['template' => '{label}{input}']],
            'description' =>  ['type' => 'textarea', 'attributes' => ['rows' => 10]],
        ];
    }

    public static function getListCategories($pid = 0, $level = 0, $currentId = 0)
    {
        $categories = self::find()->where(['=', 'parent_id', $pid])->all();
        $level      = ($pid == 0) ? 0 : ++$level;

        foreach ($categories as $category) {
            if ($category->id == $currentId) {
                continue;
            }
            self::$tree[ $category->id ] = str_repeat('-', $level * 2) . $category->name;

            self::getListCategories($category->id, $level, $currentId);
        }

        return self::$tree;
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'description'];
    }

    public function attributeLabels()
    {
        return [
            'parent_id' => Yii::t('category', 'Parent category'),
            'name' => Yii::t('category', 'Name'),
            'description' => Yii::t('category', 'Description'),
        ];
    }

    public static function getRoot() {
        return Category::find()->where(['parent_id' => 0])->orderBy('name')->all();
    }

}