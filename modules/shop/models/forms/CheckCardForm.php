<?php
namespace app\modules\shop\models\forms;

use Yii;
use yii\base\Model;
use app\modules\shop\models\Card;

class CheckCardForm extends Model
{

    public $numberCard;

    public function rules()
    {
        return [
            [['numberCard'], 'number'],
        ];
    }

    public function findCard()
    {
        $card = Card::find()
                          ->where(['number' => $this->numberCard])
                          ->with('userProfile')
                          ->one();

        return $card;
    }

    public function attributeLabels()
    {
        return [
            'numberCard' => Yii::t('card', 'Number'),
        ];
    }

}