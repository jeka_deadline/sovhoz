<?php
namespace app\modules\shop\models\forms;

use Yii;
use yii\base\Model;
use app\modules\shop\models\SellerReport;

class FindSellerReportForm extends Model
{

    public $date;

    public function rules()
    {
        return [
            [['date'], 'required'],
            [['date'], 'safe'],
        ];
    }

    public function findReports()
    {
        return SellerReport::find()->where(['date' => $this->date])->all();
    }

}