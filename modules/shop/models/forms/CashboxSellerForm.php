<?php
namespace app\modules\shop\models\forms;

use yii\base\Model;
use app\modules\shop\models\SellerReport;

class CashboxSellerForm extends Model
{

    public $sum;

    private $_currentReport;

    public function setCurrentSellerReport($report)
    {
        $this->_currentReport = $report;
    }

    public function rules()
    {
        return [
            ['sum', 'required'],
            ['sum', 'number'],
            ['sum', 'validateMaxSum'],
            ['sum', 'match', 'pattern' => '/^([1-9]\d+)$|^([1-9]\d+\.\d{1,2})$/', 'message' => 'Не правильный формат'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'sum' => 'Сумма',
        ];
    }

    public function validateMaxSum()
    {
        $maxSum = $this->_currentReport->total_cash_sum;

        $reportBeforeCurrent = $this->getSellerReportBeforeCurrent();

        if ($reportBeforeCurrent) {
            $maxSum += $reportBeforeCurrent->cashbox_balance;
        }

        if ($this->sum > $maxSum) {
            $this->addError('sum', "Сумма сдачи денег превышает лимит ({$maxSum}) денег, которые находятся в кассе");
            return FALSE;
        }

        return TRUE;
    }

    public function putDeposit()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $this->_currentReport->cashbox_seller_deposit = $this->sum;
        $this->_currentReport->cashbox_balance = $this->_currentReport->total_cash_sum - $this->sum;

        $reportBeforeCurrent = $this->getSellerReportBeforeCurrent();

        if ($reportBeforeCurrent) {
            $this->_currentReport->cashbox_balance += $reportBeforeCurrent->cashbox_balance;
        }

        $this->_currentReport->save(FALSE);

        return TRUE;
    }

    private function getSellerReportBeforeCurrent()
    {
        return SellerReport::find()
                                ->where(['seller_user_id' => $this->_currentReport->seller_user_id, ])
                                ->andWhere(['<', 'date', $this->_currentReport->date])
                                ->orderBy(['date' => SORT_DESC])
                                ->one();
    }
}