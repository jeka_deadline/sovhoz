<?php
namespace app\modules\shop\models\forms;

use Yii;
use yii\base\Model;
use app\modules\shop\models\SellerHistory;

class FindCheckForm extends Model
{

    public $code;

    public function rules()
    {
        return [
            ['code', 'required'],
        ];
    }

    public function findCheck()
    {
        return SellerHistory::findOne(['code' => $this->code, 'status' => 'good']);
   
    }

    public function findCheckForSeller()
    {
        return SellerHistory::findOne(['seller_user_id'=> '2','status' => 'good']);
    }

    public function attributeLabels()
    {
        return [
            'code' => Yii::t('check', 'Code'),
        ];
    }

}