<?php
namespace app\modules\shop\models\forms;

use Yii;
use yii\base\Model;
use app\modules\shop\models\SellerHistory;
use app\modules\shop\models\Card;
use app\modules\shop\models\ClientPurchasesHistory;
use app\modules\shop\models\Check;
use app\modules\shop\models\Product;
use app\modules\shop\models\Client;
use app\modules\shop\models\LinkPercentageSumSale;

class TotalSaleForm extends Model
{

    public $numberCard;
    public $payBonus;
    public $payMethod;

    public function rules()
    {
        return [
            [['numberCard'], 'exist','targetClass' => Card::className(), 'targetAttribute' => 'number', 'filter' => function($query) {
                return $query->joinWith('userProfile');
            }],
            [['payMethod'], 'required'],
            [['payBonus'], 'boolean'],
            [['payMethod'], 'in', 'range' => ['cash', 'non-cash']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'numberCard' => Yii::t('card', 'Number'),
            'payBonus' => Yii::t('card', 'Pay bonus'),
            'payMethod' => Yii::t('shop', 'Pay method'),
        ];
    }

    public function totalSale(SellerHistory $sellerHistory)
    {
        if (!$this->validate()) {
            return FALSE;
        }
        $sellerHistory->status = 'total';
        $totalSum       = 0;
        $totalBonus     = 0;
        $totalDiscount  = 0;
        $flagNULL = TRUE;
        if (!$this->numberCard) {
            $sellerHistory->save();
            $flagNULL = FALSE;
            goto a;
        }



        $card = Card::find()
                          ->where(['number' => $this->numberCard])
                          ->with('userProfile')
                          ->andWhere(['is not', 'type', NULL])
                          ->one();

        if (!$card) {
            $this->addError('numberCard', Yii::t('card', 'Card not found'));
            return FALSE;
        }

        foreach ($sellerHistory->getProducts()->all() as $itemHistoryProduct) {
            if ($card->type === 'discount') {
                if (!$itemHistoryProduct->product->discount || (int)$itemHistoryProduct->product->discount->value > (int)$card->value) {
                    $itemHistoryProduct->discount_percent_value = $card->value;
                } else {
                    $itemHistoryProduct->discount_percent_value = $itemHistoryProduct->product->discount->value;
                }

                if ($itemHistoryProduct->full_sum <= 100)
                {
                    $itemHistoryProduct->discount_percent_value = 0;
                }

                $itemHistoryProduct->sum = ceil(($itemHistoryProduct->full_sum - $itemHistoryProduct->full_sum * $itemHistoryProduct->discount_percent_value / 100));
                $itemHistoryProduct->discount = $itemHistoryProduct->full_sum - $itemHistoryProduct->sum;
            }
            if ($card->type === 'bonus') {
                if (!$itemHistoryProduct->product->bonus || (int)$itemHistoryProduct->product->bonus->value > (int)$card->value) {
                  $itemHistoryProduct->bonus_percent_value = $card->value;
                } else {
                    $itemHistoryProduct->bonus_percent_value = $itemHistoryProduct->product->bonus->value;
                }

                $itemHistoryProduct->bonus = $itemHistoryProduct->full_sum * ($itemHistoryProduct->bonus_percent_value / 100);
                $itemHistoryProduct->sum = ceil($itemHistoryProduct->sum);
            }

            $itemHistoryProduct->save(FALSE);

            $totalSum       += $itemHistoryProduct->sum;
            $totalBonus     += $itemHistoryProduct->bonus;
            $totalDiscount  += $itemHistoryProduct->discount;
        }
          //mark final
        a:
        if(!$flagNULL) {
           $f1 = NULL;
           $f2 = NULL;
           $f3 = NULL;
           foreach ($sellerHistory->getProducts()->all() as $itemHistoryProduct) {
            $totalSum       += $itemHistoryProduct->sum;
            $totalBonus     += $itemHistoryProduct->bonus;
            $totalDiscount  += $itemHistoryProduct->discount;
           }
        } else {
           $f1 = $card->userProfile->user_id;
           $f2 = $card->type;
           $f3 = $card->id;
        }

        $sellerHistory->total_sum           = ceil($totalSum);
        $sellerHistory->total_bonuses_sum   = $totalBonus;
        $sellerHistory->total_discount_sum  = $totalDiscount;
        $sellerHistory->client_user_id      = $f1;
        $sellerHistory->card_type           = $f2;
        $sellerHistory->card_id             = $f3;
        $sellerHistory->pay_method          = $this->payMethod;
        $sellerHistory->payment_bonus       = ($this->payBonus) ? $this->payBonus : NULL;

        $sellerHistory->save(FALSE);

        /*$sellerHistory = SellerHistory::find()
                                            ->where(['status' => 'total'])
                                            ->andWhere(['seller_user_id' => Yii::$app->user->identity->getId()])
                                            ->one();*/

        $oneBonusValue  = 0;
        $countProducts  = $sellerHistory->getProducts()->count();
        $currentBonuses = $sellerHistory->total_bonuses_sum;
        $card           = ($sellerHistory->card_id) ? Card::findOne($sellerHistory->card_id) : NULL;

        $sellerHistory->status      = 'good';
        $sellerHistory->date        = date('Y-m-d H:i:s');
        $sellerHistory->code        = $sellerHistory->id + 10000;

        if (!$card) {
            $sellerHistory->save(FALSE);
            return $sellerHistory;
        }

         if ($card->type === 'bonus') {
            if ($this->payBonus) {
                $halfSum = $sellerHistory->total_sum / 2;

                if ($card->bonus_sum > $halfSum) {
                    $oneBonusValue                = ($sellerHistory->total_sum - $halfSum) / $sellerHistory->total_sum;
                    $bonusSum                     = ($card->bonus_sum - $halfSum) + ($oneBonusValue * $sellerHistory->total_bonuses_sum);
                    $sellerHistory->payment_bonus = $halfSum;
                    $sellerHistory->total_sum     -= ceil($halfSum);
                } else {
                    $oneBonusValue                = ($sellerHistory->total_sum - $card->bonus_sum ) / $sellerHistory->total_sum;
                    $bonusSum                     = $oneBonusValue * $sellerHistory->total_bonuses_sum;
                    $sellerHistory->payment_bonus = $card->bonus_sum;
                    $sellerHistory->total_sum     -= $card->bonus_sum;
                }
                $card->bonus_sum                  = $bonusSum;
                $sellerHistory->total_bonuses_sum = $oneBonusValue * $sellerHistory->total_bonuses_sum;
            } else {
                $card->bonus_sum += $sellerHistory->total_bonuses_sum;
            }
        }


        $card->total += $sellerHistory->total_sum;
        $card->value = $this->updateCardValue($card);
        $card->save(FALSE);;

        $countTakenBonusOneProduct  = $sellerHistory->payment_bonus / ($countProducts / 2);
        $countAccruedBonusProduct   = ($currentBonuses - $sellerHistory->total_bonuses_sum) / ($countProducts/2);
        if ($countTakenBonusOneProduct) {
            foreach ($sellerHistory->getProducts()->all() as $itemHistoryProduct) {
            	if ($itemHistoryProduct->count == 0) continue;
                $itemHistoryProduct->payment_bonus = $countTakenBonusOneProduct;
                $itemHistoryProduct->sum           = ceil($itemHistoryProduct->sum - $countTakenBonusOneProduct);
                $itemHistoryProduct->bonus        -= $countAccruedBonusProduct;
                $itemHistoryProduct->save(FALSE);
            }
        }

        $sellerHistory->save(FALSE);

        return $sellerHistory;
    }

    private function updateCardValue($card)
    {
        $linkPercentageSumSale = LinkPercentageSumSale::find()
                                                            ->where(['type' => $card->type])
                                                            ->andWhere(['<', 'sum_sale', $card->total])
                                                            ->orderBy(['sum_sale' => SORT_DESC])
                                                            ->limit(1)
                                                            ->one();

        if ($linkPercentageSumSale && $linkPercentageSumSale->percent != $card->value) {
            return $linkPercentageSumSale->percent;
        }

        return $card->value;
    }

    public function getListPayMethod()
    {
        return [
            'cash' => Yii::t('shop', 'Cash'),
            'non-cash' => Yii::t('shop', 'Non cash'),
        ];
    }

}
?>