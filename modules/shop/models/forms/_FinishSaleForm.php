<?php
namespace app\modules\shop\models\forms;

use Yii;
use yii\base\Model;
use app\modules\shop\models\ClientPurchasesHistory;
use app\modules\shop\models\SellerHistory;
use app\modules\shop\models\Check;
use app\modules\shop\models\Card;
use app\modules\shop\models\Product;
use app\modules\shop\models\Client;
use app\modules\shop\models\LinkPercentageSumSale;
use app\modules\shop\models\LinkClientShop;

class FinishSaleForm extends Model
{

    public $payBonus;
    public $payMethod;

    public function rules()
    {
        return [
            [['payMethod'], 'required'],
            [['payBonus'], 'boolean'],
            [['payMethod'], 'in', 'range' => ['cash', 'non-cash']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'payBonus' => Yii::t('card', 'Pay bonus'),
            'payMethod' => Yii::t('shop', 'Pay method'),
        ];
    }

    public function finishSale(SellerHistory $sellerHistory)
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $flag           = TRUE;
        $clientId       = NULL;
        $oneBonusValue  = 0;
        $countProducts  = $sellerHistory->getProducts()->count();
        $currentBonuses = $sellerHistory->total_bonuses_sum;
        $card           = ($sellerHistory->card_id) ? Card::findOne($sellerHistory->card_id) : NULL;

        $sellerHistory->status      = 'good';
        $sellerHistory->pay_method  = $this->payMethod;
        $sellerHistory->date        = date('Y-m-d H:i:s');
        $sellerHistory->code        = $sellerHistory->id + 10000;

        if (!$card) {

            $sellerHistory->save(FALSE);
            return $sellerHistory;
        }

        if ($card->type === 'bonus') {
            if ($this->payBonus) {
                $halfSum = $sellerHistory->total_sum / 2;

                if ($card->bonus_sum > $halfSum) {
                    $oneBonusValue                = ($sellerHistory->total_sum - $halfSum) / $sellerHistory->total_sum;
                    $bonusSum                     = ($card->bonus_sum - $halfSum) + ($oneBonusValue * $sellerHistory->total_bonuses_sum);
                    $sellerHistory->payment_bonus = $halfSum;
                    $sellerHistory->total_sum     -= $halfSum;
                } else {
                    $oneBonusValue                = ($sellerHistory->total_sum - $card->bonus_sum ) / $sellerHistory->total_sum;
                    $bonusSum                     = $oneBonusValue * $sellerHistory->total_bonuses_sum;
                    $sellerHistory->payment_bonus = $card->bonus_sum;
                    $sellerHistory->total_sum     -= $card->bonus_sum;
                }
                $card->bonus_sum                  = $bonusSum;
                $sellerHistory->total_bonuses_sum = $bonusSum;
            } else {
                $card->bonus_sum += $sellerHistory->total_bonuses_sum;
            }
        }

        $card->total += $sellerHistory->total_sum;
        $card->value = $this->updateCardValue($card);
        $card->save(FALSE);
        //throw new \Exception($sellerHistory->payment_bonus / $countProducts);


        $countTakenBonusOneProduct  = $sellerHistory->payment_bonus / $countProducts;
        $countAccruedBonusProduct   = ($currentBonuses - $sellerHistory->total_bonuses_sum) / $countProducts;

        if ($countTakenBonusOneProduct) {
            foreach ($sellerHistory->getProducts()->all() as $itemHistoryProduct) {
                $itemHistoryProduct->payment_bonus = $countTakenBonusOneProduct;
                $itemHistoryProduct->sum          -= $countTakenBonusOneProduct;
                $itemHistoryProduct->bonus        -= $countAccruedBonusProduct;

                $itemHistoryProduct->save(FALSE);
            }
        }

        $sellerHistory->save(FALSE);

        return $sellerHistory;

    }

    private function updateCardValue($card)
    {
        $linkPercentageSumSale = LinkPercentageSumSale::find()
                                                            ->where(['type' => $card->type])
                                                            ->andWhere(['<', 'sum_sale', $card->total])
                                                            ->orderBy(['sum_sale' => SORT_DESC])
                                                            ->limit(1)
                                                            ->one();

        if ($linkPercentageSumSale && $linkPercentageSumSale->percent != $card->value) {
            return $linkPercentageSumSale->percent;
        }

        return $card->value;
    }

    public function getListPayMethod()
    {
        return [
            'cash' => Yii::t('shop', 'Cash'),
            'non-cash' => Yii::t('shop', 'Non cash'),
        ];
    }

}
?>