<?php
namespace app\modules\shop\models\forms;

use app\models\shop\ProductPrice;
use app\modules\shop\models\Percentage;
use app\modules\shop\models\Shop;
use Yii;
use yii\base\Model;
use app\modules\shop\models\Product;
use yii\helpers\ArrayHelper;

class AddMultiplePriceForm extends Model
{

    public $product_id;
    public $forShops;
    public $price_bag;
    public $price_kg;
    public $max_bonus_percentage;
    public $max_discount_percentage;


    public function rules()
    {
        return [
            [['product_id', 'forShops', 'price_bag'], 'required'],
            [['product_id', 'max_bonus_percentage', 'max_discount_percentage'], 'integer'],
            [['price_bag', 'price_kg'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['max_bonus_percentage'], 'exist', 'skipOnError' => true, 'targetClass' => Percentage::className(), 'targetAttribute' => ['max_bonus_percentage' => 'id']],
            [['max_discount_percentage'], 'exist', 'skipOnError' => true, 'targetClass' => Percentage::className(), 'targetAttribute' => ['max_discount_percentage' => 'id']],
            [['forShops'], 'filter', 'filter' => [$this, 'filterShops']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('product', 'Product Id'),
            'price_bag'  => Yii::t('product', 'Price bag'),
            'price_kg' => Yii::t('product', 'Price kilogramm'),
            'max_bonus_percentage' => Yii::t('product','Max bonus'),
            'max_discount_percentage' => Yii::t('product','Max discount'),
            'forShops' => Yii::t('product', 'For Shop'),
        ];
    }

    public function filterShops($value) {
        $shopIds = ArrayHelper::map(Shop::findAll($value),'id', 'id');

        $priceShopIds = ArrayHelper::map(ProductPrice::find()
            ->where([
                'forShop' => $shopIds,
                'product_id' => $this->product_id
            ])
            ->all(),'forShop', 'forShop');

        return array_diff($shopIds, $priceShopIds);
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        foreach ($this->forShops as $shopId) {
            $modelProductPrice = new ProductPrice();
            $modelProductPrice->product_id = $this->product_id;
            $modelProductPrice->forShop = $shopId;
            $modelProductPrice->price_bag = $this->price_bag;
            $modelProductPrice->price_kg = $this->price_kg;
            $modelProductPrice->max_bonus_percentage = $this->max_bonus_percentage;
            $modelProductPrice->max_discount_percentage = $this->max_discount_percentage;
            $modelProductPrice->save();
        }

        return true;
    }
}
?>