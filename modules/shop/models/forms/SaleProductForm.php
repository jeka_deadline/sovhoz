<?php
namespace app\modules\shop\models\forms;

use Yii;
use yii\base\Model;
use app\modules\shop\models\Product;
use app\modules\user\models\User;
use app\modules\shop\models\SellerHistory;
use yii\helpers\ArrayHelper;
use app\modules\shop\models\ClientPurchasesHistory;
use app\modules\shop\models\SellerStat;
use app\models\shop\ProductPrice;

class SaleProductForm extends Model
{

    public $productIds;
    public $countKg;
    public $countBag;

    public function rules()
    {
        return [
            [['countKg', 'countBag'], 'safe'],
            [['productIds'], 'exist', 'targetClass' => Product::className(), 'targetAttribute' => 'id', 'allowArray' => TRUE],
        ];
    }

    public function createSaleProduct()
    {
        if (!$this->validate()) {
            return FALSE;
        }
        $currentUserId  = Yii::$app->user->identity->getId();
        $userCreate = User::find()->where(['id' => $currentUserId])->one();
        $listProducts   = ArrayHelper::map(Product::find()->where(['id' => $this->productIds])->with('bonus', 'discount')->all(), 'id', function($model){ return $model; });
        $listPrice = ArrayHelper::map(ProductPrice::find()->where(['product_id' => $this->productIds])->andWhere(['forShop'=>$userCreate->now_in_shop])->all(),'product_id', function($model){ return $model; });
        if ($listPrice) 
        {
            foreach ($this->productIds as $key => $productId) {
                $listProducts[$productId]->price_bag = $listPrice[$productId]->price_bag;
                $listProducts[$productId]->price_kg = $listPrice[$productId]->price_kg;
                $listProducts[$productId]->max_bonus_percentage = $listPrice[$productId]->max_bonus_percentage;
                $listProducts[$productId]->max_discount_percentage = $listPrice[$productId]->max_discount_percentage;
            }
        }
        $flag           = TRUE;
        $fullSum        = 0;
        $totalKg        = 0;

        // транзакция
        $connection   = Yii::$app->db;
        $transaction  = $connection->beginTransaction();

        $sellerStat = SellerStat::find()
                                      ->where(['user_id' => $currentUserId])
                                      ->orderBy(['start_date' => SORT_DESC])
                                      ->one();
        if (!$sellerStat) {
            throw new \Exception("Error Processing Request", 1);
        }

        $sellerHistory = SellerHistory::find()
                                            ->where(['status' => 'new'])
                                            ->with('products')
                                            ->andWhere(['seller_user_id' => Yii::$app->user->identity->getId()])
                                            ->one();
        
        if ($sellerHistory) {
            foreach ($sellerHistory->products as $product) {
                $product->delete();
            }

            $sellerHistory->delete();
        }

        // история
        $sellerHistory                  = new SellerHistory();
        $sellerHistory->seller_user_id  = $currentUserId;
        $sellerHistory->status          = 'new';
        $sellerHistory->shop_id         = $sellerStat->shop_id;
        $sellerHistory->save();
 
        foreach ($this->productIds as $key => $productId) {
            $product          = $listProducts[$productId];
            $productType      = NULL;
            $productCount     = 0;
            $productPrice     = 0;
            if (isset($this->countKg[ $productId ])) {
                
                if (empty($this->countKg[ $productId ])) {
                    $productPrice = 0;
                    $productCount++;

                    $model                    = new ClientPurchasesHistory();
                    $model->seller_history_id = $sellerHistory->id;
                    $model->product_id        = $product->id;
                    $model->count             = 0;
                    $model->price_one_count   = $product->price_kg;
                    $model->sum               = $productPrice;
                    $model->full_sum          = $productPrice;
                    $model->type_product      = 'kg';
                    $fullSum                  += $productPrice;
                    $totalKg                  += $model->count;

                    if (!$model->validate() || !$model->save()) {
                         $flag = FALSE;
                        break;
                    }
                    

                } else {
                $productPrice = $product->price_kg * $this->countKg[ $productId ];
                $productCount++;

                $model                    = new ClientPurchasesHistory();
                $model->seller_history_id = $sellerHistory->id;
                $model->product_id        = $product->id;
                $model->count             = $this->countKg[ $productId ];
                $model->price_one_count   = $product->price_kg;
                $model->sum               = $productPrice;
                $model->full_sum          = $productPrice;
                $model->type_product      = 'kg';
                $fullSum                  += $productPrice;
                $totalKg                  += $model->count;

                if (!$model->validate() || !$model->save()) {
                    $flag = FALSE;
                    break;
                }
            }
            }

            if (isset($this->countBag[ $productId ])) {

                if(empty($this->countBag[ $productId ])){
                $productPrice = 0;
                $productCount++;

                $model                    = new ClientPurchasesHistory();
                $model->seller_history_id = $sellerHistory->id;
                $model->product_id        = $product->id;
                $model->count             = 0;
                $model->price_one_count   = $product->price_bag;
                $model->sum               = $productPrice;
                $model->full_sum          = $productPrice;
                $model->type_product      = 'bag';
                $fullSum                  += 0;

                if (!$model->validate() || !$model->save()) {
                    $flag = FALSE;
                    break;
                }
                } else {
                   $productPrice = $product->price_bag * $this->countBag[ $productId ];
                $productCount++;

                $model                    = new ClientPurchasesHistory();
                $model->seller_history_id = $sellerHistory->id;
                $model->product_id        = $product->id;
                $model->count             = $this->countBag[ $productId ];
                $model->price_one_count   = $product->price_bag;
                $model->sum               = $productPrice;
                $model->full_sum          = $productPrice;
                $model->type_product      = 'bag';
                $fullSum                  += $productPrice;

                if (!$model->validate() || !$model->save()) {
                    $flag = FALSE;
                    break;
                } 
                }

                
            }

            if (!$productCount) {
                $flag = FALSE;
            }
        }

        if (!$flag) {
            $sellerHistory->delete();
            $transaction->rollback();

            return FALSE;
        }

        $sellerHistory->full_sum  = $fullSum;
        $sellerHistory->total_sum = $fullSum;
        $sellerHistory->total_sale_kg = $totalKg;

        $sellerHistory->save(FALSE);
        $transaction->commit();

        return $sellerHistory->id;
    }

    public function attributeLabels()
    {
        return [
            'productId' => Yii::t('shop', 'Product'),
            'clientId'  => Yii::t('shop', 'ClientId'),
            'count'     => Yii::t('shop', 'Count'),
        ];
    }

}