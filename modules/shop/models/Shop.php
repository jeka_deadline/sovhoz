<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\Shop as BaseShop;
use yii\db\ActiveRecord;
use app\modules\shop\models\Product;
use app\modules\shop\models\ProductDef;
use yii\db\Command;
use yii\helpers\ArrayHelper;


class Shop extends BaseShop
{

    public function getFormElements()
    {
        $fields =  [
            'name'        => ['type' => 'text'],
            'description' => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'address'     => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            'phone'       => ['type' => 'text'],
            'requisites'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'description', 'address', 'phone', 'requisites'];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
            'address' => Yii::t('shop', 'Address'),
            'phone' => Yii::t('shop', 'Phone'),
            'requisites' => Yii::t('shop', 'Requisites')
        ];
    }

    public static function getListShops()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
}