<?php

namespace app\modules\shop\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_unit".
 *
 * @property int $id
 * @property string $name
 */
class ProductUnit extends \app\models\shop\ProductUnit
{

    public static function getAllListUnit() {
        return ArrayHelper::map(self::find()->all(),'id', 'name');
    }
}
