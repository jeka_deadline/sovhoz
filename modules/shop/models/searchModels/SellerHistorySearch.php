<?php

namespace app\modules\shop\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\SellerHistory;

/**
 * SellerHistorySearch represents the model behind the search form about `app\modules\shop\models\SellerHistory`.
 */
class SellerHistorySearch extends SellerHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'seller_user_id', 'client_user_id', 'card_id', 'code', 'shop_id'], 'integer'],
            [['status', 'card_type', 'date', 'pay_method'], 'safe'],
            [['total_sum', 'payment_bonus', 'total_bonuses_sum', 'total_discount_sum', 'full_sum', 'total_sale_kg'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SellerHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'seller_user_id' => $this->seller_user_id,
            'client_user_id' => $this->client_user_id,
            'card_id' => $this->card_id,
            'date' => $this->date,
            'code' => $this->code,
            'total_sum' => $this->total_sum,
            'payment_bonus' => $this->payment_bonus,
            'total_bonuses_sum' => $this->total_bonuses_sum,
            'total_discount_sum' => $this->total_discount_sum,
            'full_sum' => $this->full_sum,
            'shop_id' => $this->shop_id,
            'total_sale_kg' => $this->total_sale_kg,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'card_type', $this->card_type])
            ->andFilterWhere(['like', 'pay_method', $this->pay_method]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchWithStatusGood($params)
    {
        $query = SellerHistory::find()->where(['status' => 'good'])->orderBy(['date' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'seller_user_id' => $this->seller_user_id,
            'client_user_id' => $this->client_user_id,
            'card_id' => $this->card_id,
            'date' => $this->date,
            'code' => $this->code,
            'total_sum' => $this->total_sum,
            'payment_bonus' => $this->payment_bonus,
            'total_bonuses_sum' => $this->total_bonuses_sum,
            'total_discount_sum' => $this->total_discount_sum,
            'full_sum' => $this->full_sum,
            'shop_id' => $this->shop_id,
            'total_sale_kg' => $this->total_sale_kg,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'card_type', $this->card_type])
            ->andFilterWhere(['like', 'pay_method', $this->pay_method]);

        return $dataProvider;
    }
}