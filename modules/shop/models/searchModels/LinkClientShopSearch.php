<?php

namespace app\modules\shop\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\LinkClientShop;
use app\modules\shop\models\Shop;

/**
 * LinkClientShopSearch represents the model behind the search form about `app\modules\shop\models\LinkClientShop`.
 */
class LinkClientShopSearch extends LinkClientShop
{

    public $surname;
    public $name;
    public $patronymic;
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'shop_id'], 'integer'],
            [['surname', 'name', 'patronymic', 'phone'], 'string'],
            [['surname', 'name', 'patronymic'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LinkClientShop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'shop_id' => $this->shop_id,
        ]);

        return $dataProvider;
    }

  public function searchCustom($params)
    {
        $query = LinkClientShop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'shop_id' => $this->shop_id,
        ]);

        return $dataProvider;
    }
}