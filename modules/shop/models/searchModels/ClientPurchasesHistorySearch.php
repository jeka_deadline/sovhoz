<?php

namespace app\modules\shop\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\ClientPurchasesHistory;
use app\modules\shop\models\SellerHistory;
use yii\helpers\ArrayHelper;
use app\modules\user\models\Profile;
use app\modules\shop\models\Product;
use yii\db\Expression;
use app\modules\shop\models\LinksSellerShop;


/**
 * ClientPurchasesHistorySearch represents the model behind the search form about `app\modules\shop\models\ClientPurchasesHistory`.
 */
class ClientPurchasesHistorySearch extends ClientPurchasesHistory
{
    public $shopId;
    public $sellerId;
    public $clientId;
    public $dateFrom;
    public $dateTo;
    public $category_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'seller_history_id', 'product_id','category_id','bonus_percent_value', 'discount_percent_value'], 'integer'],
            [['count', 'sum', 'bonus', 'discount', 'full_sum', 'payment_bonus', 'price_one_count'], 'number'],
            [['dateFrom', 'dateTo'], 'default', 'value' => NULL],
            [['type_product', 'shopId', 'clientId', 'sellerId', 'dateTo', 'dateFrom'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'seller_history_id' => $this->seller_history_id,
            'product_id' => $this->product_id,
            'count' => $this->count,
            'sum' => $this->sum,
            'bonus' => $this->bonus,
            'discount' => $this->discount,
            'full_sum' => $this->full_sum,
            'bonus_percent_value' => $this->bonus_percent_value,
            'discount_percent_value' => $this->discount_percent_value,
            'payment_bonus' => $this->payment_bonus,
            'price_one_count' => $this->price_one_count,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'type_product', $this->type_product]);

        $query->orderBy(['id' => SORT_DESC]);

        $query->joinWith('sellerHistory')
              ->with('sellerHistory', 'product')
              ->andWhere([SellerHistory::tableName() . '.`status`' => 'good']);
        $sellerUserIds = [];

        if ($this->sellerId) {
            $sellerUserIds = ArrayHelper::map(Profile::find()->where(['like', 'surname', $this->sellerId])->all(), 'user_id', 'user_id');
            $query->andWhere(['seller_user_id' => $sellerUserIds]);
        }

        if ($this->clientId) {
            $clientUserIds = ArrayHelper::map(Profile::find()->where(['like', 'surname', $this->clientId])->all(), 'user_id', 'user_id');
            $query->andWhere(['client_user_id' => $clientUserIds]);
        }

        if ($this->dateFrom) {
            $query->andWhere(['>=', new Expression('DATE(' . SellerHistory::tableName() . '.`date`)'), $this->dateFrom]);
        }
        if ($this->dateTo) {
            $query->andWhere(['<=', new Expression('DATE(' . SellerHistory::tableName() . '.`date`)'), $this->dateTo]);
        }
        if ($this->shopId) {
            $query->andWhere([SellerHistory::tableName() . '.`shop_id`' => $this->shopId]);
        }
        if ($this->category_id) {
            $query->joinWith('product')->andWhere([Product::tableName() . '.`category_id`' => $this->category_id]);
        }


        return $dataProvider;
    }
}
