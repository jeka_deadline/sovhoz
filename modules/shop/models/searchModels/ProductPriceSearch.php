<?php

namespace app\modules\shop\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\shop\ProductPrice;

/**
 * ProductPriceSearch represents the model behind the search form of `app\models\shop\ProductPrice`.
 */
class ProductPriceSearch extends ProductPrice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'forShop', 'max_bonus_percentage', 'max_discount_percentage'], 'integer'],
            [['price_bag', 'price_kg'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductPrice::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'price_bag' => $this->price_bag,
            'price_kg' => $this->price_kg,
            'max_bonus_percentage' => $this->max_bonus_percentage,
            'max_discount_percentage' => $this->max_discount_percentage,
            'forShop' => $this->forShop,
        ]);

        return $dataProvider;
    }
}
