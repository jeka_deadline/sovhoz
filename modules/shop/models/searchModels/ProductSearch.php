<?php

namespace app\modules\shop\models\searchModels;

use app\modules\shop\models\Category;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Product;
use app\modules\user\models\User;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form about `app\modules\shop\models\Product`.
 */
class ProductSearch extends Product
{
    public $numberRowPage = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'max_bonus_percentage', 'max_discount_percentage'], 'integer'],
            [['name', 'description', 'created_at'], 'safe'],
            [['price_bag', 'price_kg'], 'number'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
       
        $query = Product::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->numberRowPage) {
            $dataProvider->pagination->pageSize = $this->numberRowPage;
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price_bag' => $this->price_bag,
            'price_kg' => $this->price_kg,
            'max_bonus_percentage' => $this->max_bonus_percentage,
            'max_discount_percentage' => $this->max_discount_percentage,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function setFilterCategory($dataProvider, $category) {
        $dataProvider->query->andFilterWhere([
            'category_id' => $category->id,
        ]);

        return $dataProvider;
    }

    public function searchCategories($params)
    {
        $query = Product::find();

        $this->load($params);

        $query->groupBy('category_id');

        if (!$this->validate()) {
            return ArrayHelper::map($query->all(), 'category_id', 'category_id');
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price_bag' => $this->price_bag,
            'price_kg' => $this->price_kg,
            'max_bonus_percentage' => $this->max_bonus_percentage,
            'max_discount_percentage' => $this->max_discount_percentage,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return  Category::find()
            ->where(['parent_id' => 0])
            ->andWhere(['id' => ArrayHelper::map($query->all(), 'category_id', 'category_id')])
            ->orderBy('name')
            ->all();
    }
}
