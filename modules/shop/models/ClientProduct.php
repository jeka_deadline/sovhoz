<?php
namespace app\modules\shop\models;

use app\modules\user\models\User;
use Yii;
use app\models\shop\ProductPrice;


class ClientProduct extends Product
{
    public function afterFind()
    {
        parent::afterFind();

        $user = User::find()
            ->where(['id' => Yii::$app->user->identity->getId()])
            ->one();

        if ($user && ($listPrice = ProductPrice::find()->where(['product_id' => $this->id, 'forShop'=>$user->now_in_shop])->one()))
        {
            $this->price_bag = $listPrice->price_bag;
            $this->price_kg = $listPrice->price_kg;
            if ($listPrice->max_bonus_percentage) {
                $this->max_bonus_percentage = $listPrice->max_bonus_percentage;
            }
            if ($listPrice->max_discount_percentage) {
                $this->max_discount_percentage = $listPrice->max_discount_percentage;
            }
        }
    }
}