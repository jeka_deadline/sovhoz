<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\ClientPurchasesHistory as BaseClientPurchasesHistory;
use app\modules\user\models\Profile;
use app\modules\shop\models\Category;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\grid\DataColumn;
use kartik\grid\SerialColumn;

class ClientPurchasesHistory extends BaseClientPurchasesHistory
{

    public static $reportHash;
    public static $reportCountKg;
    public static $reportCountBag;

    public function getSellerHistory()
    {
        return $this->hasOne(SellerHistory::className(), ['id' => 'seller_history_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id'])
                    ->viaTable(Client::tableName(), ['user_id' => 'client_user_id']);
    }

    public function getSeller()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id'])
                    ->viaTable(SellerHistory::tableName(), ['user_id' => 'seller_history_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getPayBonus()
    {
        $countPaymentBonuses = $this->sellerHistory->payment_bonus;

        if (!$countPaymentBonuses) {
            return 0;
        }

        return $countPaymentBonuses / $this->sellerHistory->getProducts()->count();
    }

    public static function getDataCount($searchModel)
    {
        $hash = md5($searchModel->product_id . $searchModel->dateFrom . $searchModel->dateTo . $searchModel->sellerId . $searchModel->shopId);

        if ($hash === self::$reportHash) {
            $dataCount = [
                'kg' => self::$reportCountKg,
                'bag' => self::$reportCountBag,
            ];
        } else {
            $dataCount = [
                'kg' => 0,
                'bag' => 0,
            ];

            $query = self::find()
                              ->where(['product_id' => $searchModel->product_id])
                              ->joinWith('sellerHistory')
                              ->andWhere([SellerHistory::tableName() . '.`status`' => 'good']);

            if ($searchModel->dateFrom) {
                $query->andWhere(['>=', new Expression('DATE(' . SellerHistory::tableName() . '.`date`)'), $searchModel->dateFrom]);
            }
            if ($searchModel->dateTo) {
                $query->andWhere(['<=', new Expression('DATE(' . SellerHistory::tableName() . '.`date`)'), $searchModel->dateTo]);
            }
            if ($searchModel->shopId) {
                $query->andWhere([SellerHistory::tableName() . '.`shop_id`' => $searchModel->shopId]);
            }

            $sellerUserIds = [];

            if ($searchModel->sellerId) {
                $sellerUserIds = ArrayHelper::map(Profile::find()->where(['like', 'surname', $searchModel->sellerId])->all(), 'user_id', 'user_id');
                $query->andWhere(['seller_user_id' => $sellerUserIds]);
            }

            foreach ($query->asArray()->all() as $model) {
                $dataCount[ $model[ 'type_product' ] ] += $model[ 'count' ];
            }

            self::$reportHash     = $hash;
            self::$reportCountBag = $dataCount[ 'bag' ];
            self::$reportCountKg  = $dataCount[ 'kg' ];
        }

        return $dataCount;
    }

    public static function getDataCategory($searchModel)
    {
        $hash = md5($searchModel->product_id . $searchModel->dateFrom . $searchModel->dateTo . $searchModel->sellerId . $searchModel->shopId);

        if ($hash === self::$reportHash) {
            $dataCount = [
                'kg' => self::$reportCountKg,
                'bag' => self::$reportCountBag,
            ];
        } else {
            $dataCount = [
                'kg' => 0,
                'bag' => 0,
            ];
            
            

            $category = Category::find()
                              ->where(['id' => $searchModel->category_id])->one();

            $cat_prod = Product::find()
                              ->where(['category_id' => $category->id])
                              ->all();  
            
            foreach ($cat_prod as $key => $id ){
                $query = self::find()
                              ->where(['product_id' => $id])
                              ->joinWith('sellerHistory')
                              ->andWhere([SellerHistory::tableName() . '.`status`' => 'good']);

            if ($searchModel->dateFrom) {
                $query->andWhere(['>=', new Expression('DATE(' . SellerHistory::tableName() . '.`date`)'), $searchModel->dateFrom]);
            }
            if ($searchModel->dateTo) {
                $query->andWhere(['<=', new Expression('DATE(' . SellerHistory::tableName() . '.`date`)'), $searchModel->dateTo]);
            }
            if ($searchModel->shopId) {
                $query->andWhere([SellerHistory::tableName() . '.`shop_id`' => $searchModel->shopId]);
            }

            $sellerUserIds = [];

            if ($searchModel->sellerId) {
                $sellerUserIds = ArrayHelper::map(Profile::find()->where(['like', 'surname', $searchModel->sellerId])->all(), 'user_id', 'user_id');
                $query->andWhere(['seller_user_id' => $sellerUserIds]);
            }

            foreach ($query->asArray()->all() as $model) {
                $dataCount[ $model[ 'type_product' ] ] += $model[ 'count' ];
            }

           
            }
            self::$reportHash     = $hash;
            self::$reportCountBag = $dataCount[ 'bag' ];
            self::$reportCountKg  = $dataCount[ 'kg' ];
            
        }   
        return $dataCount;
    }

    public function attributeLabels()
    {
        return [
            'date' => Yii::t('core', 'Date'),
            'client_id' => Yii::t('shop', 'Client'),
            'category_id' => Yii::t('shop', 'Category'),
            'product_id' => Yii::t('product', 'Product'),
            'bonus' => Yii::t('product', 'Bonus'),
            'discount' => Yii::t('product', 'Discount'),
            'full_sum' => Yii::t('product', 'Full sum'),
            'payment_bonus' => Yii::t('shop', 'Pay bonuses'),
            'price_one_count' => Yii::t('product', 'Price one product'),

        ];
    }

    public static function getGridColumns()
    {
        $listProducts = ArrayHelper::map(Product::find()->all(), 'id', 'name');
        $listShops    = ArrayHelper::map(Shop::find()->all(), 'id', 'name');
        $listCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');

        return [
        [
            'class' => SerialColumn::className(),
        ],
        [
            'class' => DataColumn::className(),
            'value' => function($model){  if($model->sellerHistory != null) return $model->sellerHistory->date;},
            'header' => Yii::t('core', 'Date'), 
        ],
        [
            'attribute' => 'shopId',
            'value' => function($model){ if(($model->sellerHistory) && ($model->sellerHistory->shop)) return $model->sellerHistory->shop->name;},
            'label' => Yii::t('shop', 'Shop'),
            'filter' => $listShops,
        ],
        [
            'attribute' => 'sellerId',
            'value' => function($model){ if($model->sellerHistory != null) return $model->sellerHistory->getSeller();},
            'header' => Yii::t('shop', 'Seller'),
        ],
        [
            'attribute' => 'clientId',
            'value' => function($model){ return $model->sellerHistory->getClient();},
            'header' => Yii::t('shop', 'Client'),
        ],
        [
            'attribute' => 'category_id',
            'value' => function($model) {  if ($model->product->category_id != null) return $model->product->category->name;},
            'filter' => $listCategory,
        ],
        [
            'attribute' => 'product_id',
            'value' => function($model){ return $model->product->name;},
            'filter' => $listProducts,
        ],
        [
            'attribute' => 'count',
            'value' => function($model){ return ($model->type_product === 'bag') ? $model->count : '-';},
            'header' => Yii::t('product', 'Count bag'),
            'filter' => FALSE,
        ],
        [
            'attribute' => 'count',
            'value' => function($model){ return ($model->type_product === 'kg') ? $model->count : '-';},
            'header' => Yii::t('product', 'Count kilogramm'),
            'filter' => FALSE,
        ],
        [
            'attribute' => 'price_one_count',
            'filter' => FALSE,
            'value' => function($model) {return Product::priceWithCurrency($model->price_one_count);},
        ],

        [
            'attribute' => 'full_sum',
            'filter' => FALSE,
            'value' => function($model) {return Product::priceWithCurrency($model->full_sum);},
        ],
        [
            'attribute' => 'bonus',
            'filter' => FALSE,
            'value' => function($model) {return Product::priceWithCurrency($model->bonus);},
        ],
        [
            'attribute' => 'payment_bonus',
            'filter' => FALSE,
            'value' => function($model) {return Product::priceWithCurrency($model->payment_bonus);},
        ],
        [
            'attribute' => 'discount',
            'filter' => FALSE,
            'value' => function($model) {return Product::priceWithCurrency($model->discount);},
        ],
        [
            'attribute' => 'sum',
            'value' => function($model) {return Product::priceWithCurrency($model->sum);},
            'filter' => FALSE,
            'label' => Yii::t('shop', 'Total'),
        ],

    ];

    }
}