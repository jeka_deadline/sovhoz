<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\LinkPercentageSumSale as BaseLinkPercentageSumSale;

class LinkPercentageSumSale extends BaseLinkPercentageSumSale
{

    public function attributeLabels()
    {
        return [
            'percent'   => Yii::t('percentage', 'Percent'),
            'sum_sale'  => Yii::t('shop', 'Sum sale'),
        ];
    }

}