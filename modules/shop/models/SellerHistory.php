<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\SellerHistory as BaseSellerHistory;
use app\modules\user\models\Profile;
use app\modules\shop\models\ClientPurchasesHistory;

class SellerHistory extends BaseSellerHistory
{

    public function getSellerUserProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'seller_user_id']);
    }

    public function getClientUserProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'client_user_id']);
    }

    public function getProducts()
    {
        return $this->hasMany(ClientPurchasesHistory::className(), ['seller_history_id' => 'id']);
    }

    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

    public function getSeller()
    {
        if (!$this->sellerUserProfile) {
            return NULL;
        }

        return $this->sellerUserProfile->surname . ' ' . $this->sellerUserProfile->name . ' ' . $this->sellerUserProfile->patronymic;
    }

    public function getClient()
    {
        if (!$this->clientUserProfile) {
            return NULL;
        }

        return $this->clientUserProfile->surname . ' ' . $this->clientUserProfile->name . ' ' . $this->clientUserProfile->patronymic;
    }

    public function getCard()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    public function getPayMethod()
    {
        switch ($this->pay_method) {
            case 'cash':
                return Yii::t('shop', 'Cash');
            case 'non-cash':
                return Yii::t('shop', 'Non cash');
            default:
                return NULL;
        }
    }

    public function getDate($format)
    {
        return strtr($format, [
            '{date}' => Yii::$app->formatter->asDate($this->date, 'php:d-m-y'),
            '{time}' => Yii::$app->formatter->asDate($this->date, 'php:H:i:s'),
        ]);
    }

    public function saveLinkClientShop()
    {
        if ($this->client_user_id) {
            $modelLinkClientShop = LinkClientShop::find()->where(['shop_id' => $this->shop_id, 'client_id' => $this->client_user_id])->one();

            if (!$modelLinkClientShop) {

                $modelLinkClientShop            = new LinkClientShop();
                $modelLinkClientShop->client_id = $this->client_user_id;
                $modelLinkClientShop->shop_id   = $this->shop_id;

                $modelLinkClientShop->save();
            }
        }

    }

}