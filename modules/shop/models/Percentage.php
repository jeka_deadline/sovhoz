<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\Percentage as BasePercentage;
use yii\helpers\ArrayHelper;

class Percentage extends BasePercentage
{

    public function getFormElements()
    {
        $fields =  [
            'value'  => ['type' => 'text'],
        ];

        return $fields;
    }

    public function attributeLabels()
    {
        return [
            'value' => Yii::t('percentage', 'Value'),
        ];
    }

    public static function getListPercentages()
    {
        return ArrayHelper::map(Percentage::find()->all(), 'id', function($model){
            return $model->value . '%';
        });
    }
}