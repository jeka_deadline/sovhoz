<?php

namespace app\modules\shop\models;

use Yii;
use app\models\shop\LinkClientShop as BaseLinkClientShop;
use app\modules\user\models\User;
use yii\helpers\ArrayHelper;

class LinkClientShop extends BaseLinkClientShop
{

    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }

    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

}
