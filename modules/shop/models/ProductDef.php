<?php
namespace app\modules\shop\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\shop\ProductDef as BaseProductDef;
use app\models\shop\ProductDef;
use app\modules\shop\models\Shop;


class ProductDef extends BaseProductDef
{

   

    public function getViewAttributes()
    {
        return ['id', 'category_id', 'name', 'description', 'price_kg', 'price_bag', 'max_bonus_percentage', 'max_discount_percentage', 'check_one'];
    }


    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('category', 'Name'),
            'name' => Yii::t('product', 'Name'),
            'description' => Yii::t('product', 'Description'),
            'check_one' => Yii::t('product', 'Check One'),
            'price_bag' => Yii::t('product', 'Price bag'),
            'price_kg' => Yii::t('product', 'Price kilogramm'),
            'max_bonus_percentage' => Yii::t('product', 'Max bonus'),
            'max_discount_percentage' => Yii::t('product', 'Max discount'),
            'forShop' => Yii::t('product', 'For Shop'),
        ];
    }


}