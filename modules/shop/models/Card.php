<?php
namespace app\modules\shop\models;

use Yii;
use app\models\shop\Card as BaseCard;
use app\modules\user\models\Profile;
use yii\helpers\ArrayHelper;

class Card extends BaseCard
{
        public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[['number', 'unique', 'targetClass' => self::className(), 'targetAttribute' => 'number']]);
    }

    public function getFormElements()
    {
        
        $fields =  [
            'number' =>  ['type' => 'text'],
            'value' => ['type' => 'text'],
            'total' => ['type' => 'text'],
            
        ];

       

        if ($this->type === 'bonus') {
            $fields[ 'bonus_sum' ] = ['type' => 'text'];
        }

        if (!$this->isNewRecord) {
            unset($fields[ 'number' ]);
        }

        return $fields;
    }

    public function setBonusType()
    {
        $this->type = 'bonus';
    }

    public function setDiscountType()
    {
        $this->type = 'discount';
    }

    public static function getListTypesCard()
    {
        return [
            'bonus' => Yii::t('card', 'Bonus type'),
            'discount' => Yii::t('card', 'Discount type'),
        ];
    }
    public static function checkTypeCard($type)
    {
        switch ($type) {
            case 'bonus':
                return 'bonus';
            case 'discount':
                return 'discount';
            default:
                return NULL;
        }
    }

    public function getTypeCard()
    {
        switch ($this->type) {
            case 'bonus':
                return Yii::t('card', 'Bonus type');
            case 'discount':
                return Yii::t('card', 'Discount type');
            default:
                return Yii::t('card', 'Not has type');
        }
    }

    public function getLinkUser()
    {
        $profile = $this->userProfile;

        if (!$profile) {
            return NULL;
        }

        return $profile->surname . ' ' . $profile->name . ' ' . $profile->patronymic;
    }

    public function attributeLabels()
    {
        return [
            'type' => Yii::t('card', 'Type card'),
            'number' => Yii::t('card', 'Number'),
            'value' => Yii::t('card', 'Value'),
            'total' => Yii::t('card', 'Total'),
            'bonus_sum' => Yii::t('card', 'Bonus sum'),
        ];
    }

    public function getUserProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id'])
                    ->viaTable(Client::tableName(), ['card_id' => 'id']);
    }


}