<?php

use yii\widgets\ListView;

?>

<?= ListView::widget([
    'dataProvider'  => $dataProvider,
    'itemView'      => '_item_tab_product',
    'viewParams' =>[
        'page' => $page
    ],
    //'summary'       => FALSE,
    'layout'        => '{items}',
    'itemOptions'   => [
        'tag' => FALSE,
    ],
    'options'       => [
        'tag' => FALSE,
    ],
]); ?>
