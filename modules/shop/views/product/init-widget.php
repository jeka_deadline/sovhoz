<?php
use maxeko\devbridge\Autocomplete;
use yii\helpers\Url;
?>

<?= Autocomplete::widget([
    'input' => '#i-input',
    'options' => [
        'serviceUrl' => '"' . Url::toRoute(['/shop/product/search']) . '"',
        'paramName' => "'q'",
        'dataType' => "'json'",
        'onSelect' => "function (suggestion) { 
            $.ajax({
                url: '/shop/product/get-row-sale-product',
                data: {id: suggestion.data},
                dataType: 'JSON',
            }).done(function(data){
                if (data.status == 'ok') {
                    $(document).find('#i-input').closest('tr').replaceWith(data.html);
                    //$('#form-sale-product tbody').append(data.html);
                }
            })
            //console.log('Value: ' + suggestion.value);
            //console.log('Data: ' + suggestion.data);
        }",
        'transformResult' => "function(response) {
            return {
                suggestions: $.map(response, function(item) {
                    return { value: item.name, data: item.id };
                })
            };
        }"
        // and any other options form https://www.devbridge.com/sourcery/components/jquery-autocomplete/
        // enclose string-value parameters by additional quotes "'paramValue'"
    ] 
]) ?>