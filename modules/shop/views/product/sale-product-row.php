<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\shop\models\Product;
?>

<tr>
    <td>

        <?= $product[ 'name' ]; ?>
        <?= Html::activeHiddenInput($model, 'productIds[]', ['class' => 'product-id-input', 'value' => $product[ 'product_id' ]]); ?>

    </td>
    <td>
         <?php if ($product['check_one'] != 1) { ?>
         <?= Html::activeTextInput($model, 'countKg[' . $product[ 'product_id' ] . ']', ['class' => 'product-price-kg-input form-control', 'value' => $product[ 'count_kg' ]]); ?>
         <?php } ?>
        
    </td>
    <td>

        <?= Html::activeTextInput($model, 'countBag[' . $product[ 'product_id' ] .']', ['class' => 'product-price-bag-input form-control', 'value' => $product[ 'count_bag' ]]); ?>

    </td>
    <td>

        <?= Product::priceWithCurrency($product[ 'price_kg' ]); ?>

    </td>
    <td>

        <?= Product::priceWithCurrency($product[ 'price_bag' ]); ?>

    </td>
    <td>
        <a href="#" class="js-link-remove-product" data-url="<?= Url::toRoute(['/shop/sale/remove-product-from-sale', 'id' => $product[ 'product_id' ]]); ?>"><?= Yii::t('product', 'Remove product sale'); ?></a>
         
    </td>
   
</tr>