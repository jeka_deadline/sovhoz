<?php

use app\assets\PreloaderAsset;
use app\modules\shop\assets\ProductIndexAsset;
use yii\helpers\Html;
use yii\grid\GridView;

PreloaderAsset::register($this);
ProductIndexAsset::register($this);

$this->title = Yii::t('product', 'List products');

?>

<div class="crud-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('core', "Create"), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_form_search', [
        'searchModel' => $searchModel,
        'listPercentages' => $listPercentages
    ]) ?>

    <div class="panel-group accordion-categories" role="tablist" aria-multiselectable="true">
        <?php foreach ($rootCategories as $category): ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading_<?= $category->id ?>">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" href="#collapse_<?= $category->id ?>" aria-expanded="true" aria-controls="collapse_<?= $category->id ?>" data-category-id="<?= $category->id ?>">
                            <?= $category->name ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse_<?= $category->id ?>" class="panel-collapse collapse preloader-target" role="tabpanel" aria-labelledby="heading_<?= $category->id ?>">
                    <div class="panel-body">

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>