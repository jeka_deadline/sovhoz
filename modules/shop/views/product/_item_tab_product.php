<?php

use yii\helpers\Url;

?>
<tr>
    <td><?= $page + $index+1; ?></td>
    <td><?= $model->name ?></td>
    <td><?= $model->price_bag ?></td>
    <td><?= $model->price_kg ?></td>
    <td><?= ($model->bonus ? $model->bonus->value . '%' : NULL) ?></td>
    <td><?= ($model->discount ? $model->discount->value . '%' : NULL) ?></td>
    <td>
        <a href="<?= Url::toRoute(['view','id'=>$model->id])?>">
            <span class="glyphicon glyphicon-eye-open"></span>
        </a>
        <a href="<?= Url::toRoute(['update','id'=>$model->id])?>">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <a href="<?= Url::toRoute(['delete','id'=>$model->id])?>" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
            <span class="glyphicon glyphicon-trash"></span>
        </a>
    </td>
</tr>