<?php

use app\modules\shop\models\Category;
use app\modules\shop\models\ProductUnit;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'category_id')->dropDownList(Category::getListCategories(), ['prompt' => Yii::t('category', 'Choose category')]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>

        <?= $form->field($model, 'check_one')->checkbox(); ?>

        <?= $form->field($model, 'price_bag')->textInput(['maxlength' => true]) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'price_bag_mass')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'price_bag_unit')->dropDownList(ProductUnit::getAllListUnit(), ['prompt' => Yii::t('product', 'Choose unit')])->label('&emsp;') ?>
            </div>
        </div>

<!--        --><?//= $form->field($model, 'price_bag')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'price_kg')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'max_bonus_percentage')->dropDownList($listPercentages, ['prompt' => Yii::t('product', 'Choose max bonus')]); ?>

        <?= $form->field($model, 'max_discount_percentage')->dropDownList($listPercentages, ['prompt' => Yii::t('product', 'Choose max discount')]); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('core', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

