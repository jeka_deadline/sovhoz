<?php

use app\assets\PreloaderAsset;
use app\modules\shop\assets\ProductUpdateAsset;
use yii\helpers\Html;

PreloaderAsset::register($this);
ProductUpdateAsset::register($this);

$this->title = Yii::t('product', 'Update product');

?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listPercentages' => $listPercentages
    ]); ?>

    <div class="grid-view-price-lists">
        <?= $this->render('price/_index', [
            'dataProviderPriceLists' => $dataProviderPriceLists,
            'listPercentages' => $listPercentages,
        ]); ?>
    </div>

    <?= $this->render('price/_add-form', [
        'model' => $model,
        'listPercentages' => $listPercentages,
        'modelProductPrice' => $modelProductPrice,
    ]); ?>

    <?= $this->render('price/_add-multiple-form', [
        'model' => $model,
        'listPercentages' => $listPercentages,
        'formMultiplePrice' => $formMultiplePrice,
        'modelProductPrice' => $modelProductPrice,
    ]); ?>

</div>

