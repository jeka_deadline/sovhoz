<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="modal fade" id="modalCreatePrice" tabindex="-1" role="dialog" aria-labelledby="modalCreatePriceLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalCreatePriceLabel">Добавить новую цену товару</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'action' => ['create-price'],
                    'id' => 'form-add-price',
                ]); ?>

                    <?= $form->field($modelProductPrice, 'product_id')->hiddenInput(['value' => $model->id])->label(false) ?>

                    <?= $form->field($modelProductPrice, 'forShop')->dropDownList($modelProductPrice->getShop())?>

                    <?= $form->field($modelProductPrice, 'price_bag')->textInput(['value' => $model->price_bag]) ?>

                    <?= $form->field($modelProductPrice, 'price_kg')->textInput(['value' => $model->price_kg]) ?>

                    <?= $form->field($modelProductPrice, 'max_bonus_percentage')->dropDownList($listPercentages, [
                        'prompt' => Yii::t('product', 'Choose max bonus'),
                        'value' => $model->max_bonus_percentage,
                    ]); ?>

                    <?= $form->field($modelProductPrice, 'max_discount_percentage')->dropDownList($listPercentages, [
                        'prompt' => Yii::t('product', 'Choose max discount'),
                        'value' => $model->max_discount_percentage,
                    ]); ?>

                    <div class="form-group text-right">
                        <?= Html::submitButton(Yii::t('core', 'Create'), ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>