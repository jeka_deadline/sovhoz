<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="modal fade" id="modalCreateMultiplePrice" tabindex="-1" role="dialog" aria-labelledby="modalCreateMultiplePriceLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalCreateMultiplePriceLabel">Добавить новую цену товару</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'action' => ['create-multiple-price'],
                    'id' => 'form-add-multiple-price',
                ]); ?>

                <?= $form->field($formMultiplePrice, 'product_id')->hiddenInput(['value' => $model->id])->label(false) ?>

                <?= $form->field($formMultiplePrice, 'forShops')->dropDownList($modelProductPrice->getShop(), ['multiple'=>'multiple'])?>

                <?= $form->field($formMultiplePrice, 'price_bag')->textInput(['value' => $model->price_bag]) ?>

                <?= $form->field($formMultiplePrice, 'price_kg')->textInput(['value' => $model->price_kg]) ?>

                <?= $form->field($formMultiplePrice, 'max_bonus_percentage')->dropDownList($listPercentages, [
                    'prompt' => Yii::t('product', 'Choose max bonus'),
                    'value' => $model->max_bonus_percentage,
                ]); ?>

                <?= $form->field($formMultiplePrice, 'max_discount_percentage')->dropDownList($listPercentages, [
                    'prompt' => Yii::t('product', 'Choose max discount'),
                    'value' => $model->max_discount_percentage,
                ]); ?>

                <div class="form-group text-right">
                    <?= Html::submitButton(Yii::t('core', 'Create'), ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>