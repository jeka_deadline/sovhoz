<?php

use yii\grid\GridView;
use yii2mod\editable\EditableColumn;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<h3>Стоимость</h3>

<p class="text-right">
    <?= Html::a('Добавить', null, [
        'class' => 'btn btn-success',
        'data-toggle' => 'modal',
        'data-target' => '#modalCreatePrice'
    ]) ?>
    <?= Html::a('Добавить магазины', null, [
        'class' => 'btn btn-success',
        'data-toggle' => 'modal',
        'data-target' => '#modalCreateMultiplePrice'
    ]) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProviderPriceLists,
    'columns' => [
        [
            'attribute' => 'forShop',
            'value' => function($model){
                return $model->oneShop?$model->oneShop->name:null;
            }
        ],
        [
            'class' => EditableColumn::class,
            'attribute' => 'price_bag',
            'url' => ['change'],
        ],
        [
            'class' => EditableColumn::class,
            'attribute' => 'price_kg',
            'url' => ['change'],
        ],
        [
            'class' => EditableColumn::class,
            'attribute' => 'max_bonus_percentage',
            'url' => ['change'],
            'type' => 'select',
            'editableOptions' => function ($model) use ($listPercentages) {
                return [
                    'source' => ['' => Yii::t('product', 'Choose max bonus')] + $listPercentages,
                    'value' => $model->max_bonus_percentage,
                ];
            },
            'value' => function($model) use ($listPercentages) {
                return isset($listPercentages[$model->max_bonus_percentage])?$listPercentages[$model->max_bonus_percentage]:null;
            }
        ],
        [
            'class' => EditableColumn::class,
            'attribute' => 'max_discount_percentage',
            'url' => ['change'],
            'type' => 'select',
            'editableOptions' => function ($model) use ($listPercentages) {
                return [
                    'source' => ['' => Yii::t('product', 'Choose max discount')] + $listPercentages,
                    'value' => $model->max_discount_percentage,
                ];
            },
            'value' => function($model) use ($listPercentages) {
                return isset($listPercentages[$model->max_discount_percentage])?$listPercentages[$model->max_discount_percentage]:null;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    $url = Url::to(['delete-price', 'id' => $model->id]);
                    return Html::a(Html::tag('span', null, ['class'=>'glyphicon glyphicon-trash']), $url, [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'class' => 'button-delete-price'
                    ]);
                },
            ],
        ],
    ],
]); ?>