<?php if ($categories):?>
    <div class="panel-group accordion-categories" role="tablist" aria-multiselectable="true">
        <?php foreach ($categories as $category): ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading_<?= $category->id ?>">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" href="#collapse_<?= $category->id ?>" aria-expanded="true" aria-controls="collapse_<?= $category->id ?>" data-category-id="<?= $category->id ?>">
                            <?= $category->name ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse_<?= $category->id ?>" class="panel-collapse collapse preloader-target" role="tabpanel" aria-labelledby="heading_<?= $category->id ?>">
                    <div class="panel-body">

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?= Yii::t('product', 'Name') ?></th>
        <th><?= Yii::t('product', 'Price bag') ?></th>
        <th><?= Yii::t('product', 'Price kilogramm') ?></th>
        <th><?= Yii::t('product', 'Max bonus') ?></th>
        <th><?= Yii::t('product', 'Max discount') ?></th>
        <th><?= Yii::t('core', 'Actions') ?></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<div class="row">
    <div class="col-lg-12 text-center">
        <button type="button" class="btn btn-success hidden more-product" data-category-id="<?= $rootCategory->id ?>"><?= Yii::t('product', 'Show more products') ?></button>
    </div>
</div>