<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'id' => 'form-search-product',
]); ?>
    <?= Html::hiddenInput('sort', ''); ?>


    <table class="table table-striped table-bordered">
        <thead>
            <tr class="filter-sort">
                <th>
                    <a href="#" data-sort="name">
                        <?= Yii::t('product', 'Name') ?>
                    </a>
                </th>
                <th>
                    <a href="#" data-sort="price_bag">
                        <?= Yii::t('product', 'Price bag') ?>
                    </a>
                </th>
                <th>
                    <a href="#" data-sort="price_kg">
                        <?= Yii::t('product', 'Price kilogramm') ?>
                    </a>
                </th>
                <th>
                    <a href="#" data-sort="max_bonus_percentage">
                        <?= Yii::t('product', 'Max bonus') ?>
                    </a>
                </th>
                <th>
                    <a href="#" data-sort="max_discount_percentage">
                        <?= Yii::t('product', 'Max discount') ?>
                    </a>
                </th>
            </tr>
            <tr>
                <td>
                    <?= $form->field($searchModel, 'name')->label(false) ?>
                </td>
                <td>
                    <?= $form->field($searchModel, 'price_bag')->label(false) ?>
                </td>
                <td>
                    <?= $form->field($searchModel, 'price_kg')->label(false) ?>
                </td>
                <td>
                    <?= $form->field($searchModel, 'max_bonus_percentage')->dropDownList($listPercentages, ['prompt' => ''])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($searchModel, 'max_discount_percentage')->dropDownList($listPercentages, ['prompt' => ''])->label(false); ?>
                </td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
<?php ActiveForm::end(); ?>