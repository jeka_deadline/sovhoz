<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\shop\models\Product;
?>

<tr>
    <td>

        <?= $product->name; ?>
        <?= Html::activeHiddenInput($model, 'productIds[]', ['class' => 'product-id-input', 'value' => $product->id]); ?>

    </td>
    <td>
        <?php if ($product->check_one != 1) { ?>
        <?= Html::activeTextInput($model, 'countKg[' . $product->id . ']', ['class' => 'product-price-kg-input form-control']); ?>
         <? } ?>
    </td>
    <td>

        <?= Html::activeTextInput($model, 'countBag[' . $product->id .']', ['class' => 'product-price-bag-input form-control']); ?>

    </td>
    <td>

        <?= Product::priceWithCurrency($product->price_kg); ?>

    </td>
    <td>

        <?= Product::priceWithCurrency($product->price_bag); ?>

    </td>
    <td>
        <a href="#" class="js-link-remove-product" data-url="<?= Url::toRoute(['/shop/sale/remove-product-from-sale', 'id' => $product->id]); ?>"><?= Yii::t('product', 'Remove product sale'); ?></a>    
    </td>
    <td>
        <a href="#" class="js-link-auto-product btn btn-default" data-url="<?= Url::toRoute(['/shop/sale/auto', 'id' => $product->id ,
        'pricekg'=> $product->price_kg]);?>"><?= Yii::t('product', 'Cal product sale'); ?></a>
    </td>
</tr>