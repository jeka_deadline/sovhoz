<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/shop/report/put-seller-deposit', 'reportId' => $report->id],
    'enableClientValidation' => FALSE,
    'enableAjaxValidation' => TRUE,
    'validationUrl' => ['/shop/report/validate-form-put-deposit', 'reportId' => $report->id],
]); ?>

    <table class="table">
        <tr>
            <td>Сумма сдачи:</td>
            <td><?= $form->field($model, 'sum')->label(FALSE); ?></td>
        </tr>
        <tr>
            <td>Остаток с прошлого раза:</td>
            <td><?= $report->getLastDeposit(); ?></td>
        </tr>
    </table>

    <?= Html::submitButton('Внести', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>