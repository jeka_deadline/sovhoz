<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use app\assets\AdminReportAsset;
use trntv\yii\datetime\DateTimeWidget;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
?>

<?php $form = ActiveForm::begin([
    'method' => 'get',
]); ?>

    <?= $form->field($searchModel, 'dateFrom')->widget(DateTimeWidget::className(), [
        'phpDatetimeFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'useStrict' => TRUE,
            'showClear' => TRUE
        ]])->label(Yii::t('core', 'Date from')); ?>

    <?= $form->field($searchModel, 'dateTo')->widget(DateTimeWidget::className(), [
        'phpDatetimeFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'useStrict' => TRUE,
            'showClear' => TRUE
        ]])->label(Yii::t('core', 'Date to')); ?>

    <?= Html::submitButton(Yii::t('core', 'Search'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>

<br>

<?= ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'fontAwesome' => true,
    'dropdownOptions' => [
        'label' => 'Export All',
        'class' => 'btn btn-default'
    ]
]); ?>

<br>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'gridview-admin-report',
    'filterModel' => $searchModel,
    'columns' => $gridColumns
]); ?>

<?= $this->render('data-count-report', [
    'dataCountKg' => $dataCountKg,
    'dataCountBag' => $dataCountBag,
]); ?>