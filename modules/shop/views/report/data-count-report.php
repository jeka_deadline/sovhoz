<table class="table">
    <tr>
        <th><?= Yii::t('shop', 'Count kilogramms'); ?></th>
        <td><?= $dataCountKg; ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'Count bags'); ?></th>
        <td><?= $dataCountBag; ?></td>
    </tr>
</table>
