<?php
use app\modules\shop\models\Product;
?>

<table class="table">
    <tr>
        <th colspan="2"><?= Yii::t('shop', 'Passed:'); ?></th>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'Cash on hand, rub'); ?></th>
        <td><?= Product::priceWithCurrency($report->total_cash_sum); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'Non-cash funds, rub'); ?></th>
        <td><?= Product::priceWithCurrency($report->total_non_cash_sum); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'Total amount of discounts, rub'); ?></th>
        <td><?= Product::priceWithCurrency($report->total_discount_sum); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'The total amount of accrued bonuses, rub'); ?></th>
        <td><?= Product::priceWithCurrency($report->total_bonuses_sum); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'Total amount of discounted bonuses, rub'); ?></th>
        <td><?= Product::priceWithCurrency($report->total_pay_bonuses_sum); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('shop', 'Total sale kg'); ?></th>
        <td><?= $report->total_sale_kg; ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('user', 'Last enter'); ?></th>
        <td><?= $report->last_enter; ?></td>
    </tr>
    <tr>
        <th></th>
        <td></td>
    </tr>
    <tr>
        <th><?= Yii::t('user', 'Administrator'); ?></th>
        <td><?= $report->getSeller(); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t('core', 'Signature'); ?></th>
        <td></td>
    </tr>
</table>