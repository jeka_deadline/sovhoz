<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\yii\datetime\DateTimeWidget;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DateTimeWidget::className(), [
        'phpDatetimeFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'useStrict' => TRUE,
            'showClear' => TRUE
    ]])->label(Yii::t('core', 'Date')); ?>

    <?= Html::submitButton(Yii::t('shop', 'Find reports'), ['class' => 'btn btn-primary']); ?>

    <br><?= $result; ?><br>

    <?php foreach ($reports as $report) : ?>

        <?= Html::a(Yii::t('shop', 'Report seller {seller}', ['seller' => $report->getSeller()]), ['show-report', 'id' => $report->id], ['target' => '_blank']); ?><br>

    <?php endforeach; ?>

<?php ActiveForm::end(); ?>