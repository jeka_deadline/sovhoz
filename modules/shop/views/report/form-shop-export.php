<?php
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\user\models\forms\CreateSellerForm;
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'shopIds')->widget(Select2::classname(), [
    'data' => $model->getShopsList(),
    'language' => 'ru',
    'options' => ['placeholder' =>  Yii::t('shop' , 'Choose shop'), 'multiple' => TRUE],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]); ?>
<?= Html::submitButton(Yii::t('shop', 'Export'), ['class' => 'btn btn-primary']); ?>
<?php ActiveForm::end();?>