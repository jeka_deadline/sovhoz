<?= $this->render('seller-report-header', [
    'report' => $report,
]); ?>

<?= $this->render('total-seller-report-data', [
    'report' => $report,
]); ?>