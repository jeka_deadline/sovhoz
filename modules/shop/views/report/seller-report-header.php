<h1><?= Yii::t('shop', 'Daily report') . ' ' . $report->id . ' - ' . Yii::$app->formatter->asDate($report->date, 'php:d/m/Y'); ?></h1>
<h5><?= Yii::t('shop', 'Shop'); ?>: <?= $report->shop->name; ?></h5>
<h5><?= Yii::t('shop', 'Address'); ?>: <?= $report->shop->address; ?></h5>