<?php
use yii\helpers\Html;
use app\modules\shop\models\Product;
use yii\helpers\Url;
use app\modules\user\models\User;
?>


<?= $this->render('seller-report-header', [
    'report' => $report,
]); ?>

<table class="table">
    <thead>
        <tr>
            <th>№</th>
            <th><?= Yii::t('check', '№ check'); ?></th>
            <th><?= Yii::t('core', 'Date/Time'); ?></th>
            <th><?= Yii::t('shop', 'Sum sale, rub'); ?></th>
            <th><?= Yii::t('shop', 'Pay method'); ?></th>
            <th><?= Yii::t('shop', 'Discount, rub'); ?></th>
            <th><?= Yii::t('shop', 'Bonus, rub'); ?></th>
            <th><?= Yii::t('shop', 'Pay bonuses, rub'); ?></th>
            <th><?= Yii::t('shop', 'Total sum'); ?></th>
        </tr>
    </thead>
    <tbody>

    <?php foreach ($dataSellerHistory as $index => $itemSellerHistory) : ?>

        <tr>
            <td><?= $index + 1; ?></td>
            <td><?= $itemSellerHistory->code; ?></td>
            <td><?= $itemSellerHistory->date; ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->full_sum); ?></td>
            <td><?= $itemSellerHistory->getPayMethod(); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->total_discount_sum); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->total_bonuses_sum); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->payment_bonus); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->total_sum); ?></td>
        </tr>

    <?php endforeach; ?>

    </tbody>
</table>

<?= $this->render('total-seller-report-data', [
    'report' => $report,
]); ?>

<?php if ($report->cashbox_seller_deposit === NULL && Yii::$app->user->identity->role_id == User::ROLE_SELLER) : ?>

    <?= $this->render('form-put-seller-deposit', [
        'model' => $model,
        'report' => $report,
    ]); ?>

<?php else: ?>

    <?= $this->render('seller-deposit', [
        'report' => $report,
    ]); ?>

<?php endif; ?>

<a href="<?= Url::toRoute(['/shop/report/print-report', 'id' => $report->id]); ?>" target="_blank"><?= Yii::t('shop', 'Print report'); ?></a>