<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\shop\assets\ProductAsset;
?>
<?php ProductAsset::register($this); ?>
<?= Html::beginForm(); ?>
  <div class="hotkey">
    <div id="sale-product-wrapper">
        <table class="table" id="form-sale-product" data-url-get-total="<?= Url::toRoute(['/shop/product/get-total-price']); ?>">
            <thead>
                <tr>
                    <th>
                        <?= Yii::t('product', 'Product'); ?>
                    </th>
                    <th>
                        <?= Yii::t('product', 'Amount kilogramm'); ?>
                    </th>
                    <th>
                        <?= Yii::t('product', 'Amount bag'); ?>
                    </th>
                    <th>
                        <?= Yii::t('product', 'Price kilogramm'); ?>  
                    </th>
                    <th>
                        <?= Yii::t('product', 'Price bag'); ?>
                    </th>
                    <th>
                        <?= Yii::t('core', 'Actions'); ?>
                    </th>
                     <th>
                        <?= Yii::t('core', 'Auto'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>

                <?php if ($products) : ?>
                    <?php foreach ($products as $product) : ?>
                     
                        <?= $this->render('@app/modules/shop/views/product/sale-product-row', [
                            'model' => $model,
                            'product' => $product,
                        ]); ?>

                    <?php endforeach; ?>

                <?php else : ?>

                    <?= $this->render('@app/modules/shop/views/product/init-row-sale-product'); ?>

                <?php endif; ?>

            </tbody>
        </table>
        
        <div>
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-url="<?= Url::toRoute(['/shop/product/get-init-row-sale-product']); ?>"></span>
        </div>

        <div id="totals">
            <span></span>
        </div>

    </div><br>
    </div>

    <?= Html::submitInput(Yii::t('shop', 'Go sale'), ['class' => 'btn btn-primary']); ?>

<?= Html::endForm(); ?>