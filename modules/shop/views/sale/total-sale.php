<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\shop\models\Product;
?>
<h1><?= Yii::t('shop', 'Summary'); ?></h1>

<?php $form = ActiveForm::begin(); ?>

    <table class="table">
        <thead>
            <tr>
              <th><?= Yii::t('product', 'Product'); ?></th>
              <th><?= Yii::t('product', 'Amount'); ?></th>
              <th><?= Yii::t('product', 'Type product'); ?></th>
              <th><?= Yii::t('product', 'Price one count'); ?></th>
              <th><?= Yii::t('product', 'Price'); ?></th>
            </tr>
        </thead>

        <?php foreach ($sellerHistory->products as $itemHistoryProduct) : ?>
            <tr>
                <td><?= $itemHistoryProduct->product->name; ?></td>
                <td><?= $itemHistoryProduct->count; ?></td>
                <td><?= Product::getTypeProduct($itemHistoryProduct->type_product); ?></td>
                <td><?= Product::priceWithCurrency(Product::getPriceOneProduct($itemHistoryProduct->product, $itemHistoryProduct->type_product)); ?></td>
                <td><?= Product::priceWithCurrency($itemHistoryProduct->sum); ?></td>
            </tr>

        <?php endforeach; ?>
    </table>

    <?= $form->field($totalSaleForm, 'numberCard')->textInput(['id' => 'numberCard']); ?>
     

    <?= $form->field($totalSaleForm, 'payBonus')->checkbox(); ?>

    

    <?= $form->field($totalSaleForm, 'payMethod')->radioList($totalSaleForm->getListPayMethod()); ?>
    <?= Html::submitButton(Yii::t('shop', 'Finish sale'), ['class' => 'btn btn-primary']); ?>
    <?= Html::a(Yii::t('shop', 'Cancel sale'), Url::toRoute(['/shop/sale/cancel-sale']), ['class' => 'btn btn-default']); ?>
    <?= Html::a(Yii::t('shop', 'Back sale'), Url::toRoute(['/shop/sale/sale']), ['class' => 'btn btn-default']); ?>

<?php ActiveForm::end(); ?>