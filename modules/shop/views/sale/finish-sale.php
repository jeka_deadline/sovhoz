<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\shop\models\Product;

?>
<h1><?= Yii::t('shop', 'Finish sale'); ?></h1>

<table class="table">
    <thead>
        <tr>
          <th><?= Yii::t('product', 'Product'); ?></th>
          <th><?= Yii::t('product', 'Amount'); ?></th>
          <th><?= Yii::t('product', 'Type product'); ?></th>
          <th><?= Yii::t('product', 'Price one count'); ?></th>
          <th><?= Yii::t('product', 'Price'); ?></th>

          <?php if ($sellerHistory->card_type === 'bonus'): ?>

              <th><?= Yii::t('product', 'Bonus') . ' %'; ?></th>
              <th><?= Yii::t('product', 'Accrued bonuses'); ?></th>

          <?php endif; ?>
          <?php if ($sellerHistory->card_type === 'discount'): ?>

              <th><?= Yii::t('product', 'Discount'); ?></th>

          <?php endif; ?>

          <th><?= Yii::t('product', 'Total'); ?></th>
        </tr>
    </thead>

    <?php foreach ($sellerHistory->products as $itemHistoryProduct) : ?>
      <?php if ($itemHistoryProduct->count != 0) { ?>
        <tr>
            <td><?= $itemHistoryProduct->product->name; ?></td>
            <td><?= $itemHistoryProduct->count; ?></td>
            <td><?= Product::getTypeProduct($itemHistoryProduct->type_product); ?></td>
            <td><?= Product::priceWithCurrency(Product::getPriceOneProduct($itemHistoryProduct->product, $itemHistoryProduct->type_product)); ?></td>
            <td><?= Product::priceWithCurrency($itemHistoryProduct->full_sum); ?></td>

            <?php if ($sellerHistory->card_type === 'bonus') : ?>

                <td><?= $itemHistoryProduct->bonus_percent_value . '%'; ?></td>
                <td><?= Product::priceWithCurrency($itemHistoryProduct->bonus); ?></td>

            <?php endif; ?>
            <?php if ($sellerHistory->card_type === 'discount'): ?>

                <td><?= $itemHistoryProduct->discount_percent_value . '%'; ?></td>

            <?php endif; ?>

            <td><?= Product::priceWithCurrency($itemHistoryProduct->sum); ?></td>
        </tr>
    <?php } ?>
    <?php endforeach; ?>

    <tr></tr>
</table>

<?php $form = ActiveForm::begin(); ?>

   
    <?= $form->field($finishSaleForm, 'payBonus')->checkbox(); ?>
    <?= $form->field($finishSaleForm, 'payMethod')->radioList($finishSaleForm->getListPayMethod()); ?>

    <?= Html::submitButton(Yii::t('shop', 'Finish sale'), ['class' => 'btn btn-primary']); ?>
    <?= Html::a(Yii::t('shop', 'Cancel sale'), Url::toRoute(['/shop/sale/cancel-sale']), ['class' => 'btn btn-default']); ?>

<?php ActiveForm::end(); ?>
