<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'percent'); ?>

    <?= $form->field($model, 'sum_sale'); ?>

    <?= Html::submitInput(Yii::t('core', 'Create'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>