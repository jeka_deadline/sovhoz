<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
?>

<a href="<?= Url::toRoute($url); ?>" class="btn btn-primary"><?= Yii::t('shop', 'Create link percentage sum sale'); ?></a><br><br>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => SerialColumn::className(),
        ],
        ['attribute' => 'percent'],
        ['attribute' => 'sum_sale'],
        [
            'class' => ActionColumn::className(),
            'template' => '{delete}',
        ]
    ],
]); ?>