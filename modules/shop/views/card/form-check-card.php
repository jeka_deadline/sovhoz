<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numberCard'); ?>

    <?= Html::submitButton(Yii::t('card', 'Check card'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>