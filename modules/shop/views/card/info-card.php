<?php
use yii\helpers\Url;
use app\modules\shop\models\Product;
use app\modules\user\models\User;
use yii\widgets\DetailView;
use yii\helpers\Html;
?>

<?php if (empty($card)) : ?>

    <?= Yii::t('card', 'Card with this number not found'); ?><br>

    <a href="<?= Url::toRoute(['/shop/card/check-card']); ?>"><?= Yii::t('card', 'Check other card'); ?></a>

<?php else : ?>

    <table class="table">
        <thead>
            <th><?= Yii::t('card', 'Card number'); ?></th>
            <th><?= Yii::t('card', 'Type card'); ?></th>
            <th><?= Yii::t('card', 'Value'); ?></th>
            <th><?= Yii::t('card', 'Bonus sum'); ?></th>
            <th><?= Yii::t('card', 'Total sum sale'); ?></th>
            <th><?= Yii::t('card', 'Link user'); ?></th>
        </thead>
        <tbody>
            <td>
                <?= $card->number; ?>
            </td>
            <td>
                <?= $card->getTypeCard(); ?>
            </td>
            <td>
                <?= $card->value; ?>
            </td>
            <td>
                <?= $card->bonus_sum; ?>
            </td>
            <td>
                <?= Product::priceWithCurrency($card->total); ?>
            </td>
            <td>
                <?= $card->getLinkUser(); ?>
            </td>
        </tbody>
    </table>

<?php endif; ?>
<br>
<br>
<br>
<br>
<?php if ($model != NULL) { ?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'label' => Yii::t('user', 'Surname'),
            'value' => $model->profile->surname,
        ],
        [
            'label' => Yii::t('user', 'Name'),
            'value' => $model->profile->name,
        ],
        [
            'label' => Yii::t('user', 'Patronymic'),
            'value' => $model->profile->patronymic,
        ],
        [
            'label' => Yii::t('user', 'Date birth'),
            'value' => $model->profile->date_birth,
        ],
        [
            'label' => Yii::t('user', 'Phone'),
            'value' => $model->profile->phone,
        ],
    ],
]); ?>
<?php } ?>