<?php

use app\modules\shop\models\Percentage;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\shop\ProductPrice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-price-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <? if(!empty($this->params['breadcrumbs'][2]) && $this->params['breadcrumbs'][2] == 'Update' ) {?>
        <?= $form->field($model, 'product_id')->dropDownList($model->getProductP(), ['disabled'=>TRUE])?>
    <? } else {?>
    	<?= $form->field($model, 'product_id')->dropDownList($model->getProductP())?>

    <? }?>
    <?= $form->field($model, 'price_bag')->textInput() ?>

    <?= $form->field($model, 'price_kg')->textInput() ?>

    <?= $form->field($model, 'max_bonus_percentage')->dropDownList(Percentage::getListPercentages(), ['prompt' => Yii::t('product', 'Choose max bonus')]); ?>

    <?= $form->field($model, 'max_discount_percentage')->dropDownList(Percentage::getListPercentages(), ['prompt' => Yii::t('product', 'Choose max discount')]); ?>

    <? if(!empty($this->params['breadcrumbs'][2]) && $this->params['breadcrumbs'][2] == 'Update' ) {?>
        <?= $form->field($model, 'forShop')->dropDownList($model->getShop(),['disabled'=>TRUE])?>
    <? } else {?>
    	<?= $form->field($model, 'forShop')->dropDownList($model->getShop())?>
    <? }?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
