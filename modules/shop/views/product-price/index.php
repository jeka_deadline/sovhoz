<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\searchModels\ProductPriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление прайсами';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-price-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить цены в магазин', ['copy'], ['class' => 'btn btn-success']) ?>
    </p>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            ['attribute' => 'product_id',
              'filter' => $model->getProductP(),
              'value' => function($model){ return ($model->products) ? $model->products->name : '' ;}
            ],
            'price_bag',
            'price_kg',
            [
                'attribute' => 'max_bonus_percentage',
                'value'     => function($model){ return ($model->bonus) ? $model->bonus->value . '%' : NULL; },
                'filter'    => $listPercentages,
            ],
            [
                'attribute' => 'max_discount_percentage',
                'value'     => function($model){ return ($model->discount) ? $model->discount->value . '%' : NULL; },
                'filter'    => $listPercentages,
            ],
            [
                'attribute' => 'forShop',
                'filter' => $model->getShop(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
