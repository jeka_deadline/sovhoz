<?php
use yii\helpers\Html;
use app\modules\shop\models\Product;
use yii\helpers\Url;
use app\modules\shop\assets\ProductAsset;
?>
<?php ProductAsset::register($this); ?>
<table class="table">
    <thead>
        <tr>
            <th>№</th>
            <th><?= Yii::t('check', '№ check'); ?></th>
            <th><?= Yii::t('core', 'Date/Time'); ?></th>
            <th><?= Yii::t('shop', 'Sum sale, rub'); ?></th>
            <th><?= Yii::t('shop', 'Pay method'); ?></th>
            <th><?= Yii::t('shop', 'Discount, rub'); ?></th>
            <th><?= Yii::t('shop', 'Bonus, rub'); ?></th>
            <th><?= Yii::t('shop', 'Pay bonuses, rub'); ?></th>
            <th><?= Yii::t('shop', 'Total sum'); ?></th>
        </tr>
    </thead>
    <tbody>

    <?php foreach ($dataSellerHistory as $index => $itemSellerHistory) : ?>

        <tr>
            <td><?= $index + 1; ?></td>
            <td><a href="/shop/check/view-check/?code=<?= $itemSellerHistory->code; ?>" style="color:green;"><?= $itemSellerHistory->code; ?></a></td>
            <td><?= $itemSellerHistory->date; ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->full_sum); ?></td>
            <td><?= $itemSellerHistory->getPayMethod(); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->total_discount_sum); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->total_bonuses_sum); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->payment_bonus); ?></td>
            <td><?= Product::priceWithCurrency($itemSellerHistory->total_sum); ?></td>
        </tr>

    <?php endforeach; ?>

    </tbody>
</table>

