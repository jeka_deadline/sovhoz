<?php
    use app\modules\shop\models\Product;
    use app\modules\shop\assets\ProductAsset;

?>
<?php ProductAsset::register($this); ?>
<h1 id="ish1"><?= Yii::t('check', 'Sales check'); ?></h1>

<table class="table change">
      <tbody>
            <tr>
                <td colspan="2"><?= Yii::t('check', 'Number/Date/Time'); ?>: <?= $check->code . '/' . $check->getDate('{date}/{time}'); ?></td>
            </tr>
            <tr>
                <td colspan="2"><?= Yii::t('shop', 'Name'); ?>: <?= ($check->shop) ? $check->shop->name : NULL; ?></td>
            </tr>
            <tr>
                <td colspan="2"><?= Yii::t('shop', 'Requisites'); ?>: <?= ($check->shop) ? $check->shop->requisites : NULL; ?></td>
            </tr>
            <tr>
                <td colspan="2"><?= Yii::t('shop', 'Address'); ?>: <?= ($check->shop) ? $check->shop->address : NULL; ?></td>
            </tr>
            <tr>
              <td><?= Yii::t('shop', 'Phone'); ?>: <?= ($check->shop) ? $check->shop->phone : NULL; ?></td>
              <td><?= Yii::t('shop', 'Seller'); ?>: <?= $check->getSeller(); ?></td>
            </tr>
      </tbody>
</table>
<br>
<table class="table change">
    <tr>
        <th>№</th>
        <th><?= Yii::t('product', 'Product'); ?></th>
        <th><?= Yii::t('core', 'Count'); ?></th>
        <th><?= Yii::t('product', 'kg'); ?>/<?=Yii::t('product', 'bag'); ?></th>
        <th><?= Yii::t('product', 'Price one count'); ?></th>
        <th><?= Yii::t('shop', 'Sum'); ?></th>
        <th><?= Yii::t('product', 'Discount'); ?>/<?= Yii::t('product', 'Bonus'); ?></th>
        <th><?= Yii::t('shop', 'Total sum'); ?></th>
    </tr>

    <?php foreach ($check->getProducts()->with('product')->all() as $index => $itemProductHistory) : ?>
    <?php  if ($itemProductHistory->count != 0) { ?>
        <tr>
            <td><?= $index + 1; ?></td>
            <td><?= $itemProductHistory->product->name; ?></td>
            <td><?= $itemProductHistory->count; ?></td>
            <td><?= Product::getTypeProduct($itemProductHistory->type_product, TRUE); ?></td>
            <td><?= Product::priceWithCurrency($itemProductHistory->price_one_count); ?></td>
            <td><?= Product::priceWithCurrency($itemProductHistory->full_sum); ?></td>
            <td><?= Product::getBonusOrDiscount($itemProductHistory);?></td>
            <td><?= Product::priceWithCurrency($itemProductHistory->sum); ?></td>
        </tr>
    <?php } ?>
    <?php endforeach; ?>

</table>
<br>
<table class="table change">
    <tr>
      <th><?= Yii::t('shop', 'Total amoun sum'); ?></th>
      <td><?= Product::priceWithCurrency($check->full_sum); ?></td>
    </tr>
    <tr>
      <th><?= Yii::t('product', 'Discount'); ?></th>
      <td><?= Product::priceWithCurrency($check->total_discount_sum); ?></td>
    </tr>
    <tr>
      <th><?= Yii::t('shop', 'Pay bonuses'); ?></th>
      <td><?= Product::priceWithCurrency($check->payment_bonus); ?></td>
    </tr>
    <tr>
      <th><?= Yii::t('shop', 'Bonuses accrued'); ?></th>
      <td><?= Product::priceWithCurrency($check->total_bonuses_sum); ?></td>
    </tr>
    <tr>
      <th><?= Yii::t('card', 'Number'); ?></th>
      <td><?= ($check->card) ? $check->card->number : ''; ?></td>
    </tr>
    <tr>
      <th><?= Yii::t('shop', 'Total sum'); ?></th>
      <td><?= Product::priceWithCurrency($check->total_sum); ?></td>
    </tr>
</table>
<a  id="print" href=""><?= Yii::t('shop', 'Print check'); ?></a>