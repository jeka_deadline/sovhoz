<?php
use yii\helpers\Url;
use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class'=>'yii\grid\SerialColumn'],
        [
            'label' => 'Код чека',
            'attribute' => 'code',
        ],
        [
            'label' => 'Дата',
            'attribute' => 'date',
        ],
        [
            'label' => 'Магазин',
            'attribute' => 'shop_id',
            'value' => function($model){ return ($model->shop) ? $model->shop->name : ''; },
            'filter' => $listShops,
        ],
        [
            //'attribute' => 'some_title',
            'format'    => 'raw',
            'value'     => function ($model) {
                return '<a href="' . Url::toRoute(['/shop/check/view-check', 'code' => $model->code]) . '">Просмотреть чек</a>';},
        ],
    ],
]); ?>