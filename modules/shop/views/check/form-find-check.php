<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code'); ?>

    <?php if ($model->code) : ?>

        <p><?= Yii::t('check', 'Check with code "{code}" not fount', ['code' => $model->code]); ?></p>

    <?php endif; ?>

    <?= Html::submitButton(Yii::t('check', 'Find check'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>