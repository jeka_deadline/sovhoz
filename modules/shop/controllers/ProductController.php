<?php
namespace app\modules\shop\controllers;

use app\modules\shop\models\ClientProduct;
use app\modules\shop\models\forms\AddMultiplePriceForm;
use app\modules\shop\models\searchModels\ProductSearch;
use Yii;
use app\modules\core\components\BackendController;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\shop\models\forms\SaleProductForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\modules\shop\models\Product;
use yii\helpers\ArrayHelper;
use app\modules\shop\models\Percentage;
use app\modules\shop\models\Category;
use app\modules\user\models\User;
use app\modules\core\models\Helper;
use app\modules\shop\assets\CheckAsset;
use app\modules\shop\models\Shop;
use app\models\shop\ProductPrice;

class ProductController extends BackendController
{

    public $modelName   = 'app\modules\shop\models\Product';
    public $searchModel = 'app\modules\shop\models\searchModels\ProductSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'create',
                            'update',
                            'view',
                            'delete',
                            'search',
                            'get-row-sale-product',
                            'get-init-row-sale-product',
                            'tab-list-products',
                            'change',
                            'create-price',
                            'delete-price',
                            'create-multiple-price',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'create' => [
                'class'     => 'app\modules\core\components\CRUDCreate',
                'title'     => Yii::t('product', 'Create product'),
                'modelName' => $this->modelName,
                'assets' => [CheckAsset::className()],

            ],
            'view' => [
                'class'     => 'app\modules\core\components\CRUDView',
                'title'     => Yii::t('product', 'View product'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'app\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProductSearch();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $response['success']['content'] = $this->renderPartial('index-categories', [
                'rootCategories' => $searchModel->searchCategories(Yii::$app->request->queryParams),
            ]);
            $response['success']['openAllTabs'] = $this->isOpenAllTabsAccordion($searchModel->attributes);

            return $response;
        } else {
            $searchModel->load(Yii::$app->request->queryParams);
            $rootCategories = Category::getRoot();
            $listPercentages = ArrayHelper::map(Percentage::find()->all(), 'id', function($model){ return $model->value . '%';});

            return $this->render('index', [
                'searchModel' => $searchModel,
                'rootCategories' => $rootCategories,
                'listPercentages' => $listPercentages,
            ]);
        }
    }

    public function isOpenAllTabsAccordion($attributes) {
        foreach ($attributes as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function actionTabListProducts()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];

        if (!(($category_id = Yii::$app->request->post('category_id', null)) && ($category = Category::findOne($category_id)))) {
            return $response['error'] = 'Category not found.';
        }

        $page = Yii::$app->request->get('page', 1);

        $searchModel = new ProductSearch();
        $searchModel->numberRowPage = 100;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $searchModel->setFilterCategory($dataProvider, $category);

        if ($page < 2) {
            $response['success']['header'] = $this->renderPartial('tab-list-header-products', [
                'categories' => Category::findAll(['parent_id' => $category->id]),
                'rootCategory' => $category,
            ]);
        }

        $response['success']['content'] = $this->renderPartial('tab-list-products', [
            'dataProvider' => $dataProvider,
            'page' => ($page-1)*$searchModel->numberRowPage,
        ]);

        $response['success']['maxPage'] = $dataProvider->pagination->getPageCount();

        return $response;
    }

    public function actionGetRowSaleProduct($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $session = Yii::$app->session;

        $response = [
            'status' => 'error',
            'html' => NULL,
        ];

        $user_id = Yii::$app->user->identity->getId();
        $user = User::find()
                          ->where(['id' => $user_id])
                          ->one();
        $product = ClientProduct::find()->where(['id' => $id])->one();
        $listPrice = ProductPrice::find()->where(['product_id' => $id, 'forShop'=>$user->now_in_shop])->one();
        if ($listPrice)
        {
           $product->price_bag = $listPrice->price_bag;
           $product->price_kg = $listPrice->price_kg;

        }
        $model    = new SaleProductForm();
        if ($product) {
            if ($session->has('productIds')) {
                $productIds = $session->get('productIds');
                $productIds[] = $id;
                $session->set('productIds', $productIds);
            } else {
                $session->set('productIds', [$id]);
            }
            $response[ 'status' ] = 'ok';
            $response[ 'html' ] = $this->renderPartial('sale-product-row-new', [
                'product' => $product,
                'model' => $model,
            ]);
        }

        return $response;
    }

    public function actionGetInitRowSaleProduct()
    {
        return $this->renderAjax('init-row-sale-product');
    }

    public function actionGetTotalPrice($id, $count)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'total' => 0,
        ];

        $model = Product::findOne($id);

        if ($model) {
            $response[ 'total' ] = $model->price * $count;
        }

        return $response;
    }

    public function getColumns()
    {
        $listPercentages = ArrayHelper::map(Percentage::find()->all(), 'id', function($model){ return $model->value . '%';});
        $listShops = ArrayHelper::map(Shop::find()->all(), 'id', function($model){ return $model->name;});
        return [
            $this->getGridSerialColumn(),
            [
                'attribute' => 'category_id',
                'value'     => function($model){ return ($model->category) ? $model->category->name : NULL; },
                'filter'    => ArrayHelper::map(Category::find()->all(), 'id', 'name'),
            ],
            ['attribute' => 'name'],
            ['attribute' => 'price_bag'],
            ['attribute' => 'price_kg'],
            [
                'attribute' => 'max_bonus_percentage',
                'value'     => function($model){ return ($model->bonus) ? $model->bonus->value . '%' : NULL; },
                'filter'    => $listPercentages,
            ],
            [
                'attribute' => 'max_discount_percentage',
                'value'     => function($model){ return ($model->discount) ? $model->discount->value . '%' : NULL; },
                'filter'    => $listPercentages,
            ],
            $this->getGridActions(),
        ];
    }

    public function actionSearch($q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $session = Yii::$app->session;

        $query = call_user_func([$this->modelName, 'find']);
        if ($q) {
            $query->andWhere(['like', 'name', $q]);
            if ($session->has('productIds')) {
                $query->andWhere(['NOT IN', 'id', $session->get('productIds')]);
            }
            if(!$query) break;
            return $query->all();
        }

        return [];
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $listPercentages = Percentage::getListPercentages();

        $dataProviderPriceLists = new ActiveDataProvider([
            'query' => $model->getPriceLists(),
            'pagination' => false,
            'sort' =>false,
        ]);

        $modelProductPrice = new ProductPrice();
        $formMultiplePrice = new AddMultiplePriceForm();

        return $this->render('update', [
            'model' => $model,
            'listPercentages' => $listPercentages,
            'dataProviderPriceLists' => $dataProviderPriceLists,
            'modelProductPrice' => $modelProductPrice,
            'formMultiplePrice' => $formMultiplePrice,
        ]);
    }

    public function actionChange()
    {
        $id = unserialize(base64_decode(Yii::$app->request->post('pk')));

        if (!($model = ProductPrice::findOne($id))) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $name = Yii::$app->request->post('name', '');
        if ($model->hasAttribute($name) && in_array($name, ['price_bag', 'price_kg', 'max_bonus_percentage', 'max_discount_percentage'])) {
            $model->$name = Yii::$app->request->post('value', 0);
            $model->save();
        }
    }

    public function actionCreatePrice()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];

        $model = new ProductPrice();

        if (!$model->load(Yii::$app->request->post())) {
            return $response['error'] = 'Price is not saved.';
        }

        $modelFind = ProductPrice::findOne([
            'product_id' => $model->product_id,
            'forShop' => $model->forShop
        ]);

        if (!$modelFind && $model->save()) {
            $response['success']['id'] = $model->id;
        } elseif ($modelFind && $model->validate()) {
            $modelFind->price_bag = $model->price_bag;
            $modelFind->price_kg = $model->price_kg;
            $modelFind->save();
            $response['success']['id'] = $modelFind->id;
        } else {
            $response['error'] = 'Price is not saved.';
        }

        $dataProviderPriceLists = new ActiveDataProvider([
            'query' => ProductPrice::find()->where(['product_id' => $model->product_id]),
            'pagination' => false,
            'sort' =>false,
        ]);

        $response['success']['content'] = $this->renderAjax('price/_index', [
            'listPercentages' => Percentage::getListPercentages(),
            'dataProviderPriceLists' => $dataProviderPriceLists,
        ]);

        return $response;
    }

    public function actionDeletePrice($id)
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];

        if (($model = ProductPrice::findOne($id)) === null) {
            $response['error'] = 'Not found.';
        }

        $model->delete();

        $response['success'] = 'Entry deleted.';

        return $response;
    }

    public function actionCreateMultiplePrice()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];

        $form = new AddMultiplePriceForm();

        if (!$form->load(Yii::$app->request->post())) {
            return $response['error'] = 'Price is not saved.';
        }

        if ($form->save()) {
            $response['success']['message'] = 'Data saved successfully.';
        } else {
            $response['error'] = 'Price is not saved.';
            return $response;
        }

        $dataProviderPriceLists = new ActiveDataProvider([
            'query' => ProductPrice::find()->where(['product_id' => $form->product_id]),
            'pagination' => false,
            'sort' =>false,
        ]);

        $response['success']['content'] = $this->renderAjax('price/_index', [
            'listPercentages' => Percentage::getListPercentages(),
            'dataProviderPriceLists' => $dataProviderPriceLists,
        ]);

        return $response;
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'danied-actions' => ['search', 'get-init-row-sale-product', 'get-row-sale-product'],
            ],
            User::ROLE_SELLER => [
                'danied-actions' => ['index', 'create', 'update', 'view', 'delete', 'tab-list-products', 'change', 'create-price', 'delete-price', 'create-multiple-price'],
            ],
            User::ROLE_OPERATOR => [
                'danied-actions' => ['search', 'get-init-row-sale-product', 'get-row-sale-product', 'delete'],
            ],
            User::ROLE_MANAGER => [
                'danied-actions' => ['search', 'get-init-row-sale-product', 'get-row-sale-product', 'delete'],
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}