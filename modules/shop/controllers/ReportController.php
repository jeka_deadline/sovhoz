<?php
namespace app\modules\shop\controllers;

use Yii;
use yii\web\Controller;
use app\modules\shop\models\searchModels\ClientPurchasesHistorySearch;
use yii\helpers\ArrayHelper;
use app\modules\shop\models\Product;
use app\modules\shop\models\Shop;
use app\modules\shop\models\SellerReport;
use app\modules\shop\models\SellerHistory;
use yii\db\Expression;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\core\models\Helper;
use app\modules\shop\models\forms\FindSellerReportForm;
use yii\web\NotFoundHttpException;
use app\modules\shop\models\ClientPurchasesHistory;
use kartik\mpdf\Pdf;
use app\modules\shop\models\SellerStat;
use app\modules\user\models\forms\CreateSellerForm;
use app\models\shop\ProductPrice;
use app\modules\shop\models\forms\CashboxSellerForm;
use yii\widgets\ActiveForm;
use yii\web\Response;

class ReportController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'admin-report', 'find-reports', 'show-report','print-report', 'export', 'put-seller-deposit', 'validate-form-put-deposit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $userId   = Yii::$app->user->identity->getId();
        $userShop = User::find()->where(['id' => $userId])->one();
        $currDate = date('Y-m-d');
        $report   = SellerReport::find()->where(['seller_user_id' => $userId, 'date' => $currDate, 'in_shop' =>$userShop->now_in_shop])->one();

        $dataSellerHistory = SellerHistory::find()->where(['status' => 'good', 'shop_id' =>$userShop->now_in_shop])->andWhere(['=', new Expression('DATE(date)'), $currDate])->all();;
        if (!$report) {
            $report                 = new SellerReport();
            $report->seller_user_id = $userId;
            $report->date           = $currDate;
            $report->in_shop        = $userShop->now_in_shop;
        }

        $report->setData($dataSellerHistory);

        $report->validate();
        $report->save();

        return $this->render('seller-report', [
            'dataSellerHistory' => $dataSellerHistory,
            'report' => $report,
            'model' => new CashboxSellerForm(),
        ]);
    }

    public function actionExport()
    {
        $model = new CreateSellerForm();
        if ($model->load(Yii::$app->request->post())) {
            $product_id_forShop = ArrayHelper::map(ProductPrice::find()
                ->where(['forShop' => $model->shopIds])
                ->with('products')
                ->all(), 'product_id', function ($model) {
                return $model;
            });
            $listProducts = Product::find()->all();
            foreach ($listProducts as &$product) {
                if (isset($product_id_forShop[$product->id])) {
                    $product->price_bag = $product_id_forShop[$product->id]->price_bag;
                    $product->price_kg = $product_id_forShop[$product->id]->price_kg;
                    $product->max_bonus_percentage = $product_id_forShop[$product->id]->max_bonus_percentage;
                    $product->max_discount_percentage = $product_id_forShop[$product->id]->max_discount_percentage;
                }
            }

            return \moonland\phpexcel\Excel::widget([
                'asAttachment' => true,
                'models' => $listProducts,
                'mode' => 'export',
                'columns' => [
                    'name',
                    'description',
                    'price_bag',
                    'price_kg'
                ],
            ]);
        }

        return $this->render('form-shop-export', [
            'model' => $model,
        ]);
    }

    public function actionAdminReport()
    {
        $searchModel = new ClientPurchasesHistorySearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataCountKg = '';
        $dataCountBag = '';

        if ($searchModel->product_id) {
            $dataCount = ClientPurchasesHistory::getDataCount($searchModel);

            $dataCountKg  = $dataCount[ 'kg' ];
            $dataCountBag = $dataCount[ 'bag' ];
        }
        if ($searchModel->category_id) {
            $dataCount = ClientPurchasesHistory::getDataCategory($searchModel);
            $dataCountKg  = $dataCount[ 'kg' ];
            $dataCountBag = $dataCount[ 'bag' ];
        }
        return $this->render('admin-report', [
            'gridColumns' => ClientPurchasesHistory::getGridColumns(),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataCountKg' => $dataCountKg,
            'dataCountBag' => $dataCountBag,
        ]);
    }

    public function actionFindReports()
    {
        $model    = new FindSellerReportForm();
        $reports  = [];
        $result   = '';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
              $reports = $model->findReports();
              if (!$reports) {
                  $result = Yii::t('shop', 'Reports to {date} not found', ['date' => $model->date]);
              }
        }

        return $this->render('find-reports', [
            'model' => $model,
            'reports' => $reports,
            'result' => $result,
        ]);
    }

    public function actionShowReport($id)
    {
        $report = SellerReport::findOne($id);

        if (!$report) {
            throw new NotFoundHttpException(Yii::t('shop', 'Report not found'));
        }

        $dataSellerHistory = SellerHistory::find()
                                                ->where(['status' => 'good'])
                                                ->andWhere(['=', new Expression('DATE(date)'), $report->date])
                                                ->andWhere(['seller_user_id' => $report->seller_user_id])
                                                ->all();

        return $this->render('seller-report', [
            'dataSellerHistory' => $dataSellerHistory,
            'report' => $report,
        ]);
    }

    public function actionPrintReport($id)
    {
        $report = SellerReport::findOne($id);

        if (!$report) {
            throw new NotFoundHttpException(Yii::t('shop', 'Report not found'));
        }

        $content = $this->renderPartial('small-seller-report', [
            'report' => $report,
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            //'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
             // set mPDF properties on the fly
            'options' => ['title' => Yii::t('shop', 'Seller report')],
             // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        return $pdf->render();
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'danied-actions' => ['create', 'put-seller-deposit', 'validate-form-put-deposit'],
            ],
            User::ROLE_SELLER => [
                'danied-actions' => ['admin-report', 'find-reports', 'show-report'],
            ],
            User::ROLE_OPERATOR => [
                'danied-actions' => ['create', 'admin-report', 'put-seller-deposit', 'validate-form-put-deposit'],
            ],
            User::ROLE_MANAGER => [
                'danied-actions' => ['create', 'put-seller-deposit', 'validate-form-put-deposit'],
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

    public function actionPutSellerDeposit($reportId)
    {
        $model = new CashboxSellerForm();

        $report = SellerReport::find()
                                    ->where(['id' => $reportId, 'seller_user_id' => Yii::$app->user->identity->getId()])
                                    ->andWhere(['is', 'cashbox_seller_deposit', NULL])
                                    ->one();

        if (!$report) {
            throw new NotFoundHttpException("Отчета не существует или же ранее Вы уже сдавали сумму за текущий день");
        }

        $model->setCurrentSellerReport($report);

        if ($model->load(Yii::$app->request->post()) && $model->putDeposit()) {
            return $this->redirect('create');
        }

        return $this->redirect('create');
    }

    public function actionValidateFormPutDeposit($reportId)
    {
        $model  = new CashboxSellerForm();
        $report = SellerReport::find()
                                    ->where(['id' => $reportId])
                                    ->andWhere(['seller_user_id' => Yii::$app->user->identity->getId()])
                                    ->andWhere(['is', 'cashbox_seller_deposit', NULL])
                                    ->one();

        if (!$report) {
            return FALSE;
        }

        $model->setCurrentSellerReport($report);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

}