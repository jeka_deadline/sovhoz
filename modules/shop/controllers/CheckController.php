<?php
namespace app\modules\shop\controllers;

use Yii;
use yii\web\Controller;
use app\modules\shop\models\SellerHistory;
use mPDF;
use app\modules\shop\models\forms\FindCheckForm;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\shop\SellerReport as BaseSellerReport;
use app\modules\user\models\Profile;
use app\modules\shop\models\SellerStat;
use app\modules\shop\models\SellerReport;
use yii\db\Expression;
use app\modules\user\models\User;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\searchModels\SellerHistorySearch;
use yii\helpers\ArrayHelper;
use app\modules\shop\models\Shop;

class CheckController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['find-check','view-check', 'all-checks'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionFindCheck()
    {
        $model = new FindCheckForm();
        if (Yii::$app->user->identity->role_id == 2) {
             $userId   = Yii::$app->user->identity->getId();
             $currDate = date('Y-m-d');
             $nowinshop = User::find()
                          ->where(['id' => $userId])
                          ->one();
             $dataSellerHistory = SellerHistory::find()->where(['status' => 'good'])->andWhere(['=', new Expression('DATE(date)'), $currDate])->andWhere(['shop_id' => $nowinshop->now_in_shop])->all();
            return $this->render('seller-reports-checks', [
            'dataSellerHistory' => $dataSellerHistory,
        ]);
        }

        if ($model->load(Yii::$app->request->post()) && $check = $model->findCheck()) {

            Yii::$app->params['needfooter'] = FALSE;
            return $this->render('check', [
                'check' => $check,
            ]);

        }

        return $this->render('form-find-check', [
            'model' => $model,
        ]);
    }

    public function actionViewCheck($code)
    {
      $check = SellerHistory::find()->where(['code'=> $code, 'status' => 'good'])->with('shop')->one();

      if (!$check) {
            throw new NotFoundHttpException(Yii::t('check', 'Check with code "{code}" not fount' ));
      }

      Yii::$app->params[ 'needfooter' ] = FALSE;

      return $this->render('check', [
          'check' => $check,
      ]);
    }

    public function actionAllChecks()
    {
        $searchModel = new SellerHistorySearch();
        $dataProvider = $searchModel->searchWithStatusGood(Yii::$app->request->queryParams);

        return $this->render('all-checks', [
            'dataProvider'  => $dataProvider,
            'searchModel'   => $searchModel,
            'listShops'     => ArrayHelper::map(Shop::find()->all(), 'id', 'name'),
        ]);
    }
}