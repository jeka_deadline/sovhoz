<?php
namespace app\modules\shop\controllers;

use Yii;
use app\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\user\models\User;
use app\modules\core\models\Helper;

class ShopController extends BackendController
{

    public $modelName   = 'app\modules\shop\models\Shop';
    public $searchModel = 'app\modules\shop\models\searchModels\ShopSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        $userRoleId = Yii::$app->user->identity->role_id;

        $showUpdateButton = TRUE;
        $showDeleteButton = TRUE;
        $showAddButton    = TRUE;

        switch ($userRoleId) {
            case User::ROLE_MANAGER:
                $showDeleteButton = FALSE;
                break;
            case User::ROLE_OPERATOR:
                $showDeleteButton = FALSE;
                $showUpdateButton = FALSE;
                $showAddButton    = FALSE;
                break;
        }

        return [
            'index' => [
                'class'         => 'app\modules\core\components\CRUDIndex',
                'title'         => Yii::t('shop', 'List shops'),
                'showAddButton' => $showAddButton,
            ],
            'create' => [
                'class'     => 'app\modules\core\components\CRUDCreate',
                'title'     => Yii::t('shop', 'Create shop'),
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'app\modules\core\components\CRUDUpdate',
                'title'     => Yii::t('shop', 'Update shop'),
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'             => 'app\modules\core\components\CRUDView',
                'title'             => Yii::t('shop', 'View shop'),
                'modelName'         => $this->modelName,
                'showDeleteButton'  => $showDeleteButton,
                'showUpdateButton'  => $showUpdateButton,
            ],
            'delete' => [
                'class' => 'app\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        $userRoleId = Yii::$app->user->identity->role_id;

        switch ($userRoleId) {
            case User::ROLE_OPERATOR:
                $optionsGridActions = ['template' => '{view}'];
                break;
            case User::ROLE_MANAGER:
                $optionsGridActions = ['template' => '{view}{update}'];
                break;
            default:
                $optionsGridActions = ['template' => '{view}{update}{delete}'];
        }

        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'name'],
            ['attribute' => 'address'],
            ['attribute' => 'phone'],
            $this->getGridActions($optionsGridActions),
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
            User::ROLE_OPERATOR => [
                'danied-actions' => ['delete', 'create', 'update'],
            ],
            User::ROLE_MANAGER => [
                'danied-actions' => ['delete'],
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

}