<?php
namespace app\modules\shop\controllers;

use Yii;
use app\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\user\models\User;
use app\modules\core\models\Helper;

class PercentageController extends BackendController
{

    public $modelName   = 'app\modules\shop\models\Percentage';
    public $searchModel = 'app\modules\shop\models\searchModels\PercentageSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\core\components\CRUDIndex',
                'title' => Yii::t('percentage', 'List percentages'),
            ],
            'create' => [
                'class'     => 'app\modules\core\components\CRUDCreate',
                'title'     => Yii::t('percentage', 'Create percentage'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'app\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            [
                'attribute' => 'value',
                'value' => function ($model) { return $model->value . '%';}
            ],
            $this->getGridActions(['template' => '{delete}']),
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

}