<?php
namespace app\modules\shop\controllers;

use Yii;
use yii\web\Controller;
use app\modules\shop\models\SellerHistory;
use app\modules\shop\models\forms\FinishSaleForm;
use app\modules\shop\models\forms\TotalSaleForm;
use app\modules\shop\models\forms\SaleProductForm;
use app\modules\shop\models\Check;
use mPDF;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\core\models\Helper;

class SaleController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['cancel-sale', 'sale', 'total-sale', 'remove-product-from-sale','auto'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionCancelSale()
    {
        $sellerHistory = SellerHistory::find()
                                            ->where(['seller_user_id' => Yii::$app->user->identity->getId()])
                                            ->andWhere(['or', ['status' => 'new'], ['status' => 'total']])
                                            ->one();

        if ($sellerHistory) {
            $sellerHistory->status = 'cancel';
            $sellerHistory->save();
        }

        return $this->redirect(['sale']);
    }

    public function actionSale()
    {
        $products = [];
        $session  = Yii::$app->session;

        $sellerHistory = SellerHistory::find()
                                            ->where(['or', ['status' => 'new'], ['status' => 'total']])
                                            ->with('products', 'products.product')
                                            ->andWhere(['seller_user_id' => Yii::$app->user->identity->getId()])
                                            ->one();

        if ($sellerHistory) {
            if ($sellerHistory->status === 'new') {
                foreach ($sellerHistory->products as $itemHistoryProduct) {
                    if (!isset($products[ $itemHistoryProduct->product_id ])) {
                        $products[ $itemHistoryProduct->product_id ] = [
                            'name'        => $itemHistoryProduct->product->name,
                            'count_kg'    => NULL,
                            'count_bag'   => NULL,
                            'price_kg'    => $itemHistoryProduct->product->price_kg,
                            'price_bag'   => $itemHistoryProduct->product->price_bag,
                            'product_id'  => $itemHistoryProduct->product_id,
                            'check_one'  => $itemHistoryProduct->product->check_one,
                        ];
                    }

                    if ($itemHistoryProduct->type_product === 'kg') {
                        $products[ $itemHistoryProduct->product_id ][ 'count_kg' ] = $itemHistoryProduct->count;
                    }
                    if ($itemHistoryProduct->type_product === 'bag') {
                        $products[ $itemHistoryProduct->product_id ][ 'count_bag' ] = $itemHistoryProduct->count;
                    }
                }
            } else if ($sellerHistory->status === 'total') {
                if ($session->has('productIds')) {
                    $session->remove('productIds');
                }
                // была 404 ошибка
                //return $this->redirect(['finish-sale']);
            }
        } else {
            if ($session->has('productIds')) {
                $session->remove('productIds');
            }
        }

        $model = new SaleProductForm();

        if ($model->load(Yii::$app->request->post()) && $model->createSaleProduct()) {
            return $this->redirect(['total-sale']);
        }

        return $this->render('sale', [
            'products' => $products,
            'model' => $model,
        ]);
    }

    public function actionTotalSale()
    {
        $sellerHistory = SellerHistory::find()
                                            ->where(['status' => 'new'])
                                            ->andWhere(['seller_user_id' => Yii::$app->user->identity->getId()])
                                            ->one();

        if ($sellerHistory) {
            $totalSaleForm = new TotalSaleForm();
            if (Yii::$app->request->isPost) {
                 $totalSaleForm->load(Yii::$app->request->post());
                 $check =  $totalSaleForm->totalSale($sellerHistory);
                    if($sellerHistory->total_sum == 0)
                    {
                       $sellerHistory->status = 'cancel';
                       $sellerHistory->save();
                       return $this->redirect(['sale']);

                    } else {


                        $sellerHistory->saveLinkClientShop();

                        Yii::$app->params['needfooter'] = FALSE;
                        return $this->render('success-finish-sale', [
                         'check' => $check,
                        ]);

                    }

                }
                 return $this->render('total-sale', [
                'sellerHistory' => $sellerHistory,
                'totalSaleForm' => $totalSaleForm,
                    ]);
            }




        return $this->redirect(['sale']);
    }

    public function actionRemoveProductFromSale($id)
    {
        $session = Yii::$app->session;
        if ($session->has('productIds')) {
            $productIds = $session->get('productIds');
            $key = array_search($id, $productIds);

            if ($key > -1) {
                unset($productIds[ $key ]);
            }

            if ($productIds) {
                $session->set('productIds', $productIds);
            } else {
                $session->remove('productIds');
            }
        }

        return NULL;
    }
    public function actionAuto($id, $p, $pricekg)
    {
        $data = [
                "pricekg" => 0,
                "id" => $id
                ];
        if ($pricekg == NULL || $pricekg == 0) return json_encode($data);
            $data["pricekg"] = intval(($p / $pricekg) * 1000) / 1000;



        return json_encode($data);

    }


    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_SELLER => [
                'access-actions' => '*',
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }
}