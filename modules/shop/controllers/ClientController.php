<?php
namespace app\modules\shop\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\core\models\Helper;
use app\modules\shop\models\LinkClientShop;
use yii\web\NotFoundHttpException;
use app\modules\shop\models\searchModels\LinkClientShopSearch;
use app\modules\shop\models\Shop;
use app\modules\user\models\User;

class ClientController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['clients'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionClients()
    {
        $searchModel = new LinkClientShopSearch();

        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);

        return $this->render('clients', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'gridColumns'   => $this->getGridColumns(),
        ]);
    }

    private function getGridColumns()
    {
        return [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'shop_id',
                'filter'    => Shop::getListShops(),
                'label'     => 'Магазин',
                'value'     => function($model){ return ($model->shop) ? $model->shop->name : NULL; },
            ],
            [
                'attribute' => 'surname',
                'label'     => 'Фамилия',
                'filter'    => FALSE,
                'value'     => function($model){ return ($model->client && $model->client->profile) ? $model->client->profile->surname : NULL; }
            ],
            [
                'attribute' => 'name',
                'label'     => 'Имя',
                'filter'    => FALSE,
                'value'     => function($model){ return ($model->client && $model->client->profile) ? $model->client->profile->name : NULL; }
            ],
            [
                'attribute' => 'patronymic',
                'label'     => 'Отчество',
                'filter'    => FALSE,
                'value'     => function($model){ return ($model->client && $model->client->profile) ? $model->client->profile->patronymic : NULL; }
            ],
            [
                'attribute' => 'phone',
                'label'     => 'Телефон',
                'filter'    => FALSE,
                'value'     => function($model){ return ($model->client && $model->client->profile) ? $model->client->profile->phone : NULL; }
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
            User::ROLE_MANAGER => [
                'access-actions' => '*',
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

}