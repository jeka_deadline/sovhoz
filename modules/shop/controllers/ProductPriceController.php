<?php

namespace app\modules\shop\controllers;

use app\modules\shop\models\Percentage;
use Yii;
use app\models\shop\ProductPrice;
use app\modules\shop\models\searchModels\ProductPriceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user\models\forms\CreateSellerForm;
use app\modules\shop\models\Product;
use app\modules\shop\models\Shop;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\core\models\Helper;
/**
 * ProductPriceController implements the CRUD actions for ProductPrice model.
 */
class ProductPriceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'copy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductPrice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductPriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new ProductPrice();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'listPercentages' => Percentage::getListPercentages(),
        ]);

    }

    /**
     * Displays a single ProductPrice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductPrice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductPrice();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCopy()
    {
             $shops = Shop::find()->all();
                $products = Product::find()->all();
                $rows = [];
                foreach ($shops as $shop) {
                $shopProductsLinks = ArrayHelper::map(ProductPrice::find()
                ->where(['forShop' => $shop->id])->all(), 'product_id', 'product_id');

                    foreach ($products as $product) {
                        if (isset($shopProductsLinks[$product->id])) {
                            continue;
                        }

                     $rows[] = [
                        'product_id'  => $product->id,
                        'price_kg'    => $product->price_kg,
                        'price_bag'   => $product->price_bag,
                         'max_bonus_percentage'    => $product->max_bonus_percentage,
                         'max_discount_percentage'   => $product->max_discount_percentage,
                        'forShop'     => $shop->id,
                        ];
                    }
                }
                if ($rows) {
                    Yii::$app->db->createCommand()
                        ->batchInsert(ProductPrice::tableName(),['product_id', 'price_kg', 'price_bag', 'forShop'], $rows)
                        ->execute();
                }

               $searchModel = new ProductPriceSearch();
               $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
               $model = new ProductPrice();
               return $this->render('index', [
                 'searchModel' => $searchModel,
                 'dataProvider' => $dataProvider,
                 'model' => $model,
                ]);

    }
    /**
     * Updates an existing ProductPrice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductPrice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductPrice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductPrice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductPrice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
            User::ROLE_OPERATOR => [
                'access-actions' => '*',
            ],
            User::ROLE_MANAGER => [
                'access-actions' => '*',
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }
}
