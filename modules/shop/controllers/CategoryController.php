<?php
namespace app\modules\shop\controllers;

use Yii;
use app\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\user\models\User;
use app\modules\core\models\Helper;

class CategoryController extends BackendController
{

    public $modelName   = 'app\modules\shop\models\Category';
    public $searchModel = 'app\modules\shop\models\searchModels\CategorySearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\core\components\CRUDIndex',
                'title' => Yii::t('category', 'List categories'),
            ],
            'create' => [
                'class'     => 'app\modules\core\components\CRUDCreate',
                'title'     => Yii::t('category', 'Create category'),
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'app\modules\core\components\CRUDUpdate',
                'title'     => Yii::t('category', 'Update category'),
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'app\modules\core\components\CRUDView',
                'title'     => Yii::t('category', 'View category'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'app\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'parent_id'],
            ['attribute' => 'name'],
            $this->getGridActions(),
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
            User::ROLE_OPERATOR => [
                'danied-actions' => ['delete', 'create'],
            ],
            User::ROLE_MANAGER => [
                'danied-actions' => ['delete', 'create'],
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

}