<?php
namespace app\modules\shop\controllers;

use Yii;
use app\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\shop\models\forms\CheckCardForm;
use app\modules\user\models\User;
use app\modules\shop\models\Card;
use app\modules\core\models\Helper;
use app\modules\shop\models\Client;
use yii\web\NotFoundHttpException;

class CardController extends BackendController
{

    public $modelName   = 'app\modules\shop\models\Card';
    public $searchModel = 'app\modules\shop\models\searchModels\CardSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view','check-card'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\core\components\CRUDIndex',
                'title' => Yii::t('card', 'List cards'),
            ],
            'create' => [
                'class'     => 'app\modules\core\components\CRUDCreate',
                'title'     => Yii::t('card', 'Create card'),
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'app\modules\core\components\CRUDUpdate',
                'title'     => Yii::t('card', 'Update card'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'app\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        $userRoleId = Yii::$app->user->identity->role_id;

        $optionsGridActions = [];

        if (User::ROLE_ADMIN == $userRoleId) {
            $optionsGridActions = ['template' => '{view}{update}{delete}'];
        } else {
            $optionsGridActions = ['template' => '{view}'];
        }

        $filterTypeCards = call_user_func([$this->modelName, 'getListTypesCard']);
        return [
            $this->getGridSerialColumn(),
            [
                'attribute' => 'type',
                'value' => function($model){ return $model->getTypeCard(); },
                'filter' => $filterTypeCards,
            ],
            ['attribute' => 'number'],
            ['attribute' => 'value'],
            ['attribute' => 'total'],
            [
                'attribute' => 'bonus_sum',
                'value' => function ($model){ return ($model->type === 'bonus') ? $model->bonus_sum : '-'; }
            ],
            $this->getGridActions($optionsGridActions),
        ];
    }

    public function actionCheckCard()
    {
        $model = new CheckCardForm();
        $userId   = Yii::$app->user->identity->getId();
        $infoUser = null;
        $user = User::find()
                          ->where(['id' => $userId])
                          ->one();
        if ($model->load(Yii::$app->request->post())) {
            if ($user->role_id == 1)
            {
                $card = Card::find()
                          ->where(['number' => $model->numberCard])
                          ->one();
                if (!$card) {
                    throw new NotFoundHttpException(Yii::t('card', "User's card not found"));
                }
                if ($card->value != null)
                {
                    $client = Client::find()
                                ->where(['card_id' => $card->id])
                                ->one();
                    $infoUser = User::find()
                                ->where(['id' => $client->user_id])
                                ->with('profile')
                                ->one();
                }
            }
            return $this->render('info-card', [
                'card' => $model->findCard(),
                'model' => $infoUser,
            ]);
        }

        return $this->render('form-check-card', [
            'model' => $model,
        ]);
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
            User::ROLE_SELLER => [
                'access-actions' => ['check-card'],
            ],
            User::ROLE_OPERATOR => [
                'access-actions' => ['check-card', 'index', 'view', 'create'],
            ],
            User::ROLE_MANAGER => [
                'access-actions' => ['check-card', 'index', 'view', 'create'],
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

    public function actionView($id)
    {
        $card_user = Card::find()
                          ->where(['id' => $id])
                          ->one();
        $client = null;
        $user = null;
        if ($card_user->value != null)
        {
            $client = Client::find()
                          ->where(['card_id' => $card_user->id])
                          ->one();
            $user = User::find()
                          ->where(['id' => $client->user_id])
                          ->with('profile')
                          ->one();
        }

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('card', "User's card not found"));
        }

        if (!$client) {
            throw new NotFoundHttpException(Yii::t('card', "User's card not found"));
        }

        $userRoleId = Yii::$app->user->identity->role_id;

        if ($userRoleId == User::ROLE_OPERATOR) {
            $user = NULL;
        }

        return $this->render('view', [
            'model' => $user,
            'model_card' => $card_user,
        ]);
    }
}