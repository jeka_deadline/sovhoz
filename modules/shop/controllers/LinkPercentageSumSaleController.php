<?php
namespace app\modules\shop\controllers;

use Yii;
use app\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\user\models\User;
use app\modules\core\models\Helper;

class LinkPercentageSumSaleController extends BackendController
{

    public $modelName   = 'app\modules\shop\models\LinkPercentageSumSale';
    public $searchModel = 'app\modules\shop\models\searchModels\LinkPercentageSumSaleSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index-link-bonus', 'create-link-bonus', 'index-link-discount', 'create-link-discount', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndexLinkDiscount()
    {
        $searchModel        = new $this->searchModel();
        $searchModel->type  = 'discount';

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-link-percentage-sum-sale', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'url' => ['create-link-discount'],
        ]);
    }

    public function actionIndexLinkBonus()
    {
        $searchModel        = new $this->searchModel();
        $searchModel->type  = 'bonus';

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-link-percentage-sum-sale', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'url' => ['create-link-bonus'],
        ]);
    }


    public function actionCreateLinkBonus()
    {
        $model        = new $this->modelName();
        $model->type  = 'bonus';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->type = 'bonus';
            $model->save();

            return $this->redirect(['index-link-bonus']);
        }

        return $this->render('create-link-percentage-sum-sale', [
            'model' => $model,
        ]);
    }

    public function actionCreateLinkDiscount()
    {
        $model        = new $this->modelName();
        $model->type  = 'discount';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->type = 'discount';
            $model->save();

            return $this->redirect(['index-link-discount']);
        }

        return $this->render('create-link-percentage-sum-sale', [
            'model' => $model,
        ]);
    }

    public function actions()
    {
        return [
            'delete' => [
                'class' => 'app\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $dataRules = [
            User::ROLE_ADMIN => [
                'access-actions' => '*',
            ],
        ];

        $userRoleId = Yii::$app->user->identity->role_id;

        return Helper::checkRule($dataRules, $userRoleId, $action);
    }

}