<?php

namespace app\modules\shop\assets;

use yii\web\AssetBundle;

class ProductUpdateAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/shop/assets';

    public $js = [
        'js/product/update.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG?true:false,
    ];

    public function init()
    {
        parent::init();
        $this->sourcePath = __DIR__ . DIRECTORY_SEPARATOR;
    }
}
