$(function() {

    $('#form-add-price, #form-add-multiple-price').on('submit', function(e) {
        e.preventDefault();
    });

    $('#form-add-price, #form-add-multiple-price').on('beforeSubmit', function(e) {
        var selectorContent = $(this).parent();
        var serializeForm = $(this).serialize();

        preloaderModalStart(selectorContent);

        $.ajax({
            url: $(this).attr('action'),
            method: 'post',
            dataType: 'json',
            data: serializeForm
        }).done(function(data) {
            if (data.success) {
                $('#modalCreatePrice').modal('hide');
                $('#modalCreateMultiplePrice').modal('hide');

                $('.grid-view-price-lists').html(data.success.content);

                preloaderStop();
            }
        });
    });

    $(document).on('click', '.grid-view-price-lists .button-delete-price', function (e) {
        if (confirm('Вы действительно хотите удалить эту запись?')) {
            $.ajax({
                url: $(this).attr('href'),
                method: 'post',
                dataType: 'json',
            }).done(function(data) {
                if (data.success) {

                }
            });
            $(this).closest('tr').remove();
        }
        e.preventDefault();
    });
});