$(function() {
    var updateListCategories = function() {
        var serializeFormSearch = $('#form-search-product').serialize();
        preloaderModalStart();

        $.ajax({
            url: 'index?'+serializeFormSearch,
            method: 'post',
            dataType: 'json',
        }).done(function(data) {
            if (data.success) {
                preloaderStop();
                $('.accordion-categories').html(data.success.content);
                if (data.success.openAllTabs) {
                    $('.accordion-categories .panel-title a').trigger('click');
                }
            }
        });
    }

    $(document).on('click', '.accordion-categories .panel-title a', function (e) {
        if ($(this).attr('aria-expanded') == 'false') return;

        var category_id = $(this).attr('data-category-id');
        var selectorContent = '#collapse_'+category_id+' .panel-body:first';

        var serializeFormSearch = $('#form-search-product').serialize();

        preloaderModalStart(selectorContent);

        $.ajax({
            url: 'tab-list-products?'+serializeFormSearch,
            method: 'post',
            dataType: 'json',
            data: {
                category_id: category_id,
            }
        }).done(function(data) {
            if (data.success) {
                $(selectorContent).html(data.success.header);
                $(selectorContent + ' table tbody:last').html(data.success.content);

                var buttonMore = $(selectorContent + ' button.more-product:last');
                buttonMore.attr('data-max-page', data.success.maxPage);
                buttonMore.attr('data-current-page', 1);
                if (data.success.maxPage > 1) {
                    buttonMore.removeClass('hidden');
                } else {
                    buttonMore.addClass('hidden');
                }

                preloaderStop(selectorContent);
            }
        });
    });

    $(document).on('click', '.accordion-categories .panel-body button.more-product', function (e) {

        var category_id = $(this).attr('data-category-id');
        var nextPage = parseInt($(this).attr('data-current-page'))+1;
        var maxPage = parseInt($(this).attr('data-max-page'));
        var selectorContent = '#collapse_'+category_id+' .panel-body:first';
        var serializeFormSearch = $('#form-search-product').serialize();

        $(this).attr('data-current-page', nextPage);
        if (maxPage == nextPage) {
            $(this).addClass('hidden');
        }
        preloaderModalStart(selectorContent);

        $.ajax({
            url: 'tab-list-products?page='+nextPage+'&'+serializeFormSearch,
            method: 'post',
            dataType: 'json',
            data: {
                category_id: category_id,
            }
        }).done(function(data) {
            if (data.success) {
                $(selectorContent+' table tbody:last').append(data.success.content);
                preloaderStop(selectorContent);
            }
        });
    });

    $('#form-search-product').on('keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            updateListCategories();
            e.preventDefault();
            return false;
        }
    });

    $('#form-search-product select').on('change', function(e) {
        updateListCategories();
        e.preventDefault();
        return false;
    });

    $('#form-search-product .filter-sort a').click(function (e) {
        $('#form-search-product .filter-sort a').removeClass('asc desc');
        var sort = $(this).attr('data-sort');
        var oldSort = $('#form-search-product input[name="sort"]').val();
        if (sort == oldSort) {
            $('#form-search-product input[name="sort"]').val('-'+sort);
            $(this).addClass('desc');
            $(this).removeClass('asc');
        } else {
            $('#form-search-product input[name="sort"]').val(sort);
            $(this).removeClass('desc');
            $(this).addClass('asc');
        }

        updateListCategories();
        e.preventDefault();
    });
});