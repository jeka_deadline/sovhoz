$(function() {

    $('#sale-product-wrapper').on('click', 'span.glyphicon-plus', function( e ) {
        $.ajax({
            url: $(this).data('url'),
            dataType: 'html',
        }).done(function(data) {
            $('#form-sale-product tbody').append(data);
        });
    });

    $(document).on('click', '.js-link-remove-product', function( e ) {
        e.preventDefault();
        var self = $(this);
        $.ajax({
            url: $(this).data('url'),
        }).done(function(data) {
            $(self).closest('tr').remove();
        });
    });

    $(document).on('click', '.js-link-auto-product', function( e ) {
        var price = prompt("Введите сумму", "1");
        if (isNaN(price) || price == 0 || price < 0) {
        	alert('не число');
       		e.preventDefault();
        } else {
        var self = $(this);
        $.ajax({
            url: $(this).data('url'),
            data: {  
                p: price 
            },
        }).done(function(data) {
            var data = JSON.parse(data);
            var paramSearch = 'input#saleproductform-countkg-' + data['id'];
             if (data["pricekg"] != 0)
                $('.table').find(paramSearch).val(data["pricekg"]);
             
        });

    }
    }); 


});

     $(document).keypress(function(e) {

     if(e.keyCode === 13 && $('div').is("#sale-product-wrapper")) {
        $.ajax({
            url:'/shop/product/get-init-row-sale-product',
            dataType: 'html',
        success: function(html){
        $('#sale-product-wrapper').find('tbody').append(html);
        }});
        e.preventDefault();
       }
});

$('.container').find('#print').on('click', function (event) {
    window.print();
});
