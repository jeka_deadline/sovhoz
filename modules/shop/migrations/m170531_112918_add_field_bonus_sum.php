<?php

use yii\db\Migration;

class m170531_112918_add_field_bonus_sum extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_cards}}', 'bonus_sum', $this->float()->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170531_112918_add_field_bonus_sum cannot be reverted.\n";

        return false;
    }

}
