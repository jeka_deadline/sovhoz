<?php

use yii\db\Migration;

class m170606_115830_add_column_pay_method extends Migration
{
    public function up()
    {
        $this->addColumn('{{%shop_seller_history}}', 'pay_method', $this->string(10)->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170606_115830_add_column_pay_method cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
