<?php

use yii\db\Migration;
use yii\db\Expression;

class m170519_080239_shop extends Migration
{
    public function safeUp()
    {
        // таблица магазинов
        $this->createTable('{{%shop_shops}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(NULL),
            'address'     => $this->text()->defaultValue(NULL),
            'phone'       => $this->string(20)->defaultValue(NULL),
            'created_at'  => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);

        $this->createTable('{{%shop_links_seller_shop}}', [
            'id'              => $this->primaryKey(),
            'shop_id'         => $this->integer(11)->notNull(),
            'user_id'         => $this->integer(11)->notNull(),
        ]);

        $this->createTable('{{%shop_seller_stat}}', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer(11)->notNull(),
            'start_date'  => $this->timestamp()->defaultValue(NULL),
            'end_date'    => $this->timestamp()->defaultValue(NULL),
            'work_day'    => $this->string(20)->defaultValue(NULL),
        ]);

        $this->createTable('{{%shop_cards}}', [
            'id'      => $this->primaryKey(),
            'number'  => $this->string(100)->notNull(),
            'value'   => $this->float()->defaultValue(0),
            'active'  => $this->smallInteger(1)->defaultValue(1),
        ]);

        $this->createTable('{{%shop_client}}', [
            'id'        => $this->primaryKey(),
            'user_id'   => $this->integer(11)->notNull(),
            'card_id'   => $this->integer(11)->defaultValue(NULL),
            'total_sum_buy' => $this->float()->defaultValue(0),
        ]);

        $this->createTable('{{%shop_client_purchases_history}}', [
            'id'          => $this->primaryKey(),
            'client_id'   => $this->integer(11)->defaultValue(NULL),
            'product_id'  => $this->integer(11)->notNull(),
            'count'       => $this->float()->notNull(),
            'date'        => $this->timestamp()->notNull(),
            'sum'         => $this->float()->notNull(),
            'bonus'       => $this->float()->defaultValue(0),
            'discount'    => $this->float()->defaultValue(0),
        ]);

        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%shop_client_purchases_history}}');
        $this->dropTable('{{%shop_client}}');
        $this->dropTable('{{%shop_cards}}');
        $this->dropTable('{{%shop_links_seller_shop}}');
        $this->dropTable('{{%shop_seller_stat}}');
        $this->dropTable('{{%shop_shops}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_shop_seller_shop_id', '{{%shop_links_seller_shop}}', 'shop_id');
        $this->addForeignKey('fk_shop_seller_shop_id', '{{%shop_links_seller_shop}}', 'shop_id', '{{%shop_shops}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_shop_seller_user_id', '{{%shop_links_seller_shop}}', 'user_id');
        $this->addForeignKey('fk_shop_seller_user_id', '{{%shop_links_seller_shop}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_shop_client_user_id', '{{%shop_client}}', 'user_id');
        $this->addForeignKey('fk_shop_client_user_id', '{{%shop_client}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_shop_client_card_id', '{{%shop_client}}', 'card_id');
        $this->addForeignKey('fk_shop_client_card_id', '{{%shop_client}}', 'card_id', '{{%shop_cards}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_shop_client_purchases_history_client_id', '{{%shop_client_purchases_history}}', 'client_id');
        $this->addForeignKey('fk_shop_client_purchases_history_client_id', '{{%shop_client_purchases_history}}', 'client_id', '{{%shop_client}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_shop_client_purchases_history_product_id', '{{%shop_client_purchases_history}}', 'product_id');
        $this->addForeignKey('fk_shop_client_purchases_history_product_id', '{{%shop_client_purchases_history}}', 'product_id', '{{%product_products}}', 'id', 'CASCADE', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_shop_seller_shop_id', '{{%shop_links_seller_shop}}');
        $this->dropIndex('ix_shop_seller_shop_id', '{{%shop_links_seller_shop}}');

        $this->dropForeignKey('fk_shop_seller_user_id', '{{%shop_links_seller_shop}}');
        $this->dropIndex('ix_shop_seller_user_id', '{{%shop_links_seller_shop}}');

        $this->dropForeignKey('fk_shop_client_user_id', '{{%shop_client}}');
        $this->dropIndex('ix_shop_client_user_id', '{{%shop_client}}');

        $this->dropForeignKey('fk_shop_client_card_id', '{{%shop_client}}');
        $this->dropIndex('ix_shop_client_card_id', '{{%shop_client}}');

        $this->dropForeignKey('fk_shop_client_purchases_history_client_id', '{{%shop_client_purchases_history}}');
        $this->dropIndex('ix_shop_client_purchases_history_client_id', '{{%shop_client_purchases_history}}');

        $this->dropForeignKey('fk_shop_client_purchases_history_product_id', '{{%shop_client_purchases_history}}');
        $this->dropIndex('ix_shop_client_purchases_history_product_id', '{{%shop_client_purchases_history}}');
    }
}
