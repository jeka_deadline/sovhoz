<?php

use yii\db\Migration;

class m170607_063429_add_column_payment_bonus extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_client_purchases_history}}', 'payment_bonus', $this->float()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170607_063429_add_column_payment_bonus cannot be reverted.\n";

        return false;
    }
}
