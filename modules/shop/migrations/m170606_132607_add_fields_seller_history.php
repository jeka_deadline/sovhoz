<?php

use yii\db\Migration;

class m170606_132607_add_fields_seller_history extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_seller_history}}', 'total_bonuses_sum', $this->float()->defaultValue(NULL));
        $this->addColumn('{{%shop_seller_history}}', 'total_discount_sum', $this->float()->defaultValue(NULL));
        $this->addColumn('{{%shop_seller_history}}', 'full_sum', $this->float()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170606_132607_add_fields_seller_history cannot be reverted.\n";

        return false;
    }
}
