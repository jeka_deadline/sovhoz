<?php

use yii\db\Migration;

class m170606_134553_seller_report extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%shop_seller_reports}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'seller_history_id' => $this->integer(11)->notNull(),
            'total_cash_sum' => $this->float()->defaultValue(0),
            'total_non_cash_sum' => $this->float()->defaultValue(0),
            'total_discount_sum' => $this->float()->defaultValue(0),
            'total_bonuses_sum' => $this->float()->defaultValue(0),
            'total_pay_bonuses_sum' => $this->float()->defaultValue(0),
        ]);

        $this->createRelations();
    }

    public function createRelations()
    {
        $this->createIndex('ix_shop_seller_reports_seller_history_id', '{{%shop_seller_reports}}', 'seller_history_id');
        $this->addForeignKey('fk_shop_seller_reports_seller_history_id', '{{%shop_seller_reports}}', 'seller_history_id', '{{%shop_seller_history}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function removeRelations()
    {
        $this->dropForeignKey('fk_shop_seller_reports_seller_history_id', '{{%shop_seller_reports}}');
        $this->dropIndex('ix_shop_seller_reports_seller_history_id', '{{%shop_seller_reports}}');
    }

    public function safeDown()
    {
        $this->removeRelations();
        $this->dropTable('{{%shop_seller_reports}}');
    }
}