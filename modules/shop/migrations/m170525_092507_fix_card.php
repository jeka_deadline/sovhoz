<?php

use yii\db\Migration;

class m170525_092507_fix_card extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%shop_cards}}', 'active');
        $this->addColumn('{{%shop_cards}}', 'total', $this->float()->defaultValue(0));
        $this->addColumn('{{%shop_cards}}', 'type', $this->string(10)->defaultValue(NULL));
    }

    public function safeDown()
    {
        $this->addColumn('{{%shop_cards}}', 'active', $this->smallInteger(1)->defaultValue(0));
        $this->dropColumn('{{%shop_cards}}', 'total');
        $this->dropColumn('{{%shop_cards}}', 'type');
    }
}
