<?php

use yii\db\Migration;

/**
 * Class m190416_142931_add_columns_max_min_discount_product_price_table
 */
class m190416_142931_add_columns_max_min_discount_product_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_products}}', 'vendor_code', $this->string(100)->defaultValue(NULL));
        $this->addColumn('{{%product_products}}', 'price_bag_mass', $this->float()->defaultValue(NULL));
        $this->addColumn('{{%product_products}}', 'price_bag_unit', $this->string(20)->defaultValue(NULL));

        $this->addColumn('{{%product_price}}', 'max_bonus_percentage', $this->integer(11)->defaultValue(NULL));
        $this->addColumn('{{%product_price}}', 'max_discount_percentage', $this->integer(11)->defaultValue(NULL));

        $this->createTable('{{%product_unit}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(20)->notNull()->unique(),
        ]);

        $this->batchInsert(
            '{{%product_unit}}',
            [
                'name',
            ],
            [
                [
                    'name' => 'кг',
                ],
                [
                    'name' => 'л',
                ],
                [
                    'name' => 'г',
                ],
                [
                    'name' => 'м',
                ],
                [
                    'name' => 'м2',
                ],
                [
                    'name' => 'м3',
                ],
                [
                    'name' => 'шт',
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_products}}', 'vendor_code');
        $this->dropColumn('{{%product_products}}', 'price_bag_mass');
        $this->dropColumn('{{%product_products}}', 'price_bag_unit');

        $this->dropColumn('{{%product_price}}', 'max_bonus_percentage');
        $this->dropColumn('{{%product_price}}', 'max_discount_percentage');

        $this->dropTable('{{%product_unit}}');
    }
}
