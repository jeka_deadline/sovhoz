<?php

use yii\db\Migration;

class m170607_123215_add_columns_seller_report extends Migration
{
    public function up()
    {
        $this->addColumn('{{%shop_seller_reports}}', 'seller_user_id', $this->integer(11)->notNull());
    }

    public function down()
    {
        echo "m170607_123215_add_columns_seller_report cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
