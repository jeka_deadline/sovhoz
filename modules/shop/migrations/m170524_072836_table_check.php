<?php

use yii\db\Migration;

class m170524_072836_table_check extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%shop_checks}}', [
            'id'                => $this->primaryKey(),
            'code'              => $this->integer(11)->defaultValue(NULL),
            'date'              => $this->timestamp()->notNull(),
            'seller_history_id' => $this->integer(11)->defaultValue(NULL),
        ]);

        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%shop_checks}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_shop_checks_seller_history_id', '{{%shop_checks}}', 'seller_history_id');
        $this->addForeignKey('fk_shop_checks_seller_history_id', '{{%shop_checks}}', 'seller_history_id', '{{%shop_seller_history}}', 'id', 'SET NULL', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_shop_checks_seller_history_id', '{{%shop_checks}}');
        $this->dropIndex('ix_shop_checks_seller_history_id', '{{%shop_checks}}');
    }
}
