<?php

use yii\db\Migration;

class m170523_085321_history_seller extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%shop_seller_history}}', [
            'id'      => $this->primaryKey(),
            'user_id' => $this->integer(11)->defaultValue(NULL),
            'hash'    => $this->string(32)->notNull(),
            'data'    => $this->text()->notNull(),
            'status'  => $this->string(20)->notNull(),
        ]);

        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%shop_seller_history}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_shop_seller_history_user_id', '{{%shop_seller_history}}', 'user_id');
        $this->addForeignKey('fk_shop_seller_history_user_id', '{{%shop_seller_history}}', 'user_id', '{{%user_users}}', 'id', 'SET NULL', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_shop_seller_history_user_id', '{{%shop_seller_history}}');
        $this->dropIndex('ix_shop_seller_history_user_id', '{{%shop_seller_history}}');
    }
}
