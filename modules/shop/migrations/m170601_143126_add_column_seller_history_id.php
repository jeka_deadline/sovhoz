<?php

use yii\db\Migration;

class m170601_143126_add_column_seller_history_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_client_purchases_history}}', 'seller_history_id', $this->integer(11)->notNull() . ' after id');
    }

    public function safeDown()
    {
        echo "m170601_143126_add_column_seller_history_id cannot be reverted.\n";

        return false;
    }
}
