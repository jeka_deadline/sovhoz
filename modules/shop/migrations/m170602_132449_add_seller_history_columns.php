<?php

use yii\db\Migration;

class m170602_132449_add_seller_history_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_seller_history}}', 'client_user_id', $this->integer(11)->defaultValue(NULL));
        $this->addColumn('{{%shop_seller_history}}', 'card_id', $this->integer(11)->defaultValue(NULL));
        $this->addColumn('{{%shop_seller_history}}', 'card_type', $this->string(10)->defaultValue(NULL));
        $this->dropColumn('{{%shop_seller_history}}', 'hash');
        $this->dropColumn('{{%shop_seller_history}}', 'data');
        $this->renameColumn('{{%shop_seller_history}}', 'user_id', 'seller_user_id');
        $this->addColumn('{{%shop_seller_history}}', 'date', $this->timestamp()->defaultValue(NULL));
        $this->addColumn('{{%shop_seller_history}}', 'code', $this->integer()->defaultValue(NULL));
        $this->addColumn('{{%shop_seller_history}}', 'total_sum', $this->float()->defaultValue(0));
        $this->addColumn('{{%shop_seller_history}}', 'payment_bonus', $this->float()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170602_132449_add_seller_history_columns cannot be reverted.\n";

        return false;
    }

}
