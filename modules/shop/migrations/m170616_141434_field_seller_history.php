<?php

use yii\db\Migration;

class m170616_141434_field_seller_history extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_seller_history}}', 'total_sale_kg', $this->float()->defaultValue(0));
        $this->addColumn('{{%shop_seller_reports}}', 'total_sale_kg', $this->float()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170616_141434_field_seller_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170616_141434_field_seller_history cannot be reverted.\n";

        return false;
    }
    */
}
