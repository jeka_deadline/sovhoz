<?php

use yii\db\Migration;

class m170602_073202_add_bonus_sum_field_to_check extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_checks}}', 'payment_bonus', $this->float()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170602_073202_add_bonus_sum_field_to_check cannot be reverted.\n";

        return false;
    }
}