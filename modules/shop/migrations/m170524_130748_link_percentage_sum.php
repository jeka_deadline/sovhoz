<?php

use yii\db\Migration;

class m170524_130748_link_percentage_sum extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%shop_links_percentage_sum_sale}}', [
            'id'        => $this->primaryKey(),
            'percent'   => $this->float()->notNull(),
            'sum_sale'  => $this->float()->notNull(),
            'type'      => $this->string()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%shop_links_percentage_sum_sale}}');
    }
}
