<?php

use yii\db\Migration;

class m170602_144107_fix_column_parcentage extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%product_products}}', 'max_bonus_percantage', 'max_bonus_percentage');
    }

    public function down()
    {
        echo "m170602_144107_fix_column_parcentage cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
