<?php

use yii\db\Migration;

class m170608_111222_drop_column_extra_charge extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%product_products}}', 'extra_charge');
    }

    public function down()
    {
        echo "m170608_111222_drop_column_extra_charge cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
