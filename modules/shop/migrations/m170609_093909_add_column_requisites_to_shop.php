<?php

use yii\db\Migration;

class m170609_093909_add_column_requisites_to_shop extends Migration
{
    public function up()
    {
        $this->addColumn('{{%shop_shops}}', 'requisites', $this->text()->notNull());
    }

    public function down()
    {
        echo "m170609_093909_add_column_requisites_to_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
