<?php

use yii\db\Migration;
use yii\db\Expression;

class m170519_079643_product extends Migration
{
    public function safeUp()
    {
        // таблица категорий продуктов
        $this->createTable('{{%product_categories}}', [
            'id'          => $this->primaryKey(),
            'parent_id'   => $this->integer(11)->defaultValue(0),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(NULL),
        ]);

        // таблица скидок
        $this->createTable('{{%product_percentages}}', [
            'id'    => $this->primaryKey(),
            'value' => $this->float()->notNull(),
        ]);

        // таблица продуктов
        $this->createTable('{{%product_products}}', [
            'id'                      => $this->primaryKey(),
            'category_id'             => $this->integer(11)->defaultValue(NULL),
            'name'                    => $this->string(255)->notNull(),
            'description'             => $this->text()->defaultValue(NULL),
            'price_bag'               => $this->float()->notNull(),
            'price_kg'                => $this->float()->notNull(),
            'extra_charge'            => $this->float()->defaultValue(NULL),
            'max_bonus_percantage'    => $this->integer(11)->defaultValue(NULL),
            'max_discount_percentage' => $this->integer(11)->defaultValue(NULL),
            'created_at'              => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);

        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%product_products}}');
        $this->dropTable('{{%product_percentages}}');
        $this->dropTable('{{%product_categories}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_products_category_id', '{{%product_products}}', 'category_id');
        $this->addForeignKey('fk_product_products_category_id', '{{%product_products}}', 'category_id', '{{%product_categories}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_product_products_max_bonus_percantage', '{{%product_products}}', 'max_bonus_percantage');
        $this->addForeignKey('fk_product_products_max_bonus_percantage', '{{%product_products}}', 'max_bonus_percantage', '{{%product_percentages}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_product_products_max_discount_percentage', '{{%product_products}}', 'max_discount_percentage');
        $this->addForeignKey('fk_product_products_max_discount_percentage', '{{%product_products}}', 'max_discount_percentage', '{{%product_percentages}}', 'id', 'SET NULL', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_products_category_id', '{{%product_products}}');
        $this->dropIndex('ix_product_products_category_id', '{{%product_products}}');

        $this->dropForeignKey('fk_product_products_max_bonus_percantage', '{{%product_products}}');
        $this->dropIndex('ix_product_products_max_bonus_percantage', '{{%product_products}}');

        $this->dropForeignKey('fk_product_products_max_discount_percentage', '{{%product_products}}');
        $this->dropIndex('ix_product_products_max_discount_percentage', '{{%product_products}}');
    }
}
