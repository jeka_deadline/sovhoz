<?php

use yii\db\Migration;

class m170531_141536_fields_client_history extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_client_purchases_history}}', 'type_product', $this->string(5)->notNull());
        $this->addColumn('{{%shop_client_purchases_history}}', 'full_sum', $this->float()->notNull());
    }

    public function safeDown()
    {
        echo "m170531_141536_fields_client_history cannot be reverted.\n";

        return false;
    }
}
