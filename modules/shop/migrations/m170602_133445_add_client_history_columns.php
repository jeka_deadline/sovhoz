<?php

use yii\db\Migration;

class m170602_133445_add_client_history_columns extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_shop_client_purchases_history_client_id', '{{%shop_client_purchases_history}}');
        $this->dropColumn('{{%shop_client_purchases_history}}', 'client_id');
        $this->dropColumn('{{%shop_client_purchases_history}}', 'date');
        $this->addColumn('{{%shop_client_purchases_history}}', 'bonus_percent_value', $this->integer()->defaultValue(0));
        $this->addColumn('{{%shop_client_purchases_history}}', 'discount_percent_value', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170602_133445_add_client_history_columns cannot be reverted.\n";

        return false;
    }

}
