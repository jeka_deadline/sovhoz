<?php

use yii\db\Migration;

class m170616_144942_field_seller_report extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_seller_reports}}', 'last_enter', $this->datetime()->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170616_144942_field_seller_report cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170616_144942_field_seller_report cannot be reverted.\n";

        return false;
    }
    */
}
