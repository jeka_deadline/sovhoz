<?php

use yii\db\Migration;

class m170608_105446_add_column_one_count_price extends Migration
{
    public function up()
    {
        $this->addColumn('{{%shop_client_purchases_history}}', 'price_one_count', $this->float()->notNull());
    }

    public function down()
    {
        echo "m170608_105446_add_column_one_count_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
