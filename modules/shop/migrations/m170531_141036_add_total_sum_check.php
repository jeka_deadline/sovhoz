<?php

use yii\db\Migration;

class m170531_141036_add_total_sum_check extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop_checks}}', 'total_sum', $this->float()->notNull());
    }

    public function safeDown()
    {
        echo "m170531_141036_add_total_sum_check cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
