<?php

use yii\db\Migration;

class m170609_065626_add_column_shop_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%shop_seller_history}}', 'shop_id', $this->integer(11)->notNull());
        $this->addColumn('{{%shop_seller_stat}}', 'shop_id', $this->integer(11)->notNull());
    }

    public function down()
    {
        echo "m170609_065626_add_column_shop_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
