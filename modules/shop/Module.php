<?php
namespace app\modules\shop;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'app\modules\shop\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'core' ])) {
            Yii::$app->i18n->translations[ 'core' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/core/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'shop' ])) {
            Yii::$app->i18n->translations[ 'shop' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'product' ])) {
            Yii::$app->i18n->translations[ 'product' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'category' ])) {
            Yii::$app->i18n->translations[ 'category' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'percentage' ])) {
            Yii::$app->i18n->translations[ 'percentage' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'card' ])) {
            Yii::$app->i18n->translations[ 'card' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'check' ])) {
            Yii::$app->i18n->translations[ 'check' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/shop/messages',
            ];
        }
        if (!isset(Yii::$app->i18n->translations[ 'user' ])) {
            Yii::$app->i18n->translations[ 'user' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/user/messages',
            ];
        }
    }

}
?>