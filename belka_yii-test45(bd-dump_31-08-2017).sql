-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: mysql7.locum.ru
-- Время создания: Авг 31 2017 г., 11:11
-- Версия сервера: 5.5.53-38.5-log
-- Версия PHP: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `belka_yii-test45`
--

-- --------------------------------------------------------

--
-- Структура таблицы `core_pages`
--

CREATE TABLE `core_pages` (
  `id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `menu_class` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `core_pages`
--

INSERT INTO `core_pages` (`id`, `route`, `header`, `menu_class`, `display_order`, `active`) VALUES
(1, 'core/index/index', 'Главная страница', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `core_pages_meta`
--

CREATE TABLE `core_pages_meta` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `modelClass` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_params`
--

CREATE TABLE `core_params` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` text,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_ready_answers_for_support`
--

CREATE TABLE `core_ready_answers_for_support` (
  `id` int(11) NOT NULL,
  `header` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `display_order` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_support_messages`
--

CREATE TABLE `core_support_messages` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `answer` text,
  `image` varchar(255) DEFAULT NULL,
  `is_new` smallint(1) DEFAULT '1',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_support_subjects`
--

CREATE TABLE `core_support_subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_text_block`
--

CREATE TABLE `core_text_block` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `uri` varchar(255) NOT NULL,
  `content` text,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `dictionary_termins`
--

CREATE TABLE `dictionary_termins` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `display_order` int(1) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1500370938),
('m160331_140545_users', 1500370940),
('m160610_133200_table_static_pages', 1500370952),
('m170126_135301_core', 1500370945),
('m170130_090519_dictiondary', 1500370959),
('m170615_082014_check', 1500370966),
('m170622_092834_tablet_winner_steps', 1500370966),
('m170719_122915_pets', 1500991172),
('m170720_090158_stock_collections', 1500991173),
('m170720_092930_collection_id_check', 1500991173),
('m170724_075613_new_user_fields', 1500991166);

-- --------------------------------------------------------

--
-- Структура таблицы `page_static_pages`
--

CREATE TABLE `page_static_pages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `uri` varchar(100) NOT NULL,
  `full_uri` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `menu_class` varchar(255) DEFAULT NULL,
  `content` text,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `raffle_tablet_winner_date`
--

CREATE TABLE `raffle_tablet_winner_date` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `is_winner` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `stock_check`
--

CREATE TABLE `stock_check` (
  `id` int(11) NOT NULL,
  `hash_id` varchar(32) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_phone` varchar(15) DEFAULT NULL,
  `sms_check_code` varchar(255) DEFAULT NULL,
  `attach_sms_code` varchar(32) DEFAULT NULL,
  `number` varchar(50) NOT NULL,
  `check_date` timestamp NULL DEFAULT NULL,
  `check_image` text,
  `image_hash` varchar(32) DEFAULT NULL,
  `register_date` timestamp NULL DEFAULT NULL,
  `status` smallint(1) DEFAULT '0',
  `reason_rejection` text,
  `prize_id` int(11) DEFAULT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `winner_date` timestamp NULL DEFAULT NULL,
  `send_status_id` int(11) DEFAULT NULL,
  `send_number` varchar(50) DEFAULT NULL,
  `user_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `stock_collections`
--

CREATE TABLE `stock_collections` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `stock_pets`
--

CREATE TABLE `stock_pets` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stock_pets`
--

INSERT INTO `stock_pets` (`id`, `name`, `display_order`, `active`) VALUES
(1, 'кошка', 0, 1),
(2, 'собака', 0, 1),
(3, 'кошка и собака', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `stock_prizes`
--

CREATE TABLE `stock_prizes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `stock_send_statuses`
--

CREATE TABLE `stock_send_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `stock_store`
--

CREATE TABLE `stock_store` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_log_profiles`
--

CREATE TABLE `user_log_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `code_change_phone` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `is_new` smallint(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `pet_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `surname`, `name`, `date_birth`, `phone`, `city`, `region`, `address`, `pet_id`) VALUES
(1, 1, 'Бублик', 'Евгений', '1992-05-14', '380935994767', 'Чернигов', 'Чернигов', NULL, NULL),
(4, 4, 'Ковера', 'Алена', '1988-03-10', NULL, 'Киев', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_socials`
--

CREATE TABLE `user_socials` (
  `id` int(11) NOT NULL,
  `provider` varchar(100) NOT NULL,
  `client_id` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_socials`
--

INSERT INTO `user_socials` (`id`, `provider`, `client_id`, `created_at`, `user_id`) VALUES
(3, 'odnoklassniki', '580824547639', '2017-07-19 08:48:33', 4),
(4, 'facebook', '1545739505756171', '2017-07-19 09:21:58', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `user_users`
--

CREATE TABLE `user_users` (
  `id` int(11) NOT NULL,
  `is_admin` smallint(1) DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `temporary_code` varchar(32) DEFAULT NULL,
  `password_hash` varchar(100) NOT NULL,
  `reset_password_token` varchar(32) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `blocked_at` timestamp NULL DEFAULT NULL,
  `confirm_email_at` timestamp NULL DEFAULT NULL,
  `confirm_phone_at` timestamp NULL DEFAULT NULL,
  `register_ip` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `login_with_social` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_users`
--

INSERT INTO `user_users` (`id`, `is_admin`, `email`, `temporary_code`, `password_hash`, `reset_password_token`, `auth_key`, `blocked_at`, `confirm_email_at`, `confirm_phone_at`, `register_ip`, `created_at`, `updated_at`, `login_with_social`) VALUES
(1, 1, 'jeka.deadline@gmail.com', NULL, '$2y$13$h3F7QuPiRclYUpGWce0SzOfbTrEHcRYbzI8zMGuxv9akYUhWdXqFu', NULL, '716abd6f63dfc96e1d7fb2053e5a2ab5', NULL, '2017-07-18 09:42:20', '2017-07-18 09:42:20', '127.0.0.1', '2017-07-18 09:42:20', '2017-07-18 09:42:20', 0),
(4, 0, 'lenboard@yandex.ru', NULL, '$2y$13$27AqFisxqmg9.nkOqkmc2evmbEmnNRUZ.FTemNSle/ZddzTgOTONi', NULL, '_bDFm4qWDfDw-1LtghfOfBnacDQsLMwv', NULL, '2017-07-19 08:48:33', NULL, '46.149.95.114', '2017-07-19 08:48:33', '2017-07-19 08:48:33', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `core_pages`
--
ALTER TABLE `core_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_core_pages_route` (`route`);

--
-- Индексы таблицы `core_pages_meta`
--
ALTER TABLE `core_pages_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_params`
--
ALTER TABLE `core_params`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_core_params_code` (`code`);

--
-- Индексы таблицы `core_ready_answers_for_support`
--
ALTER TABLE `core_ready_answers_for_support`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_support_messages`
--
ALTER TABLE `core_support_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_core_support_messages_subject_id` (`subject_id`),
  ADD KEY `ix_core_support_messages_user_id` (`user_id`);

--
-- Индексы таблицы `core_support_subjects`
--
ALTER TABLE `core_support_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_text_block`
--
ALTER TABLE `core_text_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_core_text_block_uri` (`uri`);

--
-- Индексы таблицы `dictionary_termins`
--
ALTER TABLE `dictionary_termins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `page_static_pages`
--
ALTER TABLE `page_static_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_pages_static_page_uri` (`uri`),
  ADD UNIQUE KEY `uix_pages_static_page_full_uri` (`full_uri`);

--
-- Индексы таблицы `raffle_tablet_winner_date`
--
ALTER TABLE `raffle_tablet_winner_date`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_raffle_tablet_winner_date_date` (`date`);

--
-- Индексы таблицы `stock_check`
--
ALTER TABLE `stock_check`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_stock_check_store_id` (`store_id`),
  ADD KEY `ix_stock_check_user_id` (`user_id`),
  ADD KEY `ix_stock_check_prize_id` (`prize_id`),
  ADD KEY `ix_stock_check_send_status_id` (`send_status_id`),
  ADD KEY `ix_stock_check_collection_id` (`collection_id`);

--
-- Индексы таблицы `stock_collections`
--
ALTER TABLE `stock_collections`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stock_pets`
--
ALTER TABLE `stock_pets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stock_prizes`
--
ALTER TABLE `stock_prizes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stock_send_statuses`
--
ALTER TABLE `stock_send_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stock_store`
--
ALTER TABLE `stock_store`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_log_profiles`
--
ALTER TABLE `user_log_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_user_log_profiles_user_id` (`user_id`);

--
-- Индексы таблицы `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_user_notifications_user_id` (`user_id`);

--
-- Индексы таблицы `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_user_profiles_user_id` (`user_id`),
  ADD KEY `ix_user_profiles_pet_id` (`pet_id`);

--
-- Индексы таблицы `user_socials`
--
ALTER TABLE `user_socials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_user_socials_user_id` (`user_id`);

--
-- Индексы таблицы `user_users`
--
ALTER TABLE `user_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_user_users_email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `core_pages`
--
ALTER TABLE `core_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `core_pages_meta`
--
ALTER TABLE `core_pages_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_params`
--
ALTER TABLE `core_params`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_ready_answers_for_support`
--
ALTER TABLE `core_ready_answers_for_support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_support_messages`
--
ALTER TABLE `core_support_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_support_subjects`
--
ALTER TABLE `core_support_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_text_block`
--
ALTER TABLE `core_text_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `dictionary_termins`
--
ALTER TABLE `dictionary_termins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `page_static_pages`
--
ALTER TABLE `page_static_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `raffle_tablet_winner_date`
--
ALTER TABLE `raffle_tablet_winner_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `stock_check`
--
ALTER TABLE `stock_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `stock_collections`
--
ALTER TABLE `stock_collections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `stock_pets`
--
ALTER TABLE `stock_pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `stock_prizes`
--
ALTER TABLE `stock_prizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `stock_send_statuses`
--
ALTER TABLE `stock_send_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `stock_store`
--
ALTER TABLE `stock_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_log_profiles`
--
ALTER TABLE `user_log_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user_socials`
--
ALTER TABLE `user_socials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user_users`
--
ALTER TABLE `user_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `core_support_messages`
--
ALTER TABLE `core_support_messages`
  ADD CONSTRAINT `fk_core_support_messages_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `core_support_subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_core_support_messages_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `stock_check`
--
ALTER TABLE `stock_check`
  ADD CONSTRAINT `fk_stock_check_collection_id` FOREIGN KEY (`collection_id`) REFERENCES `stock_collections` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_stock_check_prize_id` FOREIGN KEY (`prize_id`) REFERENCES `stock_prizes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_stock_check_send_status_id` FOREIGN KEY (`send_status_id`) REFERENCES `stock_send_statuses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_stock_check_store_id` FOREIGN KEY (`store_id`) REFERENCES `stock_store` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_stock_check_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_log_profiles`
--
ALTER TABLE `user_log_profiles`
  ADD CONSTRAINT `fk_user_log_profiles_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD CONSTRAINT `fk_user_notifications_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD CONSTRAINT `fk_user_profiles_pet_id` FOREIGN KEY (`pet_id`) REFERENCES `stock_pets` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_profiles_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_socials`
--
ALTER TABLE `user_socials`
  ADD CONSTRAINT `fk_user_socials_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
