<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_profiles}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property string $date_birth
 * @property string $gender
 * @property string $phone
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profiles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'gender'], 'required'],
            [['user_id'], 'integer'],
            [['date_birth'], 'safe'],
            [['surname', 'name', 'patronymic'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 6],
            [['phone'], 'string', 'max' => 15],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'surname' => 'Surname',
            'name' => 'Name',
            'patronymic' => 'Patronymic',
            'date_birth' => 'Date Birth',
            'gender' => 'Gender',
            'phone' => 'Phone'
  
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
