<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_cards}}".
 *
 * @property integer $id
 * @property string $number
 * @property double $value
 * @property double $total
 * @property string $type
 * @property double $bonus_sum
 */
class Card extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_cards}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['value', 'total', 'bonus_sum'], 'number'],
            [['number'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'value' => 'Value',
            'total' => 'Total',
            'type' => 'Type',
            'bonus_sum' => 'Bonus Sum',
        ];
    }
}
