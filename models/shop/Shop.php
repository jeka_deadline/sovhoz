<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_shops}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string $phone
 * @property string $created_at
 * @property string $requisites
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_shops}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'requisites'], 'required'],
            [['description', 'address', 'requisites'], 'string'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'address' => 'Address',
            'phone' => 'Phone',
            'created_at' => 'Created At',
            'requisites' => 'Requisites',
        ];
    }
}
