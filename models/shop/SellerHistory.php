<?php

namespace app\models\shop;

use Yii;
use app\models\user\User;

/**
 * This is the model class for table "{{%shop_seller_history}}".
 *
 * @property integer $id
 * @property integer $seller_user_id
 * @property string $status
 * @property integer $client_user_id
 * @property integer $card_id
 * @property string $card_type
 * @property string $date
 * @property integer $code
 * @property double $total_sum
 * @property double $payment_bonus
 * @property string $pay_method
 * @property double $total_bonuses_sum
 * @property double $total_discount_sum
 * @property double $full_sum
 * @property integer $shop_id
 * @property double $total_sale_kg
 */
class SellerHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_seller_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seller_user_id', 'status', 'shop_id'], 'required'],
            [['seller_user_id', 'client_user_id', 'card_id', 'code', 'shop_id'], 'integer'],
            [['date'], 'safe'],
            [['total_sum', 'payment_bonus', 'total_bonuses_sum', 'total_discount_sum', 'full_sum', 'total_sale_kg'], 'number'],
            [['status'], 'string', 'max' => 20],
            [['card_type', 'pay_method'], 'string', 'max' => 10],
            [['seller_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['seller_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seller_user_id' => 'Seller User ID',
            'status' => 'Status',
            'client_user_id' => 'Client User ID',
            'card_id' => 'Card ID',
            'card_type' => 'Card Type',
            'date' => 'Date',
            'code' => 'Code',
            'total_sum' => 'Total Sum',
            'payment_bonus' => 'Payment Bonus',
            'pay_method' => 'Pay Method',
            'total_bonuses_sum' => 'Total Bonuses Sum',
            'total_discount_sum' => 'Total Discount Sum',
            'full_sum' => 'Full Sum',
            'shop_id' => 'Shop ID',
            'total_sale_kg' => 'Total Sale Kg',
        ];
    }
}
