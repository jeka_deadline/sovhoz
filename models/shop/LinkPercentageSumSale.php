<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_links_percentage_sum_sale}}".
 *
 * @property integer $id
 * @property double $percent
 * @property double $sum_sale
 * @property string $type
 */
class LinkPercentageSumSale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_links_percentage_sum_sale}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['percent'], 'required'],
            [['percent', 'sum_sale'], 'number'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'percent' => 'Percent',
            'sum_sale' => 'Sum Sale',
            'type' => 'Type',
        ];
    }
}
