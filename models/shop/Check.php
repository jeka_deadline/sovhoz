<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_checks}}".
 *
 * @property integer $id
 * @property integer $code
 * @property string $date
 * @property integer $seller_history_id
 * @property double $total_sum
 * @property double $payment_bonus
 */
class Check extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_checks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'seller_history_id'], 'integer'],
            [['date'], 'safe'],
            [['total_sum'], 'required'],
            [['total_sum', 'payment_bonus'], 'number'],
            [['seller_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => SellerHistory::className(), 'targetAttribute' => ['seller_history_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'date' => 'Date',
            'seller_history_id' => 'Seller History ID',
            'total_sum' => 'Total Sum',
            'payment_bonus' => 'Payment Bonus',
        ];
    }
}
