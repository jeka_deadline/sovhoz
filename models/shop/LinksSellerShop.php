<?php

namespace app\models\shop;

use Yii;
use app\models\user\User;

/**
 * This is the model class for table "{{%shop_links_seller_shop}}".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property integer $user_id
 */
class LinksSellerShop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_links_seller_shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'user_id'], 'required'],
            [['shop_id', 'user_id'], 'integer'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_id' => 'Shop ID',
            'user_id' => 'User ID',
        ];
    }
}
