<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_client_purchases_history}}".
 *
 * @property integer $id
 * @property integer $seller_history_id
 * @property integer $product_id
 * @property double $count
 * @property double $sum
 * @property double $bonus
 * @property double $discount
 * @property string $type_product
 * @property double $full_sum
 * @property integer $bonus_percent_value
 * @property integer $discount_percent_value
 * @property double $payment_bonus
 * @property double $price_one_count
 */
class ClientPurchasesHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_client_purchases_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seller_history_id', 'product_id', 'count', 'sum', 'type_product', 'full_sum', 'price_one_count'], 'required'],
            [['seller_history_id', 'product_id', 'bonus_percent_value', 'discount_percent_value'], 'integer'],
            [['count', 'sum', 'bonus', 'discount', 'full_sum', 'payment_bonus', 'price_one_count'], 'number'],
            [['type_product'], 'string', 'max' => 5],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seller_history_id' => 'Seller History ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
            'sum' => 'Sum',
            'bonus' => 'Bonus',
            'discount' => 'Discount',
            'type_product' => 'Type Product',
            'full_sum' => 'Full Sum',
            'bonus_percent_value' => 'Bonus Percent Value',
            'discount_percent_value' => 'Discount Percent Value',
            'payment_bonus' => 'Payment Bonus',
            'price_one_count' => 'Price One Count',
        ];
    }
}
