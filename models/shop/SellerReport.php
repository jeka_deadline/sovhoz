<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_seller_reports}}".
 *
 * @property integer $id
 * @property string $date
 * @property double $total_cash_sum
 * @property double $total_non_cash_sum
 * @property double $total_discount_sum
 * @property double $total_bonuses_sum
 * @property double $total_pay_bonuses_sum
 * @property integer $seller_user_id
 * @property double $total_sale_kg
 * @property string $last_enter
 */
class SellerReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_seller_reports}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'seller_user_id'], 'required'],
            [['date', 'last_enter'], 'safe'],
            [['total_cash_sum', 'total_non_cash_sum', 'total_discount_sum', 'total_bonuses_sum', 'total_pay_bonuses_sum', 'total_sale_kg'], 'number'],
            [['seller_user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'total_cash_sum' => 'Total Cash Sum',
            'total_non_cash_sum' => 'Total Non Cash Sum',
            'total_discount_sum' => 'Total Discount Sum',
            'total_bonuses_sum' => 'Total Bonuses Sum',
            'total_pay_bonuses_sum' => 'Total Pay Bonuses Sum',
            'seller_user_id' => 'Seller User ID',
            'total_sale_kg' => 'Total Sale Kg',
            'last_enter' => 'Last Enter',
        ];
    }
}
