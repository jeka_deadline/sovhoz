<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%shop_seller_stat}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $start_date
 * @property string $end_date
 * @property string $work_day
 * @property integer $shop_id
 */
class SellerStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_seller_stat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'shop_id'], 'required'],
            [['user_id', 'shop_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['work_day'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'work_day' => 'Work Day',
            'shop_id' => 'Shop ID',
        ];
    }
}
