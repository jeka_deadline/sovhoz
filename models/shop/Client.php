<?php

namespace app\models\shop;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "{{%shop_client}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $card_id
 * @property double $total_sum_buy
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'card_id'], 'integer'],
            [['total_sum_buy'], 'number'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'card_id' => 'Card ID',
            'total_sum_buy' => 'Total Sum Buy',
        ];
    }
}
