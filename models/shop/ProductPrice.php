<?php

namespace app\models\shop;
use app\models\shop\Product;
use app\models\shop\Shop;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "{{%product_price}}".
 *
 * @property int $id
 * @property int $product_id
 * @property double $price_bag
 * @property integer $max_bonus_percentage
 * @property integer $max_discount_percentage
 * @property double $price_kg
 * @property int $forShop
 *
 * @property ProductProducts $product
 */
class ProductPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_price}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'forShop', 'max_bonus_percentage', 'max_discount_percentage'], 'integer'],
            [['price_bag'], 'required'],
            [['price_bag', 'price_kg'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['max_bonus_percentage'], 'exist', 'skipOnError' => true, 'targetClass' => Percentage::className(), 'targetAttribute' => ['max_bonus_percentage' => 'id']],
            [['max_discount_percentage'], 'exist', 'skipOnError' => true, 'targetClass' => Percentage::className(), 'targetAttribute' => ['max_discount_percentage' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => Yii::t('product', 'Product Id'),
            'price_bag'  => Yii::t('product', 'Price bag'),
            'price_kg' => Yii::t('product', 'Price kilogramm'),
            'forShop' => Yii::t('product', 'For Shop'),
            'max_bonus_percentage' => Yii::t('product','Max bonus'),
            'max_discount_percentage' => Yii::t('product','Max discount'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductP()
    {
        return ArrayHelper::map(Product::find()->all(), 'id', 'name');
    }

    public function getShop()
    {
        return ArrayHelper::map(Shop::find()->all(), 'id', 'name');
    }

    public function getOneShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'forShop']);
    }

    public function getProducts()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getMaxBonusPercentage()
    {
        return $this->hasOne(Percentage::className(), ['id' => 'max_bonus_percentage']);
    }

    public function getDiscount()
    {
        return $this->hasOne(Percentage::className(), ['id' => 'max_discount_percentage']);
    }

    public function getBonus()
    {
        return $this->hasOne(Percentage::className(), ['id' => 'max_bonus_percentage']);
    }
}
