<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "{{%product_default}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property double $price_bag
 * @property double $price_kg
 * @property integer $max_bonus_percentage
 * @property integer $max_discount_percentage
 * @property string $created_at
 */
class ProductDef extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_default}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'max_bonus_percentage', 'max_discount_percentage'], 'integer'],
            [['name', 'price_bag', 'price_kg'], 'required'],
            [['description'], 'string'],
            [['price_bag', 'price_kg'], 'number'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['max_bonus_percentage'], 'exist', 'skipOnError' => true, 'targetClass' => Percentage::className(), 'targetAttribute' => ['max_bonus_percentage' => 'id']],
            [['max_discount_percentage'], 'exist', 'skipOnError' => true, 'targetClass' => Percentage::className(), 'targetAttribute' => ['max_discount_percentage' => 'id']],
            [['forShop'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'description' => 'Description',
            'price_bag' => 'Price Bag',
            'price_kg' => 'Price Kg',
            'max_bonus_percentage' => 'Max Bonus Percentage',
            'max_discount_percentage' => 'Max Discount Percentage',
            'created_at' => 'Created At',
            'forShop' => 'For Shop',
        ];
    }
}
