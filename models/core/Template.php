<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_templates}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property integer $display_order
 * @property integer $active
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_templates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'path'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'path' => 'Path',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
