<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_mail_templates}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property string $description
 * @property string $text
 */
class MailTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_mail_templates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title', 'text'], 'required'],
            [['description', 'text'], 'string'],
            [['code', 'title'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
        ];
    }
}
