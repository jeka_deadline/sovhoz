<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_types_field_form}}".
 *
 * @property integer $id
 * @property string $name_field
 */
class TypeFieldForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_types_field_form}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_field'], 'required'],
            [['name_field'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_field' => 'Name Field',
        ];
    }
}
