<?php
namespace common\models\core;

class Helper
{

    public static function getPageMetaForModel($id, $modelClass)
    {
        return PageMeta::find()
                            ->where(['owner_id' => $id, 'model_class' => self::generateShortModelClass($modelClass)])
                            ->one();
    }

    public static function generateShortModelClass($modelClass)
    {
        return implode('\\', array_slice(explode('\\', $modelClass), 1));
    }

}