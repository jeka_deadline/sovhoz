<?php
return [
    'core' => [
        'class' => 'app\modules\core\Module',
    ],
    'user' => [
        'class' => 'app\modules\user\Module',
    ],
    'shop' => [
        'class' => 'app\modules\shop\Module',
    ],
    'gridview' => [
        'class' => 'kartik\grid\Module',
    ],
];
?>